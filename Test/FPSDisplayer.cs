﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class FPSDisplayer : MonoBehaviour
{
    private GUIText text;
    private float time = 0;
    private int frame = 0;
	private float prevRealTime = 0;
    void Start()
    {
        text = GetComponent<GUIText>();
		//TextMesh tm = null;
		prevRealTime = Time.realtimeSinceStartup;
    }

    void Update()
    {
        frame++;
		float nt = Time.realtimeSinceStartup;
		float deltat = nt-prevRealTime;
		prevRealTime = nt;
		time += deltat;
        if (time >= 0.5f)
        {
            text.text = "" + (int)(frame/0.5f);
			if(SpriteSheet.gldraw){
				Debug.Log(frame/0.5f);
			}
            time -= 0.5f;
            frame = 0;
        }
    }
}
