using UnityEngine;
using System.Collections;

public class HoldableObject : MonoBehaviour {
	
	
	private Transform tr;
	public Button4 obj;
	
	bool readyToRelease;
	
	private Vector2 prevSpeed;
	private Vector2 speed;
	// Use this for initialization
	void Start ()
	{
		tr = transform;
	}
	
	// Update is called once per frame
	void Update ()
	{
		/*RaycastHit rayHit;
		Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
		if (Physics.Raycast (ray, out rayHit)) {
			
			
			rayHit.collider.transform.localPosition = Camera.main.ScreenToWorldPoint (screenPoint);
		
		}*/
		
		if (obj.state == ButtonState.pressed) {
			readyToRelease = true;
			
			Vector3 screenPoint = Input.mousePosition;
			screenPoint.z = 10.0f; //distance of the plane from the camera
			tr.localPosition = Camera.main.ScreenToWorldPoint (screenPoint);
			
			prevSpeed = speed;
			speed = new Vector2 (tr.localPosition.x, tr.localPosition.y);
			
			Debug.Log ("x speed = " + (speed.x - prevSpeed.x) + "  " + " y Speed = " + (speed.y - prevSpeed.y));
			
			
		}
		if (obj.state == ButtonState.pressed && readyToRelease) {
			readyToRelease = false;
			tr.rigidbody.AddForce (new Vector3 ((speed.x - prevSpeed.x) * 3.0f, (speed.y - prevSpeed.y)*3.0f, 0), ForceMode.Impulse);
		}
		
		
	}
}
