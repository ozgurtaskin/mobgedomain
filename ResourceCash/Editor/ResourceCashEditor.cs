﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEditor;
using System.IO;
namespace ResourceCash
{
    class ResourceCashEditor
    {
        private const string CodeDirectory = "Assets/Scripts/Domain/ResourceCash";
        private const string ScriptName = "Cash";
        private const string Extension = "cs";

        private const string Namespace = "ResourceCash";
        private const string Extends = "System.Object";
        private HashSet<char> exceptedLetters;
        private HashSet<char> numbers;
        private static string CodeFullPath
        {
            get
            {
                return Path.Combine(CodeDirectory, ScriptName + "." + Extension);
            }
        }
        [MenuItem("Edit/Refresh Cash Script")]
        static void init()
        {
            ResourceCashEditor rcw = new ResourceCashEditor();

            rcw.OnGUI();
        }
        void OnGUI()
        {
            exceptedLetters = new HashSet<char>("abcdefghijklmnopqrstuvwxyzABCDEFGHİJKLMNOPQRSTUVWXYZ_1234567890");
            numbers = new HashSet<char>("1234567890");
            //Debug.Log(ResourceCash.CashInternal._VideoEditor_vicik_Rigidbody.mass);
            //Debug.Log(ResourceCash.CashInternal._Materials_menuico_Texture2D.width);

           // if (GUILayout.Button("refresh code"))
            {

                if (!Directory.Exists(CodeDirectory))
                {
                    Directory.CreateDirectory(CodeDirectory);
                }
                var writer = File.Create(CodeFullPath);
                sw = new StreamWriter(writer);
                generateFileContent();
                sw.Close();
                writer.Close();
                //var ta = (TextAsset)AssetDatabase.LoadAssetAtPath(CodeFullPath, typeof(TextAsset));
                //EditorUtility.SetDirty(ta);

                AssetDatabase.ImportAsset(CodeFullPath);
            }
        }
        #region resource traverse
        IEnumerable<ResourceInfo> allResourcePaths()
        {
            DirectoryInfo resDir = new DirectoryInfo("Assets/Resources");
            foreach (var ri in allResourcePaths("", "", resDir))
            {
                //Debug.Log(ri.varName);
                if (ri.type.IsPublic)
                {
                    yield return ri;
                }
            }
        }
        string trim(string s)
        {
            StringBuilder sb = new StringBuilder(s.Length);
            if (s.Length>0&& numbers.Contains(s[0]))
            {
                sb.Append("_");
            }
            foreach (var v in s)
            {
                if (exceptedLetters.Contains(v))
                {
                    sb.Append(v);
                }
                else
                {
                    sb.Append("_");
                }
            }
            return sb.ToString();
        }
        IEnumerable<ResourceInfo> allResourcePaths(string varName, string path, DirectoryInfo parent)
        {
            varName = trim(varName);
            foreach (var dir in parent.GetDirectories())
            {
                var r = allResourcePaths((varName.Length > 0 ? (varName + "_") : "") + dir.Name, (path.Length > 0 ? (path + "/") : "") + dir.Name, dir);
                foreach (var v in r)
                {
                    yield return v;
                }
            }
            foreach (var file in parent.GetFiles())
            {
                var fileName = file.Name;
                if (!fileName.EndsWith(".meta") && !fileName.EndsWith("DS_Store"))
                {
                    //Debug.Log(file.FullName);
                    int dotIndex = fileName.LastIndexOf('.');
                    var resName = fileName.Substring(0, dotIndex);
                    var fname = trim(resName) + ((dotIndex > 0) ? ("_" + fileName.Substring(dotIndex + 1)) : "");
                    var resPath = path + "/" + resName;
                    //Debug.Log(resPath);
                    var obj = AssetDatabase.LoadMainAssetAtPath("Assets/Resources/" + path + "/" + file.Name);
					//Debug.Log(file.Name + " " + fileName);
                    var fullVarName = varName + "__" + fname + "_" + obj.GetType().Name;
                    yield return new ResourceInfo(fullVarName, obj.GetType(), obj, resPath);
                    //Debug.Log(obj);
                    if (obj is GameObject)
                    {
                        var go = (GameObject)obj;
                        Type lastType = null;
                        foreach (var comp in go.GetComponents<Component>())
                        {
                            if (lastType != comp.GetType())
                            {
                                var ri = new ResourceInfo(varName + "__" + fname + "_" + comp.GetType().Name, comp.GetType(), comp, resPath);
                                ri.gameObjectVarName = fullVarName;

                                yield return ri;
                                lastType = comp.GetType();
                            }
                        }
                    }
                    else
                    {
                        //Debug.Log(path);
                    }
                }
            }
        }
        struct ResourceInfo
        {
            public string varName;
            public Type type;
            public UnityEngine.Object obj;
            public string resPath;
            public string gameObjectVarName;
            public string internalName;
            public ResourceInfo(string varName, Type type, UnityEngine.Object obj, string resPath)
            {
                this.varName = varName;
                this.type = type;
                this.obj = obj;
                this.resPath = resPath;
                gameObjectVarName = "";
                internalName = null;
            }
        }
        #endregion

        #region generate code
        int tabChars = 4;
        int indent = 0;
        StreamWriter sw;
        void generateFileContent()
        {
            string[] usings = new string[]{
                "UnityEngine",
                "System",
                "System.Collections.Generic",
            };
            foreach (var use in usings)
            {
                defineVariable("", "using", use);
            }
            beginGroup("", "namespace", Namespace);
            {
                beginGroup("public", "static class", ScriptName, new string[] { Extends });
                {
                    //defineVariable("public", "CashState", "state");
                    newLine();
                    List<ResourceInfo> variableNames = new List<ResourceInfo>();
                    foreach (var v in allResourcePaths())
                    {
                        if (!v.type.ToString().StartsWith("UnityEditor"))
                        {
                            string i_name;
                            putResourceProperty(v, out i_name);
                            var vc = v;
                            vc.internalName = i_name;
                            variableNames.Add(vc);
                        }

                    }
                    beginGroup("public static", "void", "Clear()");
                    {

                        foreach (var vname in variableNames)
                        {
                            defineVariable(vname.internalName, " = ", "null");
                        }
                    }
                    endGroup();
                    beginGroup("public static", "class", "Instantiate");
                    {

                        foreach (var vname in variableNames)
                        {
                            beginGroup("public static", vname.type.ToString(), vname.varName + "()");
                            {
                                append("return ");
                                append("(");
                                append(vname.type.ToString());
                                append(")");
                                append("GameObject.Instantiate(");
                                append("Cash.");
                                append(vname.varName);
                                append(");");
                            }
                            endGroup();
                        }
                    }
                    endGroup();
                }
                endGroup();
            }
            endGroup();
            
        }
        void append(object str)
        {
            sw.Write(str.ToString());
        }
        void putResourceProperty(ResourceInfo ri, out string i_name)
        {
            var internalName = ri.varName + "_i";
            i_name = internalName;
            //var boolName = ri.varName + "_b";
            defineVariable("private static", ri.type.ToString(), internalName);
            //defineVariable("private static", "bool", boolName + " = false");
            beginGroup("public static", ri.type.ToString(), ri.varName);
            {
                beginGroup("", "get", "");
                {
                    beginGroup("", "if", "(!" + internalName + ")");
                    {
                        if (ri.gameObjectVarName.Length > 0)
                        {
                            append(internalName);
                            append(" = ");
                            append(ri.gameObjectVarName);
                            append(".GetComponent<");
                            append(ri.type.ToString());
                            append(">();");
                        }
                        else
                        {
                            append(internalName);
                            append(" = (");
                            append(ri.type.ToString());
                            append(")Resources.Load(");
                            append("\"");
                            append(ri.resPath);
                            append("\", ");
                            append("typeof(");
                            append(ri.type.ToString());
                            append(")");
                            append(");");
                        }
                    }
                    endGroup();
                    defineVariable("", "return", internalName);
                }
                endGroup();
            }
            endGroup();
            /*if (ri.obj is GameObject)
            {
                var go = (GameObject)ri.obj;
                foreach (var comp in go.GetComponents<Component>())
                {

                }
            }*/
        }
        void newLine()
        {
            append("\r\n");
            for (int i = 0; i < tabChars * indent; i++)
            {
                append(' ');
                //Debug.Log("d");
            }
        }
        void defineVariable(string protection, string type, string name)
        {
            define(protection, type, name);
            append(';');
            newLine();
        }
        void define(string protection, string type, string name)
        {
            if (protection.Length > 0)
            {
                append(protection);
                append(' ');
            }
            append(type);
            append(' ');
            append(name);
        }
        void beginGroup(string protection, string type, string name, string[] extensions = null)
        {
            define(protection, type, name);
            if (extensions != null)
            {
                append(" : ");
                int count = 0;
                foreach (var v in extensions)
                {
                    append(v);
                    if (count < extensions.Length - 1)
                    {
                        append(", ");
                    }
                    count++;
                }
                //sb.Remove(sb.Length - 2, 2);
            }
            newLine();
            append('{');
            indent++;
            newLine();
        }
        void endGroup()
        {
            indent--;
            newLine();
            append('}');
            newLine();
        }
        #endregion
    }
}