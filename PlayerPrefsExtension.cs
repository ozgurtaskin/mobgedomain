﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using System.Xml.Serialization;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;

public class PlayerPrefsExtension 
{
    public static bool GetBool(string key, bool defaultValue)
    {
        return PlayerPrefs.GetInt(key, defaultValue ? 1 : 0) == 1;
    }
    public static void SetBool(string key, bool value)
    {
        PlayerPrefs.SetInt(key, value ? 1 : 0);
    }

    public static UnityEngine.Object GetObject(string key, UnityEngine.Object defaultValue)
    {
        if (!PlayerPrefs.HasKey(key))
        {
            return GameObject.Instantiate(defaultValue);
        }
        string s = PlayerPrefs.GetString(key);
        BinaryFormatter bf = new BinaryFormatter();
        MemoryStream ms = new MemoryStream(System.Convert.FromBase64String(s));
        UnityEngine.Object obj = (UnityEngine.Object)bf.Deserialize(ms);
        return GameObject.Instantiate(obj);
    }
    public static void SetObject(string key, UnityEngine.Object obj)
    {

        ISurrogateSelector iss;
        Debug.Log(obj.GetType().IsGenericType);
        
        MemoryStream ms = new MemoryStream();
        BinaryFormatter bf = new BinaryFormatter();
        new UnityEngine.Serialization.UnitySurrogateSelector().GetSurrogate(obj.GetType(), new StreamingContext(), out iss);
        Debug.Log(iss);
        bf.SurrogateSelector = iss;
        bf.Serialize(ms, obj);
        
        PlayerPrefs.SetString(key, System.Convert.ToBase64String(ms.GetBuffer()));

    }

}
