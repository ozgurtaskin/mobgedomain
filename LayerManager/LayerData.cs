﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class LayerData : MonoBehaviour
{
    public const int LayerCount = 32;
    public bool[] collisionInfo;

    public void readCollisionData()
    {
        int current = 0;
        for (int i = 0; i < LayerCount; i++)
        {
            for (int j = 0; j <= i; j++)
            {
                collisionInfo[current] = Physics.GetIgnoreLayerCollision(i, j);
                current++;
            }
        }
    }
    public void writeCollisionData()
    {
        int current = 0;
        for (int i = 0; i < LayerCount; i++)
        {
            for (int j = 0; j <= i; j++)
            {
                Physics.IgnoreLayerCollision(i, j, collisionInfo[current]);
                current++;
            }
        }
    }

    public void initCollisionInfo()
    {

        collisionInfo = new bool[LayerCount * (1 + LayerCount) / 2];
    }
}
