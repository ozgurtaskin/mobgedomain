﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;
using System;
using System.IO;

public class LayerManagerWindow : EditorWindow
{
	public enum Layers
	{
	}
    private const string DATA_PATH = "Domain/LayerData"; 
    [MenuItem("Window/Layer Manager")]
    static void init()
    {
        EditorWindow.GetWindow<LayerManagerWindow>("Layer Manager");
    }
    LayerData data;
    LayerData Data
    {
        get
        {
            if (data == null)
            {
                data = (LayerData)Resources.Load(DATA_PATH, typeof(LayerData));
            }
            if (data == null)
            {
                Directory.CreateDirectory("Assets/Resources/Domain");
                GameObject si = new GameObject("layer data");
                si.AddComponent<LayerData>();
                si.active = false;
                var obj = PrefabUtility.CreateEmptyPrefab("Assets/Resources/"+DATA_PATH + ".prefab");
                PrefabUtility.ReplacePrefab(si, obj);

                //AssetDatabase.CreateAsset(si, "Assets/Resources/SpriteSheet/sheetsInfo.prefab");
                //AssetDatabase.SaveAssets();
                DestroyImmediate(si);

                data = (LayerData)Resources.Load(DATA_PATH, typeof(LayerData));
                data.initCollisionInfo();
                data.transform.localPosition = data.transform.localPosition;
            }
            return data; 
        }
    }
    bool readCollisionAreYouSure = false;
    bool writeCollisionAreYouSure = false;
    void OnGUI()
    {
        if (GUILayout.Button("read collision info"))
        {
            readCollisionAreYouSure = true;
        }
        else if (readCollisionAreYouSure)
        {
            if (GUILayout.Button("no"))
            {
                readCollisionAreYouSure = false;
            }
            else if (GUILayout.Button("yes"))
            {
                readCollisionAreYouSure = false;
                Data.readCollisionData();
                Data.transform.localPosition = Data.transform.localPosition;
            }
        }
        if (GUILayout.Button("write collision info"))
        {
            writeCollisionAreYouSure = true;
        }
        else if (writeCollisionAreYouSure)
        {
            if (GUILayout.Button("no"))
            {
                writeCollisionAreYouSure = false;
            }
            else if (GUILayout.Button("yes"))
            {
                writeCollisionAreYouSure = false;
                Data.writeCollisionData();
            }
        }
		float toggleWidth = 20;
		int layerCount= LayerData.LayerCount;
		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.LabelField("",labelwidth);
        GUIStyle gs = GUI.skin.label;
        for (int j = layerCount-1; j >= 0; j--)
        {
			var mat = GUI.matrix;
		    string label = ""+((Layers)j);
            GUIContent gc = new GUIContent(label);
            var width = gs.CalcSize(gc).x;
            Rect rect = GUILayoutUtility.GetRect(15, 80, gs, togglewidth);

            //GUI.Label(rect, gc, gs);
            GUI.matrix = Matrix4x4.TRS(new Vector3(rect.xMax + 24, 70 -width+ rect.yMax - 50), Quaternion.Euler(0, 0, 90), new Vector3(1, 1, 1));
            GUI.Label(new Rect(3, 1, rect.height, rect.width), gc, gs);
			GUI.matrix = mat;
		}
		EditorGUILayout.EndHorizontal();
		for(int i = 0; i < layerCount; i++)
		{
			EditorGUILayout.BeginHorizontal();
			
			EditorGUILayout.LabelField(""+((Layers)i),labelwidth);
			for(int j = layerCount -1; j >= i ; j--){
				bool b = !Physics.GetIgnoreLayerCollision(i,j);
				b = EditorGUILayout.Toggle(b, togglewidth);
				Physics.IgnoreLayerCollision(i, j,!b);
			}
			EditorGUILayout.EndHorizontal();
		}
    }
	GUILayoutOption[] togglewidth{
		get{
			return new GUILayoutOption[]{ GUILayout.Width(15)};
		}
	}
	GUILayoutOption[] labelwidth{
		get{
			return new GUILayoutOption[]{ GUILayout.Width(80)};
		}
	}
}