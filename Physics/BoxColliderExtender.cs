﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class BoxColliderExtender : MonoBehaviour
{
    Rigidbody rb; BoxCollider bc; float oneOverScl;
    Vector3 startSize;
    Vector3 colliderOffset;
    public static BoxColliderExtender construct(Rigidbody rb, BoxCollider bc, float scl)
    {
        var bce = bc.gameObject.AddComponent<BoxColliderExtender>();
        bce.bc = bc;
        bce.rb = rb;
        bce.oneOverScl = 1/scl;
        bce.startSize = bc.size;
        bce.colliderOffset = bce.bc.center;
        return bce;
    }
    void FixedUpdate()
    {
        Vector3 vel = rb.velocity* oneOverScl;
        Vector3 absvel = new Vector3(Mathf.Abs(vel.x), Mathf.Abs(vel.y), Mathf.Abs(vel.z)) ;
        absvel = absvel * Time.deltaTime;
        var bodyCollider = bc;

        //Debug.Log(absvel);

        if (absvel.x > startSize.x / 2 || absvel.y > startSize.y / 2)
        {
            bodyCollider.size = startSize + absvel;
            bodyCollider.center = vel * Time.deltaTime / 2 + colliderOffset;
            //Debug.Log("Abs Val" + absvel);

        }
        else
        {
            bodyCollider.size = startSize;
            bodyCollider.center = colliderOffset;
        }
    }
}
