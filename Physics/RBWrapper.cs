﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


public class RBWrapper
{
    private Rigidbody rb;
    public RBWrapper(Rigidbody rb) { this.rb = rb; }

    public void AddForce(Vector3 force, ForceMode mode = ForceMode.Force)
    {
        AddForce(force.x, force.y, force.z, mode);
    }
    public void AddForce(float x, float y, float z, ForceMode mode = ForceMode.Force)
    {

        rb.AddForce(x, y, z, mode);
    }

    public Vector3 velocity { get { return rb.velocity; } set { rb.velocity = value; } }

    public static implicit operator RBWrapper( Rigidbody rb){return new RBWrapper(rb);}

    public float mass { get { return rb.mass; } set { rb.mass = value; } }

    public bool isKinematic { get { return rb.isKinematic; } set { rb.isKinematic = value; } }

    public Vector3 angularVelocity { get { return rb.angularVelocity; } set { rb.angularVelocity = value; } }

    public RigidbodyConstraints constraints { get { return rb.constraints; } set { rb.constraints = value; } }
}