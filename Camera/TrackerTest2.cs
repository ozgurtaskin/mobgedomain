using UnityEngine;
using System.Collections;

public class TrackerTest2 : MonoBehaviour {
    OutScreenTracker track;
	// Use this for initialization
	void Start () {

        track = new GameObject().AddComponent<OutScreenTracker>();
        
        //GameObject org = MonoBehaviourExtension.instantiatePrefab("Prefabs/Test/Capsule");
        //org.transform.localPosition = new Vector3(-261.06f, -53.0f, 0);


        GameObject tar = MonoBehaviourExtension.instantiatePrefab("Prefabs/Test/Capsule");
        tar.transform.localPosition = new Vector3(100, 50, 0);


        //track.origin = org.transform;
        track.target = tar.transform;

        LineRenderer lr = gameObject.AddComponent<LineRenderer>();
        //r.SetPosition(0, track.origin.localPosition);
        lr.SetPosition(1, track.target.localPosition);

        //tar.transform.parent = transform;

        transform.parent = track.tr;
        transform.localPosition = Vector3.zero;


	}
	
	// Update is called once per frame
	void OnGUI () {
        if (GUILayout.Button("delete"))
        {
            if (track != null)
            {
                this.DestroyObj(track.gameObject);
            }
        }
	}
}
