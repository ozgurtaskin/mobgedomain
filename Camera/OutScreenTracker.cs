using UnityEngine;
using System.Collections;

public class OutScreenTracker : MonoBehaviour
{

    public const float ANIM_TIME = 0.2f;

    // Use this for initialization

    public enum Mode
    {
        AllEdges,
        RightOnly,
    }

    Layer layer;
    //public Transform origin;
    public Transform target;
    public Transform tr;

    public Mode mode;
    //public GameObject indicator;

    Vector2 screenOffset;
    private Rect camRect;
    Vector2 crossPoint;
    public float distance;
    public float direction;
    float stopTrackingDistance;
    bool destroyWhenFinished;



    float maxDistance;

    float time;

    private bool destroyReady = true;

    public static OutScreenTracker construct(Transform target, Vector2 screenOffset, float stopTrackingDistance = 0, float maxDistance = float.PositiveInfinity)
    {
        GameObject go = new GameObject("tracker");
        var ost = go.AddComponent<OutScreenTracker>();

        ost.mode = Mode.AllEdges;

        ost.target = target;
        ost.screenOffset = screenOffset;
        ost.stopTrackingDistance = stopTrackingDistance + screenOffset.magnitude;
        ost.maxDistance = maxDistance;
        return ost;
    }

    void Awake()
    {

        //Debug.Log((Vector3)new Vector2(4, 6));
        tr = transform;
        layer = new GameObject().AddComponent<Layer>();
        layer.Z = 4;

        destroyWhenFinished = false;

        tr.parent = layer.tr;
    }

    // Update is called once per frame
    void Update()
    {
        time -= Time.deltaTime;
        //layer.Z = -layer.tr.parent.position.z;
        if (target)
        {
            Vector3 v = layer.tr.position;
            Vector2 v2 = v;
            camRect = new Rect(v.x + screenOffset.x, v.y + screenOffset.y,
                layer.size.x - 2 * screenOffset.x, layer.size.y - 2 * screenOffset.y);

            crossPoint = new Vector2(0, 0);

            var center = v2 + layer.size * 0.5f;
            Vector3 tp3 = target.position;
            Vector2 tpos2 = tp3;
            Vector2 tp2 = tpos2;
            float sclFactor = layer.Z / (tp3.z - layer.tr.parent.position.z);
            tp2 = center + (tp2 - center) * sclFactor;
            float minfD = stopTrackingDistance * sclFactor;

            //Debug.Log((tp3.z+" " +layer.tr.parent.position.z));
            //Debug.Log(tp3 + " " + tp2);
            bool b = intersects(center, tp2, camRect, out crossPoint);
            if (b && (distance = Mathf2.Distance(crossPoint, tp2)/sclFactor) > minfD && distance <= maxDistance)
            {
                //if (indicator != null)
                {
                    tr.localPosition = new Vector3(crossPoint.x - v.x, crossPoint.y - v.y);

                    if (destroyReady)
                    {
                        destroyReady = false;
                        //gameObject.SetActiveRecursively(true);
                        reverseTime();
                    }
                }
            }
            else
            {
                if (destroyReady == false)
                {
                    destroyReady = true;
                    //gameObject.SetActiveRecursively(false);
                    //gameObject.active = true;
                    reverseTime();

                }
            }
        }
        else
        {
            destroyWhenFinished = true;
            if (!destroyReady)
            {
                destroyReady = true;
                reverseTime();
            }
        }
        //Debug.Log(distance);
        if (time >= 0)
        {
        }
        else
        {
            if (time < 0)
            {
                time = 0;
                if (destroyWhenFinished)
                {

                    this.DestroyObj(layer.gameObject);
                    //Debug.Log("yeeees");
                }
            }
        }
        float scl = (destroyReady ? time : ANIM_TIME - time) / ANIM_TIME;
        tr.localScale = new Vector3(scl, scl, scl);
    }
    bool intersects(Vector2 startP, Vector2 targetP, Rect camRect, out Vector2 crossPoint)
    {
        crossPoint = new Vector2(0, 0);
        bool b = false;
        switch (mode)
        {
            case Mode.AllEdges:
                b = Mathf2.LineIntersectsRect(startP, targetP, camRect, out crossPoint);
                break;
            case Mode.RightOnly:
                float r, s;
                startP.y = targetP.y;
                b = Mathf2.LineIntersectsLine(startP, targetP, new Vector2(camRect.xMax, camRect.y), new Vector2(camRect.xMax, camRect.yMax), out r, out s);
                crossPoint = startP + (targetP - startP) * r;
                break;
            default:
                break;
        }
        return b;
    }
    void reverseTime()
    {
        if (time < 0)
        {
            time = ANIM_TIME;
        }
        else
        {
            time = ANIM_TIME - time;
        }
    }
    /*
    void OnDestroy()
    {
        this.DestroyObj(layer.gameObject);
    }*/
}


/*testc Usage
OutScreenTracker track = new GameObject().AddComponent<OutScreenTracker>();
        
        GameObject org = MonoBehaviourExtension.instantiatePrefab("Prefabs/Test/Capsule");
        org.transform.localPosition = new Vector3(-261.06f, -53.0f, 0);


        GameObject tar = MonoBehaviourExtension.instantiatePrefab("Prefabs/Test/Capsule");
        tar.transform.localPosition = new Vector3(100, 50, 0);


        track.origin = org.transform;
        track.target = tar.transform;

        LineRenderer lr = gameObject.AddComponent<LineRenderer>();
        lr.SetPosition(0, track.origin.localPosition);
        lr.SetPosition(1, track.target.localPosition);

        //tar.transform.parent = transform;
        
        track.indicator = transform.gameObject;
 * 
 * */

