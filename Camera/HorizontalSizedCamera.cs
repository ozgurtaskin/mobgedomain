﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[ExecuteInEditMode]
public class HorizontalSizedCamera : MonoBehaviour
{
    public float size;
    public float fieldOfView;
    public Camera cam;
    
    private float aspect;
    void Awake()
    {
        cam = GetComponent<Camera>();
        LateUpdate();
    }
    void Start()
    {
        aspect = cam.aspect;
    }
    private float Aspect
    {
        get
        {
            return aspect;
        }
    }
    public void Reset()
    {
        size = cam.orthographicSize * Aspect;
        float s = Mathf.Tan(cam.fov * Mathf.PI / 360) * Aspect;
        fieldOfView = Mathf.Atan(s) * 360 / Mathf.PI;
        //Debug.Log("reset: " + size + " " + camera.orthographicSize + " " + Aspect);
    }
    void OnEnable()
    {

    }
    void Update()
    {
#if UNITY_EDITOR
        LateUpdate();
#endif
    }
    void LateUpdate()
    {
        aspect = cam.aspect;
        cam.orthographicSize = size / Aspect;
        float s = Mathf.Tan(fieldOfView * Mathf.PI / 360) / Aspect;
        cam.fov = Mathf.Atan(s) * 360 / Mathf.PI;
        //Debug.Log("update: " + size + " " + camera.orthographicSize + " " + Aspect);
    }
}
