﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using System.Runtime.Serialization;

public class Button3D : MonoBehaviour
{
    public event EventHandler<EventArgs> onClick;
    public ButtonState state;
    protected Vector3 mp;
    private int previousTouchCount;
    private int fingerid;
    public object pocket;
    protected virtual void Start()
    {
        state = ButtonState.idle; 
    }

    public static ButtonState Button3DStateUpdate(Collider collider,ButtonState oldState, int previousTouchCount,ref int fingerid, out bool clicked, out Vector3 mp)
    {
        ButtonState state = oldState;
        clicked = false;
        mp = Vector3.zero;
        if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer)
        {
            if (state != ButtonState.pressed)
            {
                if (previousTouchCount < Input.touchCount)
                {
                    foreach (Touch t in Input.touches)
                    {
                        if (t.phase == TouchPhase.Began)
                        {
                            Ray r = UnityEngine.Camera.mainCamera.ScreenPointToRay(t.position);
                            RaycastHit rh;
                            if (collider.Raycast(r, out rh, 1000))
                            {
                                state = ButtonState.pressed;
                                mp = new Vector3(t.position.x, t.position.y, 0);
                                fingerid = t.fingerId;
                                break;
                            }
                        }
                    }
                }
            }
            else
            {
                
                //if (previousTouchCount > Input.touchCount)
                {
                    foreach (Touch t in Input.touches)
                    {
                        if (t.fingerId == fingerid)
                        {
                            if (t.phase == TouchPhase.Ended)
                            {
                                state = ButtonState.idle;
                                clicked = true;
                            }
                            mp = new Vector3(t.position.x, t.position.y, 0);
                            break;
                        }
                    }
                }
            }
        }
        else
        {
            if (state != ButtonState.pressed)
            {
                if (Input.GetMouseButtonDown(0))
                {
                    Vector3 v = Input.mousePosition;
                    Ray r = UnityEngine.Camera.mainCamera.ScreenPointToRay(v);
                    RaycastHit rh;
                    if (collider.Raycast(r, out rh, 1000))
                    {
                        state = ButtonState.pressed;
                        mp = v;
                    }
                }
            }
            else
            {
                mp = Input.mousePosition;

                if (Input.GetMouseButtonUp(0))
                {

                    state = ButtonState.idle;
                        //Debug.Log("clicked");
                    clicked = true;
                }
            }
        }
        return state;
    }

    protected virtual void Update()
    {
        bool clicked;
        state = Button3DStateUpdate(collider, state, previousTouchCount, ref fingerid, out clicked, out mp);
        if (clicked)
        {
            if (onClick != null)
            {
                onClick(this, new EventArgs());
            }
        }
        previousTouchCount = Input.touchCount;
    }
}
