﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class GUIComponent : MonoBehaviour
{
    public Layer parent;
    public Rect bounds;

    protected bool mouseOffset(out Vector2 mouseOffset, Vector3 mousePosition)
    {
        Vector2 mousePos = parent.convertMousePos(mousePosition);
        Vector3 pos = transform.localPosition;
        mousePos = mousePos - new Vector2(pos.x, pos.y);
        mouseOffset = mousePos;
        return bounds.Contains(mousePos);
    }
}
