﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[ExecuteInEditMode]
public class EditorRectDrawer : MonoBehaviour
{
    Rect _rect;
    void Awake()
    {
        if (Application.isPlaying)
        {
            this.DestroyObj(this);
        }
    }
    public Rect rect
    {
        get
        {
            return _rect;
        }
        set
        {
            _rect = value;
        }
    }
    void OnGUI()
    {
        Rect r = new Rect(_rect.x * Screen.width, (1 - _rect.y - _rect.height) * Screen.height, _rect.width * Screen.width, _rect.height * Screen.height);
        var pointA = new Vector2(r.x, r.y);
        var pointB = new Vector2(r.x + r.width, r.y + r.height);
        GUI.enabled = false;
        
        GUI.TextArea(r, "");
    }
}
