using UnityEngine;
using System.Collections;

public class NumericOnGUI
{
	private string text;
	private decimal value;
	public decimal Value{
		get{
			return value;
		}
		set{
			this.value = value;
			text = value.ToString();
		}
	}
	public NumericOnGUI(){
		text = "";
	}
	public decimal field(params GUILayoutOption[] options){
		decimal d;
		string result = GUILayout.TextField(text,options);
		if(tryParse(result, out d)){
			text = result;
			value = d;
			return d;
		}
		return value;
	}
	private bool tryParse(string s, out decimal val){
		if(s.Length <= 0){
			val = 0;
			return true;
		}
		return decimal.TryParse(s, out val);
	}
}

