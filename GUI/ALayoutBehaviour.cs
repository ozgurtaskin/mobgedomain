﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public abstract class ALayoutBehaviour : MonoBehaviour , ILayoutItem
{
    public abstract LayoutItemData Data
    {
        get;
        set;
    }
}
