﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[AddComponentMenu("Layout/Scale Layout/Item")]
public class ScaleLayoutItem : MonoBehaviour
{
    public LayoutAnchor layoutAnchor;
    public ScaleAnchor scaleAnchot;

    public Transform tr;
    public Vector2 positionOffset;
    public Vector2 scale;

    public ScaleLayout parent;

    public ILayoutItem layoutItem;

    public bool scaledOverScreen;

    void Awake()
    {
        if (!tr)
        {
            tr = transform;
        }
        refreshLayoutItem();
    }
    void Reset()
    {
        tr = transform;
        scaleAnchot = ScaleAnchor.Horizontal;
        refreshLayoutItem();
    }

    public void refreshLayoutItem()
    {
        layoutItem = GetComponent<ALayoutBehaviour>();
        if (layoutItem == null)
        {
            layoutItem = new TransformLayoutItem(tr);
        }
    }

    public void refreshTransform()
    {
        if (parent)
        {
            Vector2 size = parent.size();
            Vector3 scl = new Vector3();
            switch (scaleAnchot)
            {
                case ScaleAnchor.Horizontal:
                    scl = new Vector3(size.x * scale.x, size.x * scale.y, 1);
                    break;
			case ScaleAnchor.Vertical:
				scl = new Vector3(size.y * scale.x, size.y * scale.y, 1);
                    break;
                case ScaleAnchor.Both:
                    scl = new Vector3(size.x * scale.x, size.y * scale.y, 1);
                    break;
                default:
                    break;
            }
            Vector3 pos = new Vector3(

                calculatePos(
                 (layoutAnchor & LayoutAnchor.Left) == LayoutAnchor.Left, (layoutAnchor & LayoutAnchor.Right) == LayoutAnchor.Right,
                 size.x, positionOffset.x, scl.x),
                calculatePos(
                 (layoutAnchor & LayoutAnchor.Down) == LayoutAnchor.Down, (layoutAnchor & LayoutAnchor.Up) == LayoutAnchor.Up,
                 size.y, positionOffset.y, scl.y),

                0);
            //tr.localScale = scl;
            //tr.localPosition = pos;
            if (layoutItem == null)
            {
                refreshLayoutItem();
            }
            if (scaledOverScreen)
            {
                pos = new Vector3(pos.x / size.x, pos.y / size.y);
                scl = new Vector3(scl.x / size.x, scl.y / size.y);
                //Debug.Log("bilibilibilibli");
            }
            layoutItem.Data = new LayoutItemData(pos, scl);
        }
    }
    public void saveTransform()
    {
        if (parent)
        {
            Vector2 size = parent.size();
            var data = layoutItem.Data;
            scaledOverScreen = data.scaledOverScreen;
            if (scaledOverScreen)
            {
                data.Position = new Vector2(data.Position.x * size.x, data.Position.y * size.y);
                data.Size = new Vector2(data.Size.x * size.x, data.Size.y * size.y);
            }
            Vector2 rscl = data.Size;
            Vector3 rpos = data.Position;
            switch (scaleAnchot)
            {
                case ScaleAnchor.Horizontal:
                    scale = new Vector2(rscl.x / size.x, rscl.y / size.x);
                    break;
			case ScaleAnchor.Vertical:
				scale = new Vector2(rscl.x / size.y, rscl.y / size.y);
                    break;
                case ScaleAnchor.Both:
                    scale = new Vector2(rscl.x / size.x, rscl.y / size.y);
                    break;
                default:
                    break;
            }
            positionOffset.x = calculateUnitPos(
                (layoutAnchor & LayoutAnchor.Left) == LayoutAnchor.Left, (layoutAnchor & LayoutAnchor.Right) == LayoutAnchor.Right, 
                size.x, rpos.x, rscl.x);
            positionOffset.y = calculateUnitPos(
                (layoutAnchor & LayoutAnchor.Down) == LayoutAnchor.Down, (layoutAnchor & LayoutAnchor.Up) == LayoutAnchor.Up, 
                size.y, rpos.y, rscl.y);
        }
    }
    float calculateUnitPos(bool anc1, bool anc2, float totalSize, float pos, float scl)
    {
        if (!(anc1 || anc2))
        {
            return pos/totalSize;
        }
        if (anc1)
        {
            return pos / (scl);
        }
        else
        {
            return (totalSize - pos) / (scl);
        }
    }
    float calculatePos(bool anc1, bool anc2, float totalSize, float upos, float rscl)
    {
        if (!(anc1 || anc2))
        {
            return upos * totalSize;
        }
        if (anc1)
        {
            return upos * (rscl);
        }
        else
        {
            return totalSize - (upos) * (rscl);
        }
    }
    public bool refreshParent()
    {
        if (parent == null || parent.tr != tr.parent)
        {
            if (tr.parent)
            {
                parent = tr.parent.gameObject.GetComponent<ScaleLayout>();
                if (parent)
                {
                    parent.addItem(this);
                }
                return parent;
            }
            return false;
        }
        return true;
    }


}
[Serializable]
public class TransformLayoutItem : ILayoutItem
{
    public Transform tr;
    public TransformLayoutItem(Transform tr)
    {
        this.tr = tr;
    }

    LayoutItemData ILayoutItem.Data
    {
        get
        {
            return new LayoutItemData(tr.localPosition, tr.localScale);
        }
        set
        {
            var z = tr.localPosition.z;
            tr.localPosition = new Vector3(value.Position.x, value.Position.y, z);
            tr.localScale = new Vector3(value.Size.x, value.Size.y, 1);
        }
    }
}
public enum LayoutAnchor
{
    Up = 1,
    Down = 2,
    Left = 4,
    Right = 8,
}
public enum ScaleAnchor
{
	Horizontal,
	Vertical,
    Both,
}