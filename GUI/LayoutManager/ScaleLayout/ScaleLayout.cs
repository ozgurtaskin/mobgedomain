﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[ExecuteInEditMode]
[AddComponentMenu("Layout/Scale Layout/Manager")]
public class ScaleLayout : MonoBehaviour
{
    public Transform tr;
    public Layer layer;
    public List<ScaleLayoutItem> items;
    //private bool dirty;
    //[SerializeField]
    //private float z;
    float aspect;
    [SerializeField]
    Camera mainCam;

    void Awake()
    {
        if (!tr)
        {
            tr = transform;
        }
        mainCam = Camera.mainCamera;
    }
    void Reset()
    {
        tr = transform;
        mainCam = Camera.mainCamera;
    }
    public void ensureLayer()
    {
        if (!layer)
        {
            layer = gameObject.AddComponent<Layer>();
            layer.Z = 5;
            //dirty = true;
        }
    }
    public void setZInPrefab(float z)
    {
        layer.z = z;
    }
    public float Z
    {
        get
        {
            return layer.Z;
        }
        set
        {

            layer.Z = value;
            //dirty = false;
            layout();
        }
    }
    /*public float ZEditor
    {
        get
        {
            return z;
        }
        set
        {
            z = value;
            dirty = true;
        }
    }*/
    internal Vector2 size()
    {
        if (layer)
        {
            return layer.size;
        }
        return new Vector2(1, 1);
    }

    public void refreshItems()
    {
        items = new List<ScaleLayoutItem>();
        foreach (Transform t in tr)
        {
            var v = t.GetComponent<ScaleLayoutItem>();
            if (v)
            {
                items.Add(v);
            }
        }
    }
    public void addItem(ScaleLayoutItem item)
    {
        if (!items.Contains(item))
        {
            items.Add(item);
        }
    }
    public void layout()
    {
        if (items != null)
        {
            foreach (var v in items)
            {
                if (v)
                {
                    v.refreshTransform();
                    //Debug.Log("s");
                }
            }
        }
        //Debug.Log("layout");
    }
    void Update()
    {
        /*if (dirty)
        {
            dirty = false;
            layer.Z = z;
        }*/
        if (Mathf.Abs(aspect - mainCam.aspect) > 0.0001f)
        {
            layer.Z = layer.Z;
            layout();
            aspect = Layer.aspect(mainCam);
        }
        //Debug.Log(aspect);
    }
    void LateUpdate()
    {
    }
}
