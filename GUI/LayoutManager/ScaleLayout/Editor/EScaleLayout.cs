﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(ScaleLayout))]
public class EScaleLayout : Editor
{
    public override void OnInspectorGUI()
    {
        ScaleLayout go = (ScaleLayout)target;
        go.ensureLayer();
        var z = EditorGUILayout.FloatField("layer Z", go.Z);
        if (EditorUtility.GetPrefabType(go) == PrefabType.Prefab)
        {
            go.setZInPrefab(z);
        }
        else
        {

            go.Z = z;
        }
        go.refreshItems();
        if (GUILayout.Button("layout"))
        {
            if (EditorUtility.GetPrefabType(go) == PrefabType.Prefab)
            {
                
            }
            else
            {
                go.layout();
            }
        }
    }
}
