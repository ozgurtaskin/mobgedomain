﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(ScaleLayoutItem))]
public class EScaleLayoutItem : Editor
{
    private bool positionAnchorToggle = false;
    float width = 15;
    bool changed;

    public override void OnInspectorGUI()
    {
        changed = false;
        var go = (ScaleLayoutItem)target;
        go.refreshLayoutItem();
        go.layoutAnchor = positionAnchorField("position anchor", go.layoutAnchor);
        go.scaleAnchot = scaleAnchorField("scale anchor", go.scaleAnchot);
        changed = changed | go.refreshParent();
        var data = go.layoutItem.Data;
        data.Position = EditorGUILayout.Vector3Field("position", data.Position);
        data.Size = EditorGUILayout.Vector3Field("scale", data.Size);
        go.layoutItem.Data = data;
        go.saveTransform();
        if (GUILayout.Button("print layout item"))
        {
            Debug.Log(go.layoutItem);
        }
    }
    LayoutAnchor positionAnchorField(string s,LayoutAnchor anc)
    {
        float offset = 50;

        positionAnchorToggle = EditorGUILayout.Foldout(positionAnchorToggle,s);
        if (positionAnchorToggle)
        {
            EditorGUILayout.BeginHorizontal();
            GUILayout.Space(offset);
            GUILayout.Label("", GUILayout.Width(width));
            anc = (LayoutAnchor)mutualToggle((int)anc, (int)LayoutAnchor.Up, (int)LayoutAnchor.Down);
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            GUILayout.Space(offset);
            anc = (LayoutAnchor)mutualToggle((int)anc, (int)LayoutAnchor.Left, (int)LayoutAnchor.Right);
            GUILayout.Label("", GUILayout.Width(width));
            anc = (LayoutAnchor)mutualToggle((int)anc, (int)LayoutAnchor.Right, (int)LayoutAnchor.Left);
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            GUILayout.Space(offset);
            GUILayout.Label("", GUILayout.Width(width));
            anc = (LayoutAnchor)mutualToggle((int)anc, (int)LayoutAnchor.Down, (int)LayoutAnchor.Up);
            EditorGUILayout.EndHorizontal();
        }

        

        return anc;
    }
    ScaleAnchor scaleAnchorField(string s, ScaleAnchor sli)
    {
        var v = (ScaleAnchor)EditorGUILayout.EnumPopup(s, sli);
        if (v != sli)
        {
            changed = true;
        }
        return v;
    }
    /*T mutualToggle<T>(T opt, T toggleBit, T otherBit) where T : Sprite
    {
        return (T)mutualToggle((int)opt, (int)toggleBit, (int)otherBit);
    }*/
    int mutualToggle(int opt, int toggleBit, int otherBit)
    {
        bool oldValue = (opt & toggleBit) == toggleBit;
        bool t = GUILayout.Toggle(oldValue, "", GUILayout.Width(width));
        if (t && !oldValue)
        {
            opt |= toggleBit;
            opt &= ~otherBit;
            changed = true;
        }
        if (!t && oldValue)
        {
            opt &= ~toggleBit;
        }
            return opt;
    }
}
