using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

[ExecuteInEditMode]
public class ButtonManager : MonoBehaviour
{

    private static ButtonManager singelton;

    private const int mouseId = 1000;
    private List<Button4> buttons;
    private Dictionary<int, ButtonGroup> pressedButtons;
    private Transform camtr;
    public bool Dirty { get; set; }
    public static ButtonManager Singelton
    {
        get
        {
            if (singelton == null)
            {
                singelton = new GameObject("button manager").AddComponent<ButtonManager>();
                GameObject.DontDestroyOnLoad(Singelton.gameObject);


				singelton.levelStarted();

                singelton.buttons = new List<Button4>();
                singelton.pressedButtons = new Dictionary<int, ButtonGroup>();
                //singelton.buttons.OrderBy 

            }
            return singelton;
        }
    }
	void OnLevelWasLoaded (int level)
	{
		levelStarted();
	}
	void levelStarted ()
	{
        if (singelton && Camera.mainCamera)
        {
            singelton.camtr = Camera.mainCamera.transform;
        }
	}
    public int ButtonCount { get { return buttons.Count; } }
    void Update()
    {
        if (!Application.isPlaying)
        {
            Debug.Log("destroying singelton");
            GameObject.DestroyImmediate(gameObject);
        }
        if (Dirty)
        {
            buttons.Sort();
            Dirty = false;
        }
        //Camera cam = Camera.mainCamera;
        if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer)
        {
            foreach (var t in Input.touches)
            {
                switch (t.phase)
                {
                    case TouchPhase.Began:
                        handlePress(t.fingerId, screenPointToRay(t.position));
                        break;
                    case TouchPhase.Ended:
                        handleRelease(t.fingerId, screenPointToRay(t.position));
                        break;
                    case TouchPhase.Moved:
                        handleMove(t.fingerId, screenPointToRay(t.position));
                        break;
                    case TouchPhase.Stationary:
                        handleMove(t.fingerId, screenPointToRay(t.position));
                        break;
                }
            }
        }
        else
        {

            if (Input.GetMouseButtonDown(0))
            {
                handlePress(mouseId, screenPointToRay(Input.mousePosition));
            }
            if (Input.GetMouseButtonUp(0))
            {
                handleRelease(mouseId, screenPointToRay(Input.mousePosition));
            }
            if (Input.GetMouseButton(0))
            {
                handleMove(mouseId, screenPointToRay(Input.mousePosition));
            }
        }
    }
    private Ray screenPointToRay(Vector3 pos)
    {
        Camera cam = Camera.mainCamera;
        Ray r = cam.ScreenPointToRay(Input.mousePosition);
        r.origin = camtr.position;
        return r;
    }
    private bool tryGetRay(int id, out Ray ray)
    {
        ButtonGroup group;
        bool b = pressedButtons.TryGetValue(id, out group);
        ray = group.args.ray;
        return b;
    }
    private void handlePress(int id, Ray r, Button4 ignoreButton = null)
    {
        ButtonEventArgs args = new ButtonEventArgs(r, id);
        //Debug.Log("handle press");
        var list = new List<Button4>();
        pressedButtons[id] = new ButtonGroup(list, new ButtonEventArgs(r, id));
        foreach (var b in buttons)
        {
            if (b != ignoreButton && b.enabled && b.gameObject.active && b.cursorDown(args))
            {
                list.Add(b);
                if (b.holdInput)
                    break;
            }
        }
    }
    private void handleMove(int id, Ray r)
    {
        ButtonGroup list;


        if (pressedButtons.TryGetValue(id, out list))
        {
            list.args.ray = r;
            foreach (var b in list.buttons)
            {
                var move = b.cursorMoved(list.args);
                if (!move)
                {
                    list.args.touchCancel = true;
                    handlePress(id, r, b);
                    b.cursorUp(list.args);
                    list.args.touchCancel = false;
                }
            }
        }
    }
    private void handleRelease(int id, Ray r)
    {
        //Debug.Log("handle release");
        ButtonGroup list;


        if (pressedButtons.TryGetValue(id, out list))
        {

            pressedButtons.Remove(id);
            list.args.ray = r;
            foreach (var b in list.buttons)
            {
                b.cursorUp(list.args);
            }
        }
    }
    /*public void priorityChanged(Button4 button, int oldProprity)
    {
		
        if(oldProprity<button.Priority)
        {
			
        }
        else
        {
        }
		
    }*/
    public void addButton(Button4 button)
    {
        buttons.Add(button);
        Dirty = true;
        /*int i = buttons.Length-2;
        while( i >= 0 )
        {
            if(buttons[i].CompareTo(button) < 0)
            {
                buttons[i+1] = buttons[i];
                i--;
            }
            else
            {
                break;
            }
        }
        buttons[i+1] = button;*/
    }
    public void removeButton(Button4 button)
    {
        if (Dirty)
        {
            buttons.Sort();
            if (this)
            {
                Dirty = false;
            }
        }
        int i = buttons.BinarySearch(button);
        if (i >= 0)
        {
            buttons.RemoveAt(i);
        }
        //Dirty = true;
        /*int i = 0;
        for(;i<buttons.Count; i++)
        {
            if(buttons[i] == button)
            {
                for(; i < buttons.Count-1; i++)
                {
                    buttons[i] = buttons[i+1];
                }
                buttons.RemoveAt(i);
                break;	
            }
        }*/
    }
    private class ButtonGroup
    {
        public List<Button4> buttons;
        public ButtonEventArgs args;
        public ButtonGroup(List<Button4> buttons, ButtonEventArgs args)
        {
            this.args = args;
            this.buttons = buttons;
        }
    }
}
