﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public interface ILayoutItem 
{
    LayoutItemData Data { get; set; }
}
public struct LayoutItemData
{
    public Vector2 Size;
    public Vector2 Position;
    public bool scaledOverScreen;
    //public Vector2 totalSize;
    public LayoutItemData(Vector2 pos, Vector2 size)
    {
        this.Size = size;
        this.Position = pos;
        scaledOverScreen = false;
        //this.totalSize = totalSize;
    }
}