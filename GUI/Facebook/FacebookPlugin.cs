using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;
using System;

public class FacebookPlugin : MonoBehaviour {

	[DllImport("__Internal")]
	private extern static void init(string appid, string[] permissions, int percount);
	[DllImport("__Internal")]
	public extern static void login();
	[DllImport("__Internal")]
	public extern static void logout();
	[DllImport("__Internal")]
	private extern static Friend getScoreInfo(int i);
	[DllImport("__Internal")]
	private extern static Friend getUserInfo();
	private class RQ{
		[DllImport("__Internal")]
		public extern static bool isLoggedIn();
		[DllImport("__Internal")]
		public extern static void requestScores();
		[DllImport("__Internal")]
		public extern static void postScore(int score);
	}
	public static bool isLoggedIn ()
	{
		if (FacebookEnabled) {
			return RQ.isLoggedIn ();
		} else {
			return true;
		}
	}
	public static void postScore (int score)
	{
		if (FacebookEnabled) {
			if (isLoggedIn ()) {
				userInfo.score = score;
			}
			RQ.postScore (score);
		} else {
		}
	}
	public static void requestScores ()
	{
		if (FacebookEnabled) {
			RQ.requestScores ();
		} else {
			Shared().scoresCame("");
		}
	}
	public static Friend userInfo;
	public static void init(string appid, string[] permissions)
	{
		Shared();
		init(appid,permissions, permissions.Length);
	}

	private static FacebookPlugin shared;
	private static FacebookPlugin Shared ()
	{
		if (shared == null) {
			GameObject go = new GameObject("FacebookPlugin");
			GameObject.DontDestroyOnLoad(go);
			shared = go.AddComponent<FacebookPlugin>();
			userInfo = new Friend();
		}
		return shared;
	}

	public struct Friend : IComparable<Friend>
	{
		public string name;
		public int score;
		public string userId;

		#region IComparable implementation

		public int CompareTo (Friend other)
		{
			return score-other.score;
		}

		#endregion

		public override string ToString ()
		{
			return "name: " + name + " id: " + userId + " score: " + score;
		}
	}

	public static FacebookPluginListener listener;
	private static bool FacebookEnabled
	{
		get{
			return Application.platform == RuntimePlatform.IPhonePlayer;
		}
	}
	public void loggedIn (string message)
	{

		print (message);
		userInfo = getUserInfo();
		Debug.Log(userInfo);
		if (listener != null) {
			listener.loggedIn();
		}
	}
	public void loginFailed (string message)
	{
		print (message);
		if (listener != null) {
			listener.loginFailed ();
		}
	}
	public void scoresCame (string count)
	{
		if (FacebookEnabled) {
			int cnt = int.Parse (count);
			Debug.Log ("score count: " + cnt);
			Friend[] friends = new Friend[cnt];
			for (int i = 0; i < friends.Length; i++) {
				friends [i] = getScoreInfo (i);
				if(friends[i].userId == userInfo.userId)
				{
					userInfo.score = friends[i].score;
				}
			}
			if (listener != null) {
				listener.scoreListArrived (friends);
			}
		} else {
			
			Friend[] friends = new Friend[0];
			/*friends[0].name = "bicik";
			friends[0].score = 120;
			friends[1].name = "bip bip bup";
			friends[1].score = 1200;
			friends[2].name = "vaaaaa lülülülülü";
			friends[2].score = 540;
			friends[3].name = "zibidibi zibidibi";
			friends[3].score = 530;
			friends[4].name = "000000";
			friends[4].score = 0;
			friends[4].name = "arada";
			friends[4].score = 533;

			*/
			if (listener != null) {
				listener.scoreListArrived (friends);
			}
		}
	}
	public void scoresFailed (string message)
	{
		print (message);
		if (listener != null) {
			listener.scoreListFailed ();
		}
	}
}
public interface FacebookPluginListener
{
	void loggedIn();
	void loginFailed();


	void scoreListArrived(FacebookPlugin.Friend[] friends);
	void scoreListFailed();
}