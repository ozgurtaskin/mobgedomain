using System;
using UnityEngine;

public class FacebookButton : Button3D, FacebookPluginListener
{
	Sprite sprite;
	private bool logged;
	void Start ()
	{
		sprite = GetComponent<Sprite> ();
		if (FacebookPlugin.isLoggedIn ())
			loggedIn ();
		else
			loginFailed ();

		var permissions = new string[]{"user_about_me", "publish_stream", "user_photos" };
		
		FacebookPlugin.init ("477364828962291", permissions);
		FacebookPlugin.listener = this;

		onClick += HandleonClick;
	}
	void HandleonClick (object sender, EventArgs e)
	{
		if (!logged) {
			FacebookPlugin.login ();
		} else {
			FacebookPlugin.logout ();
		}
		//print("unity: "+permissions[1]);
	}

	#region FacebookPluginListener implementation

	public void loggedIn ()
	{
		sprite.RealColor = Color.white;
		logged = true;
	}

	public void loginFailed ()
	{
		sprite.RealColor = Color.gray;
		logged = false;
	}


	public void userInfoArrived (FacebookPlugin.Friend friend)
	{
	}


	public void scoreListArrived (FacebookPlugin.Friend[] friends)
	{

	}

	public void scoreListFailed ()
	{

	}

	#endregion
}

