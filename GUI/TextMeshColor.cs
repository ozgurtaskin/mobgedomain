﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class TextMeshColor : MonoBehaviour
{
    public Renderer textRenderer;
    public Color color;
    void Start()
    {
        textRenderer.material.color = color;
        this.DestroyObj(this);
    }
}
