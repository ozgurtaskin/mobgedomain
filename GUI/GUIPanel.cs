using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class GUIPanel : Button4 {

    public event EventHandler<GUIPanelEventArgs> Resized;

    private static HashSet<GUIPanel> panels = new HashSet<GUIPanel>();
    
    [SerializeField]
    private Rect bounds;
    [SerializeField]
    private float z;
    private int uniqueLayer;

    public Camera cam;
    private static Camera mainCam;
    private static Transform mainCamTr;
    public Transform camTr;
    protected Vector2 size;
    float aspect;



    public static void ensureCash()
    {
        mainCam = Camera.mainCamera;
        mainCamTr = mainCam.transform;
        //Debug.Log(mainCam);
    }
    void OnEnable()
    {
        cam.gameObject.active = true;
    }
    void OnDisable()
    {
        if (cam)
        {
            cam.gameObject.active = false;
        }
    }
    public override LayoutItemData Data
    {
        get
        {
            var v = new LayoutItemData(bounds.center, new Vector2(bounds.width, bounds.height));
            v.scaledOverScreen = true;
            return v;
        }
        set
        {
            Vector2 p = value.Position;
            Vector2 s = new Vector3(value.Size.x, value.Size.y, 1);
            
            //Vector2 ts = value.totalSize;
            Bounds = new Rect(p.x-s.x*0.5f, p.y-s.y*0.5f, s.x, s.y);
        }
    }
    public static Vector3 rayPosition(Ray r , float deltaZ){

        Vector3 fpos = r.origin + r.direction * (deltaZ / r.direction.z);
        return fpos;
    }

    public void init()
    {
        if (!tr || !cam)
        {
            tr = transform;
            cam = new GameObject().AddComponent<Camera>();
            camTr = cam.transform;
            camTr.parent = tr.parent;
            camTr.localRotation = Quaternion.identity;
            camTr.localPosition = Vector3.zero;

            //z = 5;
        }
    }
    public static bool intersectsRay(Vector3 center, Vector2 size, Ray r, out Vector3 intersectionOffset)
    {
        ensureCash();
        Vector3 pos = center;
        Vector3 campos = mainCamTr.position;
        float zdif = pos.z - campos.z;
        Vector3 fpos = rayPosition(r, zdif);
        intersectionOffset = fpos - pos;// -new Vector3(size.x * 0.5f, size.y * 0.5f, 0);
        //Debug.Log(intersectionOffset.z);
        return new Rect(-size.x * 0.5f, -size.y * 0.5f, size.x, size.y).Contains(new Vector2(intersectionOffset.x, intersectionOffset.y));
    }
    public bool intersectsRay(Ray r, out Vector3 intersectionOffset)
    {
        return intersectsRay(tr.position, size, r, out intersectionOffset);
    }
    /*public Vector2 Size
    {
        get { return size; }
    }*/
    public bool ButtonEnabled
    {
        get
        {
            return col;
        }
        set
        {
            if (value)
            {

                BoxCollider bc = col ? (BoxCollider)col : gameObject.AddComponent<BoxCollider>();
                col = bc;
                bc.size = new Vector3(size.x, size.y, 1);
            }
            else
            {
                if (col)
                {
                    this.DestroyObj(col);
                    col = null;
                }
            }

        }
    }
    protected override void Awake()
    {
        base.Awake();
        ensureCash();
        tr = transform;
        tr.parent = mainCamTr;
        tr.localPosition = Vector3.zero;
        tr.localRotation = Quaternion.identity;

        //mainCam.isOrthoGraphic = true;
        panels.Add(this);
        init();
        //cam.isOrthoGraphic = true;

        if (bounds.width * bounds.height > 0.000001f)
        {
            Bounds = bounds;
        }

        uniqueLayer = -1;
    }
    public float Z
    {
        get { return z; }
        set
        {
            z = value;
            //tr.localPosition = new Vector3(0, 0, z);
        }
    }
    protected override void OnDestroy()
    {
        base.OnDestroy();
        panels.Remove(this);
        if (camTr)
        {
            this.DestroyObj(camTr.gameObject);
        }
    }
    void setBounds(Rect r)
    {
    }
    public virtual Rect Bounds
    {
        get { return bounds; }
        set
        {
            //cam.fieldOfView = mainCam.fieldOfView;
            bounds = value;
            if (Application.isPlaying)
            {
                //bounds = new Rect(0, 0, 1, 1); 
                float n = mainCam.nearClipPlane;
                float f = mainCam.farClipPlane;
                // http://www.songho.ca/opengl/files/gl_projectionmatrix_eq16.png
                float tan = Mathf.Tan(mainCam.fov * Mathf.Deg2Rad / 2);
                float tan2n = 2 * n * tan;
                float l = (bounds.xMin - 0.5f) * tan2n * Layer.aspect(mainCam);
                float r = (bounds.xMax - 0.5f) * tan2n * Layer.aspect(mainCam);
                float lPlusR = l + r;
                float rminusL = r - l;
                float t = (bounds.yMin - 0.5f) * tan2n;
                float b = (bounds.yMax - 0.5f) * tan2n;
                float tPlusB = t + b;
                float tminusB = -t + b;

                /*Debug.Log(l);
                Debug.Log(r);
                Debug.Log(t);
                Debug.Log(b);
                */

                Matrix4x4 m = new Matrix4x4();
                m[0, 0] = 2 * n / rminusL; m[0, 1] = 0; m[0, 2] = lPlusR / rminusL; m[0, 3] = 0;
                m[1, 0] = 0; m[1, 1] = 2 * n / tminusB; m[1, 2] = tPlusB / tminusB; m[1, 3] = 0;
                m[2, 0] = 0; m[2, 1] = 0; m[2, 2] = -(f + n) / (f - n); m[2, 3] = -2 * f * n / (f - n);
                m[3, 0] = 0; m[3, 1] = 0; m[3, 2] = -1; m[3, 3] = 0;

                //m = m.transpose;


                m[0, 1] = 0;


                cam.rect = value;
                cam.projectionMatrix = m;

                float tanz = z / n;
                float tanzaspect = tanz;
                Rect rect = new Rect(l * tanzaspect, t * tanz, (rminusL) * tanzaspect, tminusB * tanz);
                Vector2 center = rect.center;
                tr.localPosition = new Vector3(center.x, center.y, z);
                size = new Vector2(rect.width, rect.height);

                //Debug.Log(mainCam.projectionMatrix);
                //Debug.Log(cam.projectionMatrix);

                ButtonEnabled = ButtonEnabled;
                if (Resized != null)
                {
                    Resized(this, new GUIPanelEventArgs());
                }
            }
                refreshEditorRect();
        }
    }
    public Vector2 Size
    {
        get { return size; }
    }
    private static void refreshMainCamCulling()
    {
        int i = 0;
        foreach (var v in panels)
        {
            if (v.uniqueLayer >= 0)
            {
                i |= 1 << v.uniqueLayer;
            }
        }
        mainCam.cullingMask = ~i;
    }
    public int UniqueLayer
    {
        get { return uniqueLayer; }
        set
        {
            uniqueLayer = value;
            refreshMainCamCulling();
            //mainCam.cu
        }
    }

    public void refreshEditorRect()
    {

        if (!Application.isPlaying)
        {
            var go = this;
            var drawer = go.GetComponent<EditorRectDrawer>();
            if (!drawer)
            {
                drawer = go.gameObject.AddComponent<EditorRectDrawer>();
            }
            drawer.rect = go.Bounds;
        }
    }
}
public class GUIPanelEventArgs : EventArgs
{

}