﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public enum ButtonState
{
	idle = 1
,
	pressed = 2
,    
}

public class Button2D : MonoBehaviour
{


	public event EventHandler<EventArgs> onClick;

	public GUITexture image;
	private ButtonState state = ButtonState.idle;
	private System.Object pocket;

	void Start ()
	{
        
		image = GetComponent<GUITexture> ();
        
	}

	public T Pocket<T> ()
	{
		return (T)pocket;
	}

	public void setPocket (System.Object p)
	{
		pocket = p;
	}

	private bool pointerInside (Vector3 pos)
	{
//        GameVariables.Log("button " + this);
		return image.GetScreenRect ().Contains (pos);
	}

	public ButtonState State {
		get {
			return state;
		}
	}

	void Update ()
	{
		if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer) {
			bool pressed = false;
			foreach (Touch t in Input.touches) {

				if (pointerInside (new Vector3(t.position.x, t.position.y, 0))) {
					pressed = true;
					break;
				}
			}
			state = pressed ? ButtonState.pressed : ButtonState.idle;
		} else {
			if (Input.GetMouseButtonDown (0) && state == ButtonState.idle && pointerInside (Input.mousePosition))
				state = ButtonState.pressed;
			else if (Input.GetMouseButtonUp (0) && state == ButtonState.pressed) {
				state = ButtonState.idle;
				performAction ();
			}
		}
	}

	public void performAction ()
	{

		if (onClick != null) {

			onClick (this, new EventArgs ());
		}
	}
}