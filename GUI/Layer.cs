﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class Layer : MonoBehaviour
{
    public float z;
    private Vector2 mPos;
    public Vector2 size;
    public Transform tr;
    void Awake()
    {
        tr = transform;
        Z = z;
    }
    public float Z
    {
		get{
			return z;
		}
        set
        {
            z = value;
            UnityEngine.Camera cam = UnityEngine.Camera.mainCamera;
            if (!tr)
            {
                tr = transform;
            }
            tr.parent = cam.transform;
            Vector3 p = getLeftBottomCornerPos(z, cam, Vector3.zero);
            tr.localPosition = p;
            size = -2 * new Vector2(p.x, p.y);
        }
    }
    public static float aspect(Camera cam) 
    {
#if UNITY_EDITOR


        System.Type T = System.Type.GetType("UnityEditor.GameView,UnityEditor");
        System.Reflection.MethodInfo GetSizeOfMainGameView = T.GetMethod("GetSizeOfMainGameView", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Static);
        System.Object Res = GetSizeOfMainGameView.Invoke(null, null);
        var v = (Vector2)Res;
        //Debug.Log(v.x / v.y + " " + cam.aspect);
        return v.x / v.y;

#else
        return cam.aspect;
#endif
    }
    public static Vector3 getLeftBottomCornerPos(float z, UnityEngine.Camera mainCam, Vector3 offset)
    {
        if (!UnityEngine.Camera.mainCamera.isOrthoGraphic)
        {
            float fov = mainCam.fieldOfView / 2;
            float ar = aspect(mainCam);
            float y = z * Mathf.Tan(fov * Mathf.PI / 180);
            return new Vector3(-y * ar, -y, z) + offset;
        }
        else
        {
            float ar = aspect(mainCam);
            float y = mainCam.orthographicSize;
            return new Vector3(-y * ar, -y, z) + offset;
        }
    }
    public Vector2 MousePosition
    {
        get
        {
            return mPos;
        }
    }
    public Vector2 convertMousePos(Vector3 mpos)
    {
        float w = Screen.width;
        //float h = Screen.height;
        float screenLayerRatio = w / size.x;
        Vector3 p = mpos;
        Vector2 p2 = new Vector2(p.x, p.y);
        mpos = p2 / screenLayerRatio;
        return mpos;
    }
    void Update()
    {
        mPos = convertMousePos(Input.mousePosition);
    }
}
