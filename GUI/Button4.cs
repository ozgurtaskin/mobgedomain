using UnityEngine;
using System.Collections;
using System;

public class Button4 : ALayoutBehaviour, IComparable<Button4>
{
    private int priority = 1;
    public bool holdInput = true;
    public Collider col;
    public ButtonState state;
    public event EventHandler<ButtonEventArgs> onClick;
    public event EventHandler<ButtonEventArgs> onPress;
    public event EventHandler<ButtonEventArgs> onRelease;
    public Transform tr;
	public object pocket;
    public Ray touchStart;
    public float minLooseFocusDistance = float.PositiveInfinity;


    public int Priority
    {
        get { return priority; }
        set { priority = value; ButtonManager.Singelton.Dirty = true; }
    }



    protected virtual void Awake()
    {
        //Debug.Log("awake");
        Priority = priority;
        state = ButtonState.idle;
        ButtonManager.Singelton.addButton(this);
        tr = transform;
    }
    void Reset()
    {
        tr = transform;
    }
    protected virtual void OnDestroy()
    {
        ButtonManager.Singelton.removeButton(this);
    }
    public bool Pressed
    {
        get { return state == ButtonState.pressed; }
    }
    void Start()
    {
        if (col == null)
        {
            col = collider;
        }
    }
    private void Log(object o)
    {
        //Debug.Log(o.ToString());
    }
    public virtual bool cursorDown(ButtonEventArgs args)
    {
        //Debug.Log("cursor down ?");
        if (col != null)
        {
            RaycastHit rh;
            if (col.Raycast(args.ray, out rh, float.PositiveInfinity))
            {
                state = ButtonState.pressed;
                if (onPress != null)
                {
                    onPress(this, args.copyWithPocket(pocket));
                }
                Log("pressed " + gameObject.name);
                touchStart = args.ray;
                return true;
            }
        }
        return false;
    }
    public virtual void cursorUp(ButtonEventArgs args)
    {
        state = ButtonState.idle;
        RaycastHit rh;
        if (onRelease != null)
        {
            onRelease(this, args.copyWithPocket(pocket));
        }
        Log("released " + gameObject.name);
        if (!args.touchCancel)
        {
            if (col.Raycast(args.ray, out rh, float.PositiveInfinity))
            {
                click(args);
            }
        }
    }

    public virtual bool cursorMoved(ButtonEventArgs args)
    {
        if ((args.ray.direction - touchStart.direction).sqrMagnitude > minLooseFocusDistance * minLooseFocusDistance)
        {
            return false;
        }
        return true;
    }

    public virtual void click(ButtonEventArgs args)
    {
        if (onClick != null)
        {
            onClick(this, args.copyWithPocket(pocket));
        }
        Log("clicked " + gameObject.name);
    }
    #region IComparable[Button4] implementation
    public int CompareTo(Button4 other)
    {
        return priority - other.priority;
    }
    #endregion

    public override LayoutItemData Data
    {
        get
        {
            return new LayoutItemData(tr.localPosition, tr.localScale);
        }
        set
        {
            tr.localPosition = value.Position;
            tr.localScale = new Vector3(value.Size.x, value.Size.y, 1);
        }
    }
}

public class ButtonEventArgs : EventArgs
{
    public Ray ray;
    public int touchId;
	public object pocket;
    public bool touchCancel;
    public ButtonEventArgs(Ray ray, int touchId)
    {
        touchCancel = false;
		//this.pocket = pocket;
        this.touchId = touchId;
        this.ray = ray;
    }
	public ButtonEventArgs copyWithPocket(object pocket){
		var v = new ButtonEventArgs(ray,touchId);
		v.pocket = pocket;
		return v;
	}
}