﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(GUIPanel))]
public class EGUIPanel : Editor
{
    public override void OnInspectorGUI()
    {
        var go = (GUIPanel)target;
        GUIPanel.ensureCash(); 
        //go.init();
        go.Bounds = EditorGUILayout.RectField(go.Bounds);
        go.Z = EditorGUILayout.FloatField(go.Z);

        go.refreshEditorRect();
        
    }
}

 