﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class ScrollPanel : GUIPanel
{
    private Vector2 contentSize;
    public Type type;
    public Transform items;
    private int touchId;
    private Vector2 touch;
    private Vector2 scrollPos;
    private Vector2 scrollSpeed;
    public float offBoundsSpeed = 0.4f;
    float acc
    {
        get
        {
            return Mathf.Min(size.x, size.y) * 20;
        }
    }

    protected override void Awake()
    {
        base.Awake();
        items = new GameObject("items").transform;
        items.parent = tr;

        type = Type.Horizontal;
        ButtonEnabled = true;

        scrollPos = Vector2.zero;
        scrollSpeed = Vector2.zero;

    }
    public Vector2 ScrollPos
    {
        get { return scrollPos; }
        set
        {
            scrollPos = value;
            if ((type & Type.Horizontal) != Type.None)
            {
                scrollPos.x = value.x;
                if (state != ButtonState.pressed)
                {
                    scrollSpeed.x += Time.deltaTime * calculateAcc(size.x, contentSize.x, scrollPos.x);
                }
                else
                {
                    scrollSpeed.x = 0;
                }
            }
            else
            {
                scrollPos.x = 0;
                scrollSpeed.x = 0;
            }
            if ((type & Type.Vertical) != Type.None)
            {
                scrollPos.y = value.y;
                if (state != ButtonState.pressed)
                {
                    scrollSpeed.y -= Time.deltaTime * calculateAcc(size.y, contentSize.y, -scrollPos.y);
                }
                else
                {
                    scrollSpeed.y = 0;
                }
                //Debug.Log(scrollSpeed.y);
            }
            else
            {
                scrollPos.y = 0;
                scrollSpeed.y = 0;
            }
            
            items.localPosition = new Vector3(scrollPos.x - size.x * 0.5f, scrollPos.y + size.y * 0.5f, 0);
        }
    }
    private float calculateAcc(float size, float contentSize, float scrollPos)
    {

        float requiredMove;
        if (outOfBounds(size, contentSize, scrollPos, out requiredMove))
        {
            return requiredMove < 0 ? -acc : acc;
        }
        return 0;
    }
    
    private bool outOfBounds(float size, float contentSize, float scrollPos, out float requiredMovement)
    {
        if (scrollPos > 0)
        {
            requiredMovement = -scrollPos;
            return true;
        }
        float dif = scrollPos + Mathf.Max(contentSize, size) - size;
        if (dif < 0)
        {
            requiredMovement = -dif;
            return true;
        }
        requiredMovement = 0;
        return false;

    }
    public override Rect Bounds
    {
        get
        {
            return base.Bounds;
        }
        set
        {
            base.Bounds = value;
            if (items != null)
            {
                ScrollPos = scrollPos;
            }
        }
    }
    public override bool cursorDown(ButtonEventArgs args)
    {
        Vector3 intersection;
        bool b = base.intersectsRay(args.ray, out intersection);
        if (b)
        {
            touch = intersection;
            //Debug.Log(intersection);
            state = ButtonState.pressed;

        }
        return b;
    }
    public override void cursorUp(ButtonEventArgs args)
    {
        base.cursorUp(args);
        cursorMove(args.ray);
    }
    void cursorMove(Ray r)
    {
        Vector3 intersection;
        base.intersectsRay(r, out intersection);
        Vector2 dif = new Vector2(intersection.x, intersection.y) - touch;
        float requiredMovement;
        if (outOfBounds(size.y, contentSize.y, -scrollPos.y, out requiredMovement))
        {
            dif.y *= offBoundsSpeed;
        }
        if (outOfBounds(size.x, contentSize.x, scrollPos.x, out requiredMovement))
        {
            dif.x *= offBoundsSpeed;
        }
        ScrollPos += dif;
        touch = new Vector2(intersection.x, intersection.y);

        scrollSpeed = scrollSpeed * 0.8f + dif * 0.2f / Time.deltaTime;
    }
    public override bool cursorMoved(ButtonEventArgs args)
    {
        var v = base.cursorMoved(args);
        cursorMove(args.ray);
        return v;
    }
    public float getContentSize(Type type)
    {
        float r;
        switch (type)
        {
            case Type.Horizontal:
                r = contentSize.x;
                break;
            case Type.Vertical:
                r = contentSize.y;
                break;
            default:
                throw new ArgumentException("argument can only be Horizontal or Vertical", "" + type.GetType());
        }
        return r;
    }
    void Update()
    {
        const float dump = 0.92f;
        if (state != ButtonState.pressed)
        {
            Vector3 pos = ScrollPos;
            float requiredMovement;
            float dy = scrollSpeed.y * Time.deltaTime;
            float dx = scrollSpeed.x * Time.deltaTime;
            if (outOfBounds(size.y, contentSize.y, -scrollPos.y, out requiredMovement))
            {
                requiredMovement = -requiredMovement;
                if (requiredMovement < 0)
                {
                    if (dy < requiredMovement)
                    {
                        dy = requiredMovement;
                        scrollSpeed.y = 0;
                    }
                }
                else
                {
                    if (dy > requiredMovement)
                    {
                        dy = requiredMovement;
                        scrollSpeed.y = 0;
                    }
                }
            }
            else
            {
                scrollSpeed.y *= dump;
            }
            if (outOfBounds(size.x, contentSize.x, scrollPos.x, out requiredMovement))
            {
                if (requiredMovement < 0)
                {
                    if (dx < requiredMovement)
                    {
                        dx = requiredMovement;
                        scrollSpeed.x = 0;
                    }
                }
                else
                {
                    if (dx > requiredMovement)
                    {
                        dx = requiredMovement;
                        scrollSpeed.x = 0;
                    }
                }
            }
            else
            {
                scrollSpeed.x *= dump;
            }
            pos.x += dx;

            pos.y += dy;

            ScrollPos = pos;
        }
    }
    public void setContentSize(Type type, float dim)
    {
        switch (type)
        {
            case Type.Horizontal:
                contentSize.x = dim;
                break;
            case Type.Vertical:
                contentSize.y = dim;
                break;
            default:
                throw new ArgumentException("argument can only be Horizontal or Vertical", "" + type.GetType());

        }
        ScrollPos = scrollPos;
    }
    public enum Type
    {
        None = 0x0,
        Horizontal = 0x1,
        Vertical = 0x2,
        Both = Horizontal | Vertical,
    }

}