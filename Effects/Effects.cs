using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class Effects
{
	public static List<Rigidbody> createExplosion(Vector3 pos, float radius, float force)
	{
		var cols = Physics.OverlapSphere(pos, radius);
		List<Rigidbody> rbs = new List<Rigidbody>(cols.Length);
		foreach (var col in cols)
		{
			var rb = col.attachedRigidbody;
			if (rb != null)
			{
				rbs.Add(rb);
				rb.AddExplosionForce(force, pos, radius, 0, ForceMode.Impulse);
			}
		}
		return rbs;
	}
}
