﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class AutoShurikenDestroy : MonoBehaviour
{
    public ParticleSystem ps;
    void Update()
    {
        if (!ps.isPlaying)
        {
            this.DestroyObj(gameObject);
        }
    }
}
