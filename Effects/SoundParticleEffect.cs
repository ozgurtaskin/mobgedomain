﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class SoundParticleEffect : MonoBehaviour
{
    public TypedAudioSource sound;
    public ParticleSystem system;
    public ParticleEmitter emitter;
    public bool autoDestroy = true;
    

    public Transform tr;

    void Awake()
    {
        tr = transform;
    }
    void Start()
    {
        if (sound)
        {
            sound.Audio.Play();
        }
    }
    public TypedAudioSource initTypedAudioSource(AudioSourceType type)
    {
        sound = gameObject.AddComponent<TypedAudioSource>();
        sound.type = type;
        return sound;
    }
    void Update()
    {
        if (autoDestroy)
        {
            if ((!sound || !sound.Audio.isPlaying) && (!emitter || !emitter.emit) && (!system || !system.isPlaying))
            {
                this.DestroyObj(gameObject);
            }
        }
    }
}
