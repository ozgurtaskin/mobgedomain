﻿using UnityEngine;


public class FixJointCorrector : MonoBehaviour
{

    public bool position;
    public bool rotation;

    private Vector3 v;
    private Vector3 euler;

    void Start()
    {
        v = transform.localPosition;
        euler = transform.localEulerAngles;
    }

    void EarlyUpdate()
    {
        if (position)
        {
            transform.localPosition = v;
        }
        if (rotation)
        {
            transform.localEulerAngles = euler;
        }
    }

    void Update()
    {

    }
}