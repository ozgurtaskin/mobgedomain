﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public static class Mathf2
{
    /// <summary>
    /// given:
    /// v = (k,k*tan(angle)) (1)
    /// t = dxz/k (2)
    /// dy = k*tan(angle)*t+t^2*g*0.5 (3)
    /// 
    /// solve v:
    /// dy = k*tan(angle)*dxz/k+(dxz/k)^2*0.5*g (1)+(2)=(4)
    /// dy = tan(angle)*dxz+dxz^2*0.5*g/k^2
    /// dy-tan(angle)*dxz=dxz^2*0.5*g/k^2
    /// k^2 = ( dxz^2 * 0.5 * g ) / ( dy - tan(angle) * dxz ) (5)
    /// k = sqrt(k^2)
    /// v = (k,k*tan(angle))
    /// 
    /// </summary>
    /// <param name="target">location of the target relative to projectile start location</param>
    /// <param name="angle"></param>
    /// <returns>required power to hit the target</returns>
    public static Vector3 projectilePower(Vector3 target, float angle, float g, out float magnitude, out float time)
    {
        float sinAng = Mathf.Sin(angle);
        float cosAng = Mathf.Cos(angle);
        return projectilePower(target, sinAng, cosAng, g, out magnitude, out time);
    }
    public static Vector3 projectilePower(Vector3 target, float sinAng, float cosAng, float g, out float magnitude, out float time)
    {
        float dxz2 = target.x * target.x + target.z * target.z;
        float dxz = Mathf.Sqrt(dxz2);
        float dy = target.y;
        float tanAng = sinAng / cosAng;
        float k2 = (dxz2 * 0.5f * g) / (dy - tanAng * dxz);
        float k = Mathf.Sqrt(k2);
        magnitude = k / cosAng;
        time = dxz / k;
        return new Vector3(target.x * k / dxz, k * tanAng, target.z * k / dxz);
    }

    public static bool LineIntersectsRect(Vector2 p1, Vector2 p2, Rect r,out Vector2 intpoint)
    {
        var x = 0f;
        var y = 0f;
        
        var result = LineIntersectsLine(p1, p2, new Vector2(r.x, r.y), new Vector2(r.x + r.width, r.y), out x, out y) ||
               LineIntersectsLine(p1, p2, new Vector2(r.x + r.width, r.y), new Vector2(r.x + r.width, r.y + r.height), out x, out y) ||
               LineIntersectsLine(p1, p2, new Vector2(r.x + r.width, r.y + r.height), new Vector2(r.x, r.y + r.height), out x, out y) ||
               LineIntersectsLine(p1, p2, new Vector2(r.x, r.y + r.height), new Vector2(r.x, r.y), out x, out y);
        if (LineIntersectsLine(p1, p2, new Vector2(r.x + r.width, r.y), new Vector2(r.x + r.width, r.y + r.height), out x, out y))
        {
            intpoint = (p1 * (1 - x) + p2 * (x));
            return result;
        }
        else if (LineIntersectsLine(p1, p2, new Vector2(r.x, r.y), new Vector2(r.x + r.width, r.y), out x, out y))
        {
            intpoint = (p1 * (1 - x) + p2 * (x));
            return result;
        }
        else if ( LineIntersectsLine(p1, p2, new Vector2(r.x + r.width, r.y + r.height), new Vector2(r.x, r.y + r.height), out x, out y))
        {
            intpoint = (p1 * (1 - x) + p2 * (x));
            return result;
        }
        else if  (LineIntersectsLine(p1, p2, new Vector2(r.x, r.y + r.height), new Vector2(r.x, r.y), out x, out y))
        {
            intpoint = (p1 * (1 - x) + p2 * (x));
            return result;
        }
        intpoint = (p1 * (1 - x) + p2 * (x));
        return result;

	}

    public static bool LineIntersectsLine(Vector2 l1p1, Vector2 l1p2, Vector2 l2p1, Vector2 l2p2,out float r,out float s)
    {
        float q = (l1p1.y - l2p1.y) * (l2p2.x - l2p1.x) - (l1p1.x - l2p1.x) * (l2p2.y - l2p1.y);
        float d = (l1p2.x - l1p1.x) * (l2p2.y - l2p1.y) - (l1p2.y - l1p1.y) * (l2p2.x - l2p1.x);
        r = 0;
        s = 0;
        if (d == 0)
        {
            return false;
        }

        r = q / d;

        q = (l1p1.y - l2p1.y) * (l1p2.x - l1p1.x) - (l1p1.x - l2p1.x) * (l1p2.y - l1p1.y);
        s = q / d;

        if (r < 0 || r > 1 || s < 0 || s > 1)
        {
            return false;
        }
        //Debug.Log("Line Crossed");
        return true;
        
    }

    public static float Distance(Vector2 p1, Vector2 p2)
    {
        return (p1-p2).magnitude;
    }
    public static float normalizeRad(float rad)
    {
        float num = Mathf.Repeat(rad, Mathf.PI * 2);
        if (num > Mathf.PI)
        {
            num -= Mathf.PI * 2;
        }
        return num;
    }
	public static float LerpRad (float a, float b, float t)
	{
        var num = normalizeRad(b - a);
		return a + num * Mathf.Clamp01 (t);
	}
    public static Vector3 snap(Vector3 v, float snapValue)
    {

        return new Vector3(snap(v.x, snapValue), snap(v.y, snapValue), snap(v.z, snapValue));
    }
    public static float snap(float f, float snapValue)
    {
        return Mathf.Round(f / snapValue) * snapValue;
    }
    /// <summary>
    /// reverse function of cdf of geometric distrubution with success probabilty p
    /// </summary>
    /// <param name="p"></param>
    /// <returns></returns>
    public static float geometricRandomValue(float p)
    {

        return Mathf.Log(1 - UnityEngine.Random.value, 1 - p);
    }
    /// <summary>
    /// aproaches 'to' by 'amount' from 'from', does not go other side of 'to'
    /// </summary>
    /// <param name="source"></param>
    /// <param name="target"></param>
    /// <param name="amount"></param>
    /// <returns></returns>
    public static float moveBy(float from, float to, float amount)
    {
        float dif = to - from;
        if (dif < 0)
        {
            return Mathf.Max(to, from - amount);
        }
        else
        {
            return Mathf.Min(to, from + amount);
        }
    }
    public static Vector2 moveByV2(Vector2 from, Vector2 to, Vector2 amount)
    {
        return new Vector2(moveBy(from.x, to.x, amount.x), moveBy(from.y, to.y, amount.y));
    }
}