﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class SpriteBomb : MonoBehaviour
{
    public string objectName;
    protected List<UnityEngine.Particle> particles;
    protected List<Rect> frames;
    
    //private Texture tex;
    public float lifeTime
    {
        get { return pe.minEnergy; }
        set
        {
            pe.minEnergy = value;
            pe.maxEnergy = value;
        }
    }
    //public float transperantStartTime;
    public float gravityWeight
    {
        get
        {
            return pa.force.y / Physics.gravity.y;
        }
        set
        {
            var force = pa.force;
            force.y = Physics.gravity.y * value;
            pa.force = force;
        }
    }
    public Vector2 velocity
    {
        get { return pe.localVelocity; }
        set { pe.localVelocity = value; }
    }
    public Vector2 randomVelocity
    {
        get { return pe.rndVelocity; }
        set { pe.rndVelocity = value; }
    }
    //public int debugSize;
    public float angularVelocity
    {
        get { return pe.angularVelocity; }
        set { pe.angularVelocity = value; }
    }
    public float randomAngularVelocity
    {
        get { return pe.rndAngularVelocity; }
        set { pe.rndAngularVelocity = value; }
    }
    public bool randomShape;

    public ParticleEmitter pe;
    public ParticleAnimator pa;
    public ParticleRenderer pr;
    protected virtual void Awake()
    {
        initParticle();
        particles = new List<UnityEngine.Particle>();
        frames = new List<Rect>();
        AutoDestroy = false;
    }
    void initParticle()
    {
        var go = gameObject;
        pe = (ParticleEmitter)go.AddComponent("EllipsoidParticleEmitter");
        pe.useWorldSpace = true;
        pe.emit = false;
        pa = go.AddComponent<ParticleAnimator>();
        pr = go.AddComponent<ParticleRenderer>();
    }
    public bool AutoDestroy;
    public int SheetIndex
    {
        set
        {
            var sheet = SpriteSheetManager.Singelton.getSheet(value);
            Material m = sheet.Material;
            pr.sharedMaterial = m;
            //pr.material.mainTextureScale = new Vector2(1, 1);
            //tex = pr.material.mainTexture;
        }
    }
    public void reset()
    {
        randomShape = true;
        lifeTime = 0.5f;
        //transperantStartTime = new Vector2(lifeTime , lifeTime )* 0.55f ;
        gravityWeight = 1.0f;
        velocity = Vector2.zero;
        randomVelocity =new Vector2(1,1)*45f;
        angularVelocity = 0;
        randomAngularVelocity = 24;
    }
    public void destroy(Vector3 pos3, float rot, Frame f, Vector2 anchor, Vector3 scale, float pieceSize)
    {
        var tl = f.topLeft;
        var br = f.bottomRight;
        var size = tl - br;
        
        //int tm = 3;
        //int tilex = tm*tex.width / ((int)fsize.x);
        //int tiley = tm*tex.height / ((int)fsize.y);
        //pr.uv
        //int maxLimit = 20;
        pa.force = new Vector3();
        //pe.Emit(
    }
    public void destroyg(Vector3 pos3, float rot, Frame f, Vector2 anchor, Vector3 scale, float pieceSize)
    {
		//return;
        Vector2 pos2 = new Vector2(pos3.x, pos3.y);

        Rect bounds = Sprite.calculateBounds(f, anchor);
        bounds.width *= scale.x;
        bounds.height *= scale.y;
        bounds.x *= scale.x;
        bounds.y *= scale.y;
        int xseg = (int)(bounds.width / pieceSize) + 1;
        int yseg = (int)(bounds.height / pieceSize) + 1;

        Vector2 psize = new Vector2(bounds.width /( xseg), bounds.height / ( yseg));
        Vector2 pcorner = new Vector2(bounds.xMin , bounds.yMin ) + psize / 2 + pos2;

        Vector2 uvcorner = new Vector2(f.topLeft.x, f.bottomRight.y);
        Vector2 uvsize = f.bottomRight - f.topLeft;
        Vector2 uvstep = new Vector2(uvsize.x / xseg, -uvsize.y / yseg);
//        Vector2 uvpsize = new Vector2(uvstep.x, uvstep.y);

        //Debug.Log("" + " " + xseg + " " + yseg);

        for (int i = 0; i < xseg; i++)
        {
            for (int j = 0; j < yseg; j++)
            {
                /*
                Particle p = new Particle();
                p.randomSeed = (short)UnityEngine.Random.Range(short.MinValue, short.MaxValue);
                p.time = 0;
                p.size = new Vector2(scale.x, scale.y);
                p.topLeft = uvcorner + new Vector2(uvstep.x * (i + 1), uvstep.y * (j + 1));
                p.bottomRight = new Vector2(p.topLeft.x - uvstep.x, p.topLeft.y - uvstep.y);
                Vector2 pos = pcorner + new Vector2(psize.x * i, psize.y * j);
                p.pos = new Vector3(pos.x, pos.y, pos3.z);
                p.rotation = rot;
                p.angularVelocity = raspeed();

                p.velocity = rspeed();
                particles.Add(p);
                 */


                
                UnityEngine.Particle p = new UnityEngine.Particle();
                //p.randomSeed = (short)UnityEngine.Random.Range(short.MinValue, short.MaxValue);
                p.energy = 0.5f;
                p.size = pieceSize;
                Rect frame = new Rect(uvstep.x * (i + 1), uvstep.y * (j), uvstep.x, -uvstep.y);
                Vector2 pos = pcorner + new Vector2(psize.x * i, psize.y * j);
                p.position = new Vector3(pos.x, pos.y, pos3.z);
                p.rotation = rot;
                p.angularVelocity = raspeed();

                p.velocity = rspeed();
                particles.Add(p);
                frames.Add(frame);
            }
        }
    }
    public void destroy(Sprite sprite, Vector3 scale, float pieceSize, bool destroyGameObject = false)
    { 
        //Debug.Log(sprite.name);
        Vector3 pos3 = sprite.thisTransform.position;
        float rot = sprite.thisTransform.getZAngle();
        Frame f = sprite.Frame;
        destroy(pos3, rot, f, sprite.anchor, scale, pieceSize);

        
        if (destroyGameObject)
        {
            Destroy(sprite.gameObject);
        }
    }
    protected Vector2 rspeed()
    {
        return velocity + new Vector2(UnityEngine.Random.Range(-randomVelocity.x, randomVelocity.x), UnityEngine.Random.Range(-randomVelocity.y, randomVelocity.y));
    }
    public List<UnityEngine.Particle> Particles
    {
        get
        {
            return particles;
        }
    }
    protected float raspeed()
    {
        return angularVelocity + UnityEngine.Random.Range(-randomAngularVelocity, randomAngularVelocity);
    }
    /*public override int PictureCount
    {
        get
        {
            return particles.Count;
        }
    }*/
    protected virtual void FixedUpdate() { }
    protected virtual void Update()
    {
        //pe.particles = particles.ToArray();
        //pr.uvTiles = frames.ToArray();
        //Debug.Log(particles.Count + " " + frames.Count);
        for (int i = 0; i < particles.Count; i++)
        {
            
            var p = particles[i];
            p.energy += Time.deltaTime;
            p.velocity += Physics.gravity * Time.deltaTime * gravityWeight;
            p.position += p.velocity * Time.deltaTime;
            p.rotation += p.angularVelocity * Time.deltaTime;
            particles[i] = p;
        }
    }
    /*
    public override int draw(SpriteSheet.DynamicArray<Vector3> verts, SpriteSheet.DynamicArray<Vector2> uv, SpriteSheet.DynamicArray<Color> colors, int index)
    {
        Particle p;int i = 0;
    returnAdress:
        while( i < particles.Count )
        {
            if (particles[i].time >= lifeTime)
            {
                p = particles[i];
                particles[i] = particles[particles.Count - 1];
                particles.RemoveAt(particles.Count - 1);
                goto draw;
                
            }
            else
            {

                p = particles[i];

                i++;

                goto draw;

            }
        }
        debugSize = particles.Count;

        if (AutoDestroy && particles.Count <= 0)
        {
            Destroy(this);
        }
        return index;
        draw:
        int i4 = index * 4;

        System.Random r = new System.Random(p.randomSeed);

        Vector2 size = p.bottomRight - p.topLeft;
        Vector2 hs = new Vector2(size.x * p.size.x, size.y * p.size.y) * (0.005f);

        float cos = Mathf.Cos(p.rotation);
       // float cos = 1;// Mathf.Cos(p.rotation);
        float sin = Mathf.Sin(p.rotation);
       // float sin = 0;// Mathf.Sin(p.rotation);
        Vector2 hcos = new Vector2(hs.x * cos, hs.y * cos);
        Vector2 hsin = new Vector2(hs.x * sin, -hs.y * sin);
        if (randomShape)
        {
            int max = 1500; float div = 0.001f;
            float r1 = r.Next(max) * div;
            float r2 = r.Next(max) * div;
            float r3 = r.Next(max) * div;
            float r4 = r.Next(max) * div;
            float r5 = r.Next(max) * div;
            float r6 = r.Next(max) * div;
            float r7 = r.Next(max) * div;
            float r8 = r.Next(max) * div;
            //Vector2 hcos = hs;
            //Vector2 hsin = new Vector2(0, 0); ;

            verts[i4 + 0] = new Vector3(p.pos.x + (-hcos.x - hsin.y) * r1, p.pos.y + (-hcos.y - hsin.x) * r5, p.pos.z);//PB
            verts[i4 + 1] = new Vector3(p.pos.x + (+hcos.x - hsin.y) * r2, p.pos.y + (-hcos.y + hsin.x) * r6, p.pos.z);//BP
            verts[i4 + 2] = new Vector3(p.pos.x + (+hcos.x + hsin.y) * r3, p.pos.y + (+hcos.y + hsin.x) * r7, p.pos.z);//Bb
            verts[i4 + 3] = new Vector3(p.pos.x + (-hcos.x + hsin.y) * r4, p.pos.y + (+hcos.y - hsin.x) * r8, p.pos.z);//bB
        }
        else
        {
            verts[i4 + 0] = new Vector3(p.pos.x + (-hcos.x - hsin.y) , p.pos.y + (-hcos.y - hsin.x) , p.pos.z);//PB
            verts[i4 + 1] = new Vector3(p.pos.x + (+hcos.x - hsin.y) , p.pos.y + (-hcos.y + hsin.x) , p.pos.z);//BP
            verts[i4 + 2] = new Vector3(p.pos.x + (+hcos.x + hsin.y) , p.pos.y + (+hcos.y + hsin.x) , p.pos.z);//Bb
            verts[i4 + 3] = new Vector3(p.pos.x + (-hcos.x + hsin.y) , p.pos.y + (+hcos.y - hsin.x) , p.pos.z);//bB
        }
        Color color = this.color;
        color.a = p.time > transperantStartTime ?
            ((lifeTime - p.time) / (lifeTime - transperantStartTime)) :
            1;

        colors[i4 + 0] = color;
        colors[i4 + 1] = color;
        colors[i4 + 2] = color;
        colors[i4 + 3] = color;

        uv[i4 + 0] = new Vector2(p.topLeft.x, p.bottomRight.y);
        uv[i4 + 1] = new Vector2(p.bottomRight.x, p.bottomRight.y);
        uv[i4 + 2] = new Vector2(p.bottomRight.x, p.topLeft.y);
        uv[i4 + 3] = new Vector2(p.topLeft.x, p.topLeft.y);



        index++;

        goto returnAdress;
    }
    */
    public struct Particle
    {
        public short randomSeed;
        public Vector2 bottomRight;
        public Vector2 topLeft;
        public Vector3 pos;
        public Vector3 velocity;
        public float rotation;
        public float angularVelocity;
        public float time;
        public Vector2 size;
    }
}

