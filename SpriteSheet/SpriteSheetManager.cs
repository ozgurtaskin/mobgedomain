﻿//#define TEST_SPEED

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization;

[AddComponentMenu("Sprite Sheet/Manager")]
[ExecuteInEditMode]
public class SpriteSheetManager : MonoBehaviour
{
    public const int MAX_SHEET_COUNT = 10000;

    private static SpriteSheetManager singelton;
    [SerializeField]
    private bool initialized = false;
    [SerializeField]
    public int nextIndex;
    public List<SpriteSheet> sheets;
	
	//static int c=0;



    [SerializeField]
    private int nextReplicaIndex;
    public List<SpriteSheet> replicaSheets;
    public bool firstTime;
    public bool sheetsChanged;

	private static bool useNative;
	public static bool UseNative {
		get {

			return useNative;
		}
		set {
			if(Application.platform == RuntimePlatform.IPhonePlayer){
				useNative = value;
			}
			else{
				useNative = false;
			}
		}
	}
	
	public List<SpriteSheet> sortedSheets;
	bool gldraw{get{return SpriteSheet.gldraw;}}
    void Awake()
    {
		if(gldraw){
			this.transform.RemoveAllChildren();
			sortedSheets = new List<SpriteSheet>(sheets.Count);
			sortedSheets.AddRange(sheets);
			sortedSheets.Sort();
		}
		/*MeshRenderer mr = GetComponent<MeshRenderer>();
		if(!mr){
			mr = gameObject.AddComponent<MeshRenderer>();
		}
		foreach (var v in sortedSheets){
			mr.material = v.Material;
		}
		*/
		/*if(Camera.mainCamera.GetComponent<SheetDrawer>() == null){
			Camera.mainCamera.gameObject.AddComponent<SheetDrawer>().manager = this;
		}*/
        firstTime = true;
        sheetsChanged = false;
        if (!initialized)
        {
            sheets = new List<SpriteSheet>();
            nextIndex = 0;
            initialized = true;
        }
        if (replicaSheets == null)
        {
            replicaSheets = new List<SpriteSheet>();
            nextReplicaIndex = 0;
        }
        singelton = this;
    }
	void OnRenderObject(){
#if TEST_SPEED
		SpriteSheet.gldrawTime.start();
#endif
		if(SpriteSheet.gldraw){
			/*
			var objs = FindObjectsOfType(typeof(MeshRenderer));
			StringBuilder sb = new StringBuilder(""+c++);
			foreach(MeshRenderer obj in objs){
				if(obj.isVisible){
				sb.Append(obj.name + " -- " );}
			}
			Debug.Log(sb);
			*/
			foreach(var v in sortedSheets){
                if (v)
                {
                    v.Material.SetPass(0);
                    GL.Begin(GL.QUADS);
                    for (int i = 0; i < v.Count; i++)
                    {
                        int i4 = i * 4;
                        //int i6 = i*6;
                        GL.Color(v.colors[i4]);
                        GL.TexCoord(v.uv[i4 + 0]);
                        GL.Vertex(v.verts[i4 + 0]);
                        GL.TexCoord(v.uv[i4 + 1]);
                        GL.Vertex(v.verts[i4 + 1]);
                        GL.TexCoord(v.uv[i4 + 2]);
                        GL.Vertex(v.verts[i4 + 2]);
                        GL.TexCoord(v.uv[i4 + 3]);
                        GL.Vertex(v.verts[i4 + 3]);

                    }
                    GL.End();
                }
			}
			//Debug.Log("===");
		}
#if TEST_SPEED
		SpriteSheet.gldrawTime.endAndAdd();
		SpriteSheet.gldrawTime.increaseCount();
#endif
	}
    private static int addSheetToList(SpriteSheet sheet, List<SpriteSheet> sheets, ref int nextIndex)
    {
        int oldI = nextIndex;
        if (nextIndex == sheets.Count)
        {
            nextIndex++;
            sheets.Add(sheet);
            return oldI;
        }
        sheets[nextIndex] = sheet;
        while (nextIndex < sheets.Count && sheets[nextIndex] != null)
        {
            nextIndex++;
        }
        return oldI;
    }
    public int addSheet(SpriteSheet sheet)
    {
        int oo = addSheetToList(sheet, sheets, ref nextIndex);

        singelton.sheetsChanged = true;
        return oo;
    }
    public int addReplicaSheet(SpriteSheet sheet)
    {
        return addSheetToList(sheet, replicaSheets, ref nextReplicaIndex);
    }
#if(UNITY_EDITOR)
    void Update()
    {
        if (!Application.isPlaying)
        {
            singelton = this;
        }

    }
    void OnGUI()
    {
        if (!Application.isPlaying)
        {
            Singelton = this;
        }
    }
#endif
    public static SpriteSheetManager Singelton
    {
        get
        {
            if (singelton == null)
            {
                singelton = (SpriteSheetManager)GameObject.FindObjectOfType(typeof(SpriteSheetManager));
            }
            /*if (singelton == null)
            {
                singelton = UnityEngine.Camera.mainCamera.GetComponent<SpriteSheetManager>();
                if (singelton == null)
                {
                    singelton = UnityEngine.Camera.mainCamera.gameObject.AddComponent<SpriteSheetManager>();
                }
                if (singelton.frames == null)
                {
                    singelton.frames = new List<Picture>();
                }
                if (singelton.sheets == null)
                {
                    singelton.sheets = new List<SpriteSheet>();
                }
            }*/
            return singelton;
        }
        set
        {
            singelton = value;
        }
    }
    void cancelLateUpdate()
    {
        foreach (SpriteSheet ss in sheets)
        {
            Graphics.DrawMesh(ss.mesh, Vector3.zero, Quaternion.identity, ss.material, 0);
        }
    }
    public SpriteSheet getOriginalSheet(int index)
    {
        return sheets[index / MAX_SHEET_COUNT - 1];
    }
    public SpriteSheet getSheet(int index)
    {
        if (index >= MAX_SHEET_COUNT)
        {
            return replicaSheets[index % MAX_SHEET_COUNT];
        }
        return sheets[index];
    }
    private static void removeSheetFromList(int index, List<SpriteSheet> sheets, ref int nextIndex)
    {
        sheets[index] = null;
        if (index < nextIndex)
        {
            nextIndex = index;
        }
    }
    public void removeSheet(int index)
    {
        removeSheetFromList(index, sheets, ref nextIndex);
        singelton.sheetsChanged = true;
    }
    public void removeReplicaSheet(int index)
    {
        removeSheetFromList(index, replicaSheets, ref nextReplicaIndex);
    }
}