﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[AddComponentMenu("Sprite Sheet/Replica Sheet")]
[Serializable]
[ExecuteInEditMode]
public class ReplicaSpriteSheet : SpriteSheet
{
    public int originalIndex;
    protected override int takeIndex()
    {
        return SpriteSheetManager.Singelton.addReplicaSheet(this);
    }
    protected override void removeIndex()
    {
        SpriteSheetManager.Singelton.removeReplicaSheet(index);
    }
    public override string Path
    {
        get
        {
            return SpriteSheetManager.Singelton.sheets[originalIndex].Path;
        }
        set
        {
            //base.Path = value;
        }
    }
    public override int Index
    {
        get
        {
            return (1 + originalIndex) * SpriteSheetManager.MAX_SHEET_COUNT + index;
        }
    }
    public override Dictionary<string, Frame> FrameData
    {
        get
        {
            return SpriteSheetManager.Singelton.sheets[originalIndex].FrameData;
        }
    }
}
