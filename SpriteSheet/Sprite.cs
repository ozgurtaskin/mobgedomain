﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Linq;
using System.Text;
using UnityEngine;
using System.IO;
using System.Xml;

[AddComponentMenu("Sprite Sheet/Sprite")]
[ExecuteInEditMode]
[Serializable]
public class Sprite : MonoBehaviour, IComparable<Sprite>
{
    [SerializeField]
    private int sheet;
    [SerializeField]
    protected Frame frame;
    //public int index = -1;
    public Transform thisTransform;
    [SerializeField]
    public BoxCollider thisCollider;
    public Vector2 anchor;
	protected Vector3[] fixedPoints;
	[SerializeField]
	private bool isfixed;



    public Color color = new Color(0.5f, 0.5f, 0.5f, 0.5f);

    public float z;
    //private Sprite n;
    void kkkOnDisable()
    {
#if(UNITY_EDITOR)
        if (!(Application.isPlaying))
        {
            GameObject g = new GameObject("holder");
            g.transform.parent = transform;
            FrameNameHolder fnh = g.AddComponent<FrameNameHolder>();
            fnh.setFrameAndName(this);
        }   
#endif
    }
    public Transform tr
    {
        get
        {
            return thisTransform;
        }
    }
    public void fitInto(Vector2 size, bool keepRatio = true)
    {
        var fsize = Frame.size;
        Vector2 scl2 = new Vector2(size.x / fsize.x, size.y / fsize.y);
        if (keepRatio)
        {
            var scl = Mathf.Min(scl2.x, scl2.y);
            thisTransform.localScale = new Vector3(scl, scl, scl);
        }
        else
        {
            thisTransform.localScale = new Vector3(scl2.x, scl2.y, 1);
        }
    }
    public Color RealColor { 
		get {
			var value = color;
			return new Color(value.r * 2, value.g * 2, value.b * 2, value.a * 2);
		}
		set { color = new Color (value.r / 2, value.g / 2, value.b / 2, value.a / 2); }
	}
    /*public Sprite next
    {
        get
        {
            return n;
        } 
        set
        {
            n = value;
        }
    }*/
    public void log(object o)
    {
        Debug.Log(o);
    }
    public static Rect calculateBounds(Frame frame, Vector2 anchor)
    {
        Vector2 size = frame.topLeft - frame.bottomRight;
        size *= 0.01f;
        Vector2 center = frame.anchor * 0.01f;
        center += new Vector2(size.x * anchor.x, size.y * anchor.y);
        Rect r = new Rect(center.x + size.x / 2, center.y + size.y / 2, -size.x, -size.y);
        return r;
    }
	public bool Fixed
	{
		get{
			return isfixed;
		}
		set{
			isfixed = value;
			fixedPoints = null;

			//isfixed = false;
		}

	}
    public Rect LocalBounds
    {
        get
        {
            return calculateBounds(frame, anchor);
        }
    }
    public void resizeCollider(float sizeZ = 0.1f)
    {
        Rect r = LocalBounds;
        Vector2 center = r.center;
        thisCollider.size = new Vector3(r.width, r.height, sizeZ);
        thisCollider.center = new Vector3(center.x, center.y, 0);
    }
    public void setSheetWithIndex(int index)
    {
        SheetIndex = index;
        
    }
    public BoxCollider Collider
    {
        get
        {
            return thisCollider;
        }
    }
    public void addCollider(float sizeZ = 0.1f)
    {
        thisCollider = gameObject.AddComponent<BoxCollider>();
        resizeCollider(sizeZ);
    }
    public void removeCollider()
    {
        this.DestroyObj(thisCollider);
        //Destroy(collider);
        thisCollider = null;
    }
    public void setFrameName(string name)
    {
        Frame f;
        bool b = OriginalSheet.FrameData.TryGetValue(name, out f);
        if (b)
        {
            //Frame fff = Frame;
                Frame = f;
            //Debug.Log(" " + Frame.topLeft);
            //Debug.Log("setting: " +fff.topLeft );
        }
    }
    public Frame Frame
    {
        get
        {
            return frame;
        }
        set
        {
            frame.anchor = value.anchor;
            frame.bottomRight = value.bottomRight;
            frame.topLeft = value.topLeft;
        }
    }
    protected SpriteSheet OriginalSheet
    {
        get
        {
            //Debug.Log(this);
            if (SheetIndex >= SpriteSheetManager.MAX_SHEET_COUNT)
            {
                return SpriteSheetManager.Singelton.getOriginalSheet(sheet);
            }
            else
            {
                return Sheet;
            }
        }
    }
    public SpriteSheet Sheet
    {
        get
        {
            return SpriteSheetManager.Singelton.getSheet(sheet);
        }
    }
    public int SheetIndex
    {
        get
        {
            return sheet;
        }
        set
        {
            sheet = value;
            if (frame == null)
            {
                //Debug.Log(name+" initializing frame");
                frame = new Frame();
				Fixed = false;
			}
			if(thisTransform == null)
				thisTransform = transform;
			if (sheet != value)
            {
                //if (sheet != null)
                {
                    //sheet.removeSprite(index);
                    //index = -1;
                }
                //if (sheet != null)
                {
                    
                    //index = sheet.addSprite(this);
                }
            }
        }
    }

    public void duplicated()
    {

        /*SpriteSheet old = sheet;
        sheet = null;
        //index = -1;
        Sheet = old;*/
    }
    public virtual int PictureCount
    {
        get
        {

            return 1;
        }
    }
    //private Vector3 oldPos;
    public void Update()
    {
        //Debug.Log(name);
        //oldPos = thisTransform.position;
        //if (sheet != null)
        {
            //Debug.Log(name);
            z = thisTransform.position.z;
			/*if(this is WavingSprite)
			{
				Debug.Log(name);
			}*/
#if UNITY_EDITOR
            if (SpriteSheetManager.Singelton != null)
            {
                Sheet.AddSprite(this, PictureCount);
            }
#else
            Sheet.AddSprite(this, PictureCount);
#endif

        }

    }
	private void multiply (ref Vector2 a, ref Vector3 b)
	{
		a.x *= b.x;
		a.y*=b.y;
	}
    private bool insideRotLimit()
    {
		float limit = 0.005f;
        return thisTransform.rotation.z < limit && thisTransform.rotation.z > -limit;
    }
    void bltrg(Vector2 topLeft,Vector2 bottomRight, out Vector2 bl, out Vector2 trg)
    {
        Frame f = frame;
        Vector2 realSize = topLeft - bottomRight;
        Vector2 hs = realSize * 0.5f;
        Vector2 tr = new Vector2(realSize.x * anchor.x, realSize.y * anchor.y);
        trg = hs + f.anchor + tr;
        bl = -hs + f.anchor + tr;
    }
    private int drawi(SpriteSheet.DynamicArray<Vector3> verts, SpriteSheet.DynamicArray<Vector2> uv, SpriteSheet.DynamicArray<Color> colors, int index)
    {

        //

        Frame f = frame;
        Vector2 topLeft = f.topLeft;
        Vector2 bottomRight = f.bottomRight;
        Vector2 trg;
        Vector2 bl;

        bltrg(topLeft, bottomRight, out bl, out trg);

        int i4 = index * 4;
        if (insideRotLimit())
        {
            Vector3 scl = thisTransform.lossyScale;
            Vector3 pos = thisTransform.position;




            multiply(ref trg, ref scl);
            multiply(ref bl, ref scl);


            verts[i4 + 2] = (new Vector3(bl.x, trg.y, 0) * 0.0100f) + pos;
            verts[i4 + 1] = (new Vector3(bl.x, bl.y, 0) * 0.0100f) + pos;
            verts[i4 + 3] = (new Vector3(trg.x, trg.y, 0) * 0.0100f) + pos;
            verts[i4 + 0] = (new Vector3(trg.x, bl.y, 0) * 0.0100f) + pos;
            //Color color = new Color(0.0f,0.0f,0.5f,0.5f);
            Color color = this.color;
            colors[i4 + 0] = this.color;
            colors[i4 + 1] = this.color;
            colors[i4 + 2] = this.color;
            colors[i4 + 3] = color;

            //Debug.Log(mat);
            /*
        */
        }
        else
        {

            colors[i4 + 0] = color;
            colors[i4 + 1] = color;
            colors[i4 + 2] = color;
            colors[i4 + 3] = color;

            Matrix4x4 mat = thisTransform.localToWorldMatrix;

            verts[i4 + 3] = mat.MultiplyPoint3x4(new Vector3(trg.x, trg.y, 0) * 0.0100f);
            verts[i4 + 2] = mat.MultiplyPoint3x4(new Vector3(bl.x, trg.y, 0) * 0.0100f);
            verts[i4 + 1] = mat.MultiplyPoint3x4(new Vector3(bl.x, bl.y, 0) * 0.0100f);
            verts[i4 + 0] = mat.MultiplyPoint3x4(new Vector3(trg.x, bl.y, 0) * 0.0100f);
        }

        fillUvs(uv, i4, topLeft, bottomRight);

        return index + 1;
    }
    protected void fillUvs(IList<Vector2> uv, int i4, Vector2 topLeft, Vector2 bottomRight)
    {
        uv[i4 + 0] = new Vector2(topLeft.x, topLeft.y);
        uv[i4 + 1] = new Vector2(bottomRight.x, topLeft.y);
        uv[i4 + 2] = new Vector2(bottomRight.x, bottomRight.y);
        uv[i4 + 3] = new Vector2(topLeft.x, bottomRight.y);
    }
	public virtual int draw (SpriteSheet.DynamicArray<Vector3> verts, SpriteSheet.DynamicArray<Vector2> uv, SpriteSheet.DynamicArray<Color> colors, int index)
	{


		if (this == null) {
			return index + PictureCount; 
			//Debug.Log("Asd");
			//new GameObject("hey").transform.position = oldPos;
			//Debug.Break();
		}
		/*if (SpriteSheetManager.UseNative) {
			return SpriteSheet.spriteDraw(Sheet.sheet, new Matrix3x4(mat),new SFrame(f),anchor, color ,index);
		} 
		else*/
		{
			if(isfixed)
			{
				int i4 = index * 4;
				if(fixedPoints == null)
				{
					drawi(verts, uv, colors, index);
					fixedPoints = new Vector3[4];
                    updateFixedPoints(i4, verts);
				}
				else{
					Frame f = Frame;
					
					verts[i4+0] = fixedPoints[0];
					verts[i4+1] = fixedPoints[1];
					verts[i4+2] = fixedPoints[2];
					verts[i4+3] = fixedPoints[3];
					//Color color = new Color(0.5f,0.0f,0.0f,0.5f);
					Color color = this.color;
					colors [i4 + 0] = this.color;
					colors [i4 + 1] = this.color;
					colors [i4 + 2] = this.color;
					colors [i4 + 3] = color;
					
					uv [i4 + 0] = new Vector2 (f.topLeft.x, f.topLeft.y);
					uv [i4 + 1] = new Vector2 (f.bottomRight.x, f.topLeft.y);
					uv [i4 + 2] = new Vector2 (f.bottomRight.x, f.bottomRight.y);
					uv [i4 + 3] = new Vector2 (f.topLeft.x, f.bottomRight.y);
				}
				return index + 1;
			}
			else{

				return drawi(verts, uv, colors, index);
			}
		}
    }
    protected void updateFixedPoints(int i4, IList<Vector3> verts)
    {
        fixedPoints[0] = verts[i4 + 0];
        fixedPoints[1] = verts[i4 + 1];
        fixedPoints[2] = verts[i4 + 2];
        fixedPoints[3] = verts[i4 + 3];
    }

    public int CompareTo(Sprite other)
    {
        return (int)((other.z - z) * 100000);
    }

}
