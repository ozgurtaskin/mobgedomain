﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEditor;
using System.IO;

[CustomEditor(typeof(ReplicaSpriteSheet))]
public class EReplicaSpriteSheet : Editor
{

    public override void OnInspectorGUI()
    {
        ReplicaSpriteSheet sheet = (ReplicaSpriteSheet)target;

        sheet.register();

        int selected = 0;

        List<GUIContent> options = new List<GUIContent>();
        List<int> indexes = new List<int>();
        List<SpriteSheet> sheets = SpriteSheetManager.Singelton.sheets;

        for (int i = 0; i < sheets.Count; i++)
        {
            if (sheets[i] != null)
            {
                if (sheets[i].Index == sheet.originalIndex)
                {
                    selected = indexes.Count;
                }
                if (sheets[i].Path.Count() > 2)
                {
                    FileInfo fi = new FileInfo(sheets[i].Path);
                    options.Add(new GUIContent("" + fi.Name));
                    indexes.Add(i);
                }
            }
        }

        sheet.originalIndex = indexes[EditorGUILayout.Popup(new GUIContent("Original Sheet"), selected, options.ToArray())];

        EditorGUILayout.LabelField("Index:", "" + sheet.Index);
        sheet.Material = (Material)EditorGUILayout.ObjectField("Material", sheet.Material, typeof(Material),true);
        sheet.Layer = EditorGUILayout.IntField("Layer", sheet.Layer);
    }
}

