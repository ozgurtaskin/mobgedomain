using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEditor;
using System.IO;

[CustomEditor(typeof(Sprite))]
public class ESprite : Editor
{
    String search = "";
    private GUIContent[] frameNameList;
    private int selectedindex = 0;
    Frame[] listedFrames;
    public override void OnInspectorGUI()
    {
        Sprite go = (Sprite)target;
        /*if (go.index >= 0)
        {
            if (go.Sheet.sprites[go.index] != go)
            {
                go.duplicated();
            }
        }*/
        int selected = 0;
        
        List<GUIContent> options = new List<GUIContent>();
        List<int> indexes = new List<int>();
        List<SpriteSheet> sheets = new List<SpriteSheet>();
        if (SpriteSheetManager.Singelton == null)
        {
            SpriteSheetManager.Singelton = (SpriteSheetManager)FindObjectOfType(typeof(SpriteSheetManager));
        }
        if (SpriteSheetManager.Singelton != null)
        {
            sheets.AddRange(SpriteSheetManager.Singelton.sheets);
            sheets.AddRange(SpriteSheetManager.Singelton.replicaSheets);

            for (int i = 0; i < sheets.Count; i++)
            {
                if (sheets[i] != null)
                {
                    if (sheets[i] == go.Sheet)
                    {
                        selected = indexes.Count;
                    }
                    if (sheets[i].Path.Count() > 2 )
                    {
                        FileInfo fi = new FileInfo(sheets[i].Path);

                        if (sheets[i].Index >= SpriteSheetManager.MAX_SHEET_COUNT)
                        {
                            options.Add(new GUIContent("Replica " + (sheets[i].Index % SpriteSheetManager.MAX_SHEET_COUNT) + fi.Name));
                        }
                        else
                        {
                            options.Add(new GUIContent("" + fi.Name));
                        }
                        indexes.Add(i);
                    }
                }
            }
        }
        if (options.Count == 0)
        {
            EditorGUILayout.LabelField("Please add ", "a sprite sheet to the scene");
        }
        else
        {
            int newShitIndex = EditorGUILayout.Popup(new GUIContent("Sheet: "), selected, options.ToArray());
            string newSearch = EditorGUILayout.TextField("Search String: ", search);
            if (frameNameList == null || newSearch != search || newShitIndex != go.SheetIndex)
            {
                search = newSearch;

			int iii = indexes[newShitIndex];
                	go.SheetIndex = iii;




                Dictionary<string, Frame> dic = go.Sheet.FrameData;
                List<GUIContent> goptions = new List<GUIContent>(dic.Count);
                Dictionary<string, Frame>.KeyCollection keys = dic.Keys;
                Dictionary<string, Frame>.ValueCollection values = dic.Values;
                listedFrames = new Frame[dic.Count];
                int count = 0;
                selectedindex = 0;
                for (int i = 0; i < keys.Count; i++)
                {
                    bool b = false;
                    if (search != "")
                    {
                        if (keys.ElementAt(i).ToLower().IndexOf(search.ToLower()) >= 0)
                        {
                            b = true;
                        }
                    }
                    else
                    {
                        b = true;
                    }
                    //Debug.Log("" + values.ElementAt(i).bottomRight);
                    if (b)
                    {
                        goptions.Add(new GUIContent(keys.ElementAt(i)));
                        listedFrames[count] = values.ElementAt(i);
                        
                        if (values.ElementAt(i) == go.Frame)
                        {
                            selectedindex = count;
                        }
                        count++;
                    }
                }
                frameNameList = goptions.ToArray();
            }
            selectedindex = EditorGUILayout.Popup(new GUIContent("Frame: "), selectedindex, frameNameList);
            /*if (GUILayout.Button("Save Frame Names"))
            {
                saveFrameNames(go);
            }*/
            //Debug.Log("count " + listedFrames.Count() +" selected " + selectedindex);
            if (frameNameList.Count() == 0)
            {
                //Debug.Log("vat" + selectedindex);
            }
            else
            {
                go.Frame = listedFrames.ElementAt(selectedindex);
            }
        }
        go.anchor = EditorGUILayout.Vector2Field("anchor", go.anchor);
        go.color = EditorGUILayout.ColorField("Color", go.color);
        bool hasCollider = EditorGUILayout.Toggle("Has Collider", go.Collider != null);
        if (hasCollider != (go.Collider != null))
        {
            if (hasCollider)
            {
                go.addCollider();
            }
            else
            {
                DeleteComponentFromEditor d = go.gameObject.AddComponent<DeleteComponentFromEditor>();
                d.componentReference = go.collider;
            }
        }
        go.transform.position = go.transform.position;
		go.Fixed = EditorGUILayout.Toggle("Fixed",go.Fixed);
        
    }
    private static void saveFrameNames(Sprite sdf)
    {

        Sprite[] s = sdf.GetComponentsInChildren<Sprite>();
        foreach (Sprite sheet in s)
        {
            FrameNameHolder fnh = sheet.gameObject.GetComponent<FrameNameHolder>();
            if (fnh == null)
            {
                fnh = sheet.gameObject.AddComponent<FrameNameHolder>();
            }
            fnh.setFrameAndName(sheet);
        }
    }

}

