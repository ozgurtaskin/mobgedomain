﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(FrameNameHolder))]
public class EFrameNameHolder : Editor
{

    public override void OnInspectorGUI()
    {
        FrameNameHolder go = (FrameNameHolder)target;
        if (GUILayout.Button("Remove Traversal " + go.frameName))
        {
            FrameNameHolder[] fnh = go.GetComponentsInChildren<FrameNameHolder>();
            foreach (FrameNameHolder f in fnh)
            {
                MonoBehaviourExtension.DestroyObj(f);
            }
        }
        EditorGUILayout.ObjectField(go.sprite, typeof(Sprite),true);
    }
}

