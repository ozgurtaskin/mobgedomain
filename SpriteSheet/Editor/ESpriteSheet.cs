﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(SpriteSheet))]
public class ESpriteSheet : Editor
{

    public override void OnInspectorGUI()
    {
        SpriteSheet sheet = (SpriteSheet)target;
        sheet.register();
        EditorGUILayout.LabelField("Index:", ""+sheet.index);
        sheet.Material = (Material)EditorGUILayout.ObjectField("Material", sheet.Material,typeof(Material),true);



        UnityEngine.Object plist = Resources.LoadAssetAtPath(sheet.Path, typeof(UnityEngine.Object));
        plist = EditorGUILayout.ObjectField("Plist File", plist, typeof(UnityEngine.Object),true);
        sheet.Path = AssetDatabase.GetAssetPath(plist);

        sheet.Layer = EditorGUILayout.IntField("Layer",sheet.Layer);

        if (GUILayout.Button("Update"))
        {
            List<Frame> frames = sheet.frames;
            List<string> keys = sheet.keys;
            sheet.setPath(sheet.Path);

            List<Frame> newframes = sheet.frames;
            //List<string> newkeys = sheet.keys;

            UnityEngine.Object[] objs = Resources.LoadAll("Prefabs", typeof(GameObject));
            foreach (UnityEngine.Object obj in objs)
            {
                if (obj is GameObject)
                {
                    GameObject g = (GameObject)obj;
                    //EditorUtility.SetDirty(obj);
                    if (sheet.updateFrames(g.transform, frames, newframes, keys))
                    {
                        g.transform.position = g.transform.position;
                    }
                }
            }
            AssetDatabase.SaveAssets();
        }

    }
}

