﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEditor;
using System.Xml;
using System.IO;

[CustomEditor(typeof(SpriteSheetManager))]
public class ESpriteSheetManager : Editor
{
    UnityEngine.Object ai;
	int increaseAmount = 0;

    
    public override void OnInspectorGUI ()
	{
		SpriteSheetManager go = SpriteSheetManager.Singelton;
        
		if (go == null) {
			go = (SpriteSheetManager)target;
			SpriteSheetManager.Singelton = go;
		}
		EditorGUILayout.LabelField ("Dont Touch This", "");
		if (go.firstTime) {
			go.firstTime = false;
			if (Resources.Load ("SpriteSheet/sheetsInfo") != null) {
				//refreshSheetsFromInfo(go);
			} else {
				Directory.CreateDirectory ("Assets/Resources/SpriteSheet");
				GameObject si = new GameObject ("sheetsInfo");
				si.AddComponent<SheetData> ();
				si.active = false;
				var obj = PrefabUtility.CreateEmptyPrefab ("Assets/Resources/SpriteSheet/sheetsInfo.prefab");
				PrefabUtility.ReplacePrefab (si, obj);

				//AssetDatabase.CreateAsset(si, "Assets/Resources/SpriteSheet/sheetsInfo.prefab");
				//AssetDatabase.SaveAssets();
				DestroyImmediate (si);
				//refreshInfoFromSheets(go);
			}
            
		}
		if (go.sheetsChanged) {
			go.sheetsChanged = true;
			//refreshInfoFromSheets(go);
		}
		/*
        EditorExtension.listSizeField<SpriteSheet>("Sheet Count", go.sheets, defaultSheet);
        int start = 0;
        for (int i = 0; i < go.sheets.Count; i++)
        {
            EditorGUILayout.LabelField("" + i, "");
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.Space();
            EditorGUILayout.BeginVertical();
            SpriteSheet sheet = go.sheets[i];
            sheet.material = (Material)EditorGUILayout.ObjectField("Material", sheet.material, typeof(Material));
            UnityEngine.Object plist = Resources.LoadAssetAtPath(sheet.name,typeof(UnityEngine.Object));
            plist = EditorGUILayout.ObjectField("Plist File", plist, typeof(UnityEngine.Object));
            string oldName = sheet.name;
            sheet.name = AssetDatabase.GetAssetPath(plist);
            if (plist != null)
            {
                if (sheet.name != oldName)
                {
                    List<SpriteAttribute> attributes = ZwoptexParser.getSprites(sheet.name);
                    sheet.count = attributes.Count;
                    Debug.Log("parsing");
                }
            }
            else
            {
                sheet.count = 0;
            }
            sheet.start = start;
            start += sheet.count;
            EditorGUILayout.EndVertical();
            EditorGUILayout.EndHorizontal();
        }
        */
		EditorGUILayout.BeginHorizontal ();
		if (GUILayout.Button ("change layers by")) {
			if(go.sheets != null){
				foreach(var sheet in go.sheets)
				{
					if(sheet)
						sheet.Layer += increaseAmount;
				}
			}
			if(go.replicaSheets != null)
			{
				foreach(var sheet in go.replicaSheets)
				{
					if(sheet)
						sheet.Layer += increaseAmount;
				}
			}
		}
		increaseAmount = EditorGUILayout.IntField(increaseAmount);

		EditorGUILayout.EndHorizontal();
    }
    public void refreshInfoFromSheets(SpriteSheetManager go)
    {

        int count = go.sheets.Count;
        SheetData sd = (SheetData)Resources.Load("SpriteSheet/sheetsInfo", typeof(SheetData));
        sd.plists = new string[count];
        sd.materials = new string[count];
        for (int i = 0; i < count; i++)
        {
            if (go.sheets[i] != null)
            {
                sd.plists[i] = go.sheets[i].Path;
                sd.materials[i] = AssetDatabase.GetAssetPath(go.sheets[i].material);
                
            }
            else
            {
                sd.materials[i] = "";
                sd.plists[i] = "";
            }
        }
        sd.nextIndex = go.nextIndex;
        sd.sheetCount = go.sheets.Count;
        sd.transform.position = sd.transform.position;
        AssetDatabase.SaveAssets();
    }
    public void refreshSheetsFromInfo(SpriteSheetManager go)
    {
        SheetData sd = (SheetData)Resources.Load("SpriteSheet/sheetsInfo", typeof(SheetData));
        List<SpriteSheet> sheets = new List<SpriteSheet>();
        List<SpriteSheet> oldSheets = go.sheets;
        go.sheets = sheets;
        for (int i = 0; i < sd.plists.Count(); i++)
        {
            if (sd.plists[i] == "")
            {
                go.sheets.Add(null);
                if (oldSheets.Count > i && oldSheets[i] != null)
                {
                    DestroyImmediate(oldSheets[i]);
                }

            }
            else
            {
                if (oldSheets.Count > i && oldSheets[i] != null)
                {
                    go.sheets.Add(oldSheets[i]);
                    oldSheets[i].Material = (Material)AssetDatabase.LoadAssetAtPath(sd.materials[i],typeof(Material));
                    oldSheets[i].setPath(sd.plists[i]);
                }
                else
                {
                    go.nextIndex = go.sheets.Count;
                    //Debug.Log(go.nextIndex);
                    go.sheets.Add(null);
                    SpriteSheet ss = go.gameObject.AddComponent<SpriteSheet>();
                    ss.register();
                    ss.initializeComponents();
                    ss.Material = (Material)AssetDatabase.LoadAssetAtPath(sd.materials[i], typeof(Material));
                    ss.setPath(sd.plists[i]);
                }
            }
        }
        go.sheetsChanged = false;
        go.nextIndex = sd.nextIndex;
    }
    private SpriteSheet defaultSheet()
    {
        return new SpriteSheet();
    }
}

