﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[ExecuteInEditMode]
public class FrameNameHolder : MonoBehaviour
{
    public Sprite sprite;
    public string frameName;


    public void setFrameAndName(Sprite f)
    {

        sprite = f;

        Debug.Log(sprite);
        Debug.Log(sprite.Sheet);
        Debug.Log(sprite.SheetIndex);
        Debug.Log(sprite.Frame);
        Debug.Log(sprite.thisTransform);
        Debug.Log(sprite.anchor);

        Debug.Log(sprite);
        SpriteSheet sheet = sprite.Sheet;
        for (int i = 0; i < sheet.frames.Count; i++)
        {
            Frame fr = sheet.frames[i];
            if (fr == f.Frame)
            {
                frameName = sheet.keys[i];
                Debug.Log(frameName);
                break;
            }
        }
    }
    void Start()
    {
        if (Application.isPlaying)
        {
            this.DestroyObj(this);
        }
    }
    void OnEnable()
    {
        Debug.Log(sprite);
        Debug.Log(sprite.Sheet);
        Debug.Log(sprite.SheetIndex);
        Debug.Log(sprite.Frame);
        Debug.Log(sprite.thisTransform);
        Debug.Log(sprite.anchor);
        if (sprite != null)
        {
            sprite.setFrameName(frameName);
        }
    }
    void OnDestroy()
    {
        if (sprite != null)
        {
            sprite.setFrameName(frameName);
        }
        //this.DestroyObj(this);
    }
}
