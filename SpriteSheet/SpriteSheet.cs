﻿//#define TEST_SPEEDusing System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using System.Runtime.InteropServices;
using System;

[AddComponentMenu("Sprite Sheet/Sheet")]
[Serializable]
[ExecuteInEditMode]
public class SpriteSheet : MonoBehaviour, IComparable<SpriteSheet>
{
	//unsafe{
	[DllImport("__Internal")]
	private static extern IntPtr genSheet ();

	[DllImport("__Internal")]
	private static extern void freeSheet (IntPtr sheet);

	[DllImport("__Internal")]
	private static extern void sheetSetProperties (IntPtr sheet,
	                                              Vector3[] poses,
	                                              Vector3[] normals,
	                                              Vector2[] uvs,
	                                              Color[] colors,
	                                              int[] tris);

	[DllImport("__Internal")]
	private static extern  void fillConstantPart (IntPtr sheet, int count);

	[DllImport("__Internal")]
	internal static extern int spriteDraw (IntPtr sheet, Matrix3x4 mat, SFrame f, 
	                                      Vector2 anchor, Color c, int index);
	//} 
	[SerializeField]
    private String path = "";
    [SerializeField]
    private Vector2 texSize;
    [SerializeField]
    public Material material;
    public int index = -1;
	
	public const bool gldraw = false;
    public int layer;

    public List<Frame> frames;

    public List<String> keys;
    private Dictionary<string, Frame> data;

    public List<Sprite> sprites;
    //public SortedList<float, Sprite> sortedSprites;
    //private Sprite root;
    //private Sprite oldRoot;
    private List<Sprite> list;
	private List<int> truthCheck;
    private Sprite[] oldList;
    public int count;
    [SerializeField]
    public Mesh mesh;

    /*[SerializeField]
    private MeshFilter mf;
    [SerializeField]
    private MeshRenderer mr;
    */
    private int maxvert;

	public IntPtr sheet;
	bool isSheetInitialized;

#if TEST_SPEED

	public static TimeTest gldrawTime;

	public static TimeTest drawTime;
	public static TimeTest rlFixedTime;
	public static SpriteSheet drawResponsible;
	static void trytobedrawResponsible (SpriteSheet s)
	{
		if (drawResponsible == null) {
			drawResponsible = s;
			gldrawTime = new TimeTest();
			rlFixedTime = new TimeTest();
			drawTime = new TimeTest();
		}
	}
#endif

    public DynamicArray<Vector2> uv;
	public DynamicArray<Vector3> verts;
    public DynamicArray<int> triangles;
    public DynamicArray<Vector3> normals;
    public DynamicArray<Color> colors;

	
	void Awake ()
	{
		//Debug.Log(material.mainTexture.name);
		initData ();
		Layer = layer;
		maxvert = 0;
		initTempData ();
		isSheetInitialized = false;
#if TEST_SPEED
		trytobedrawResponsible(this);
#endif

    }
	void ensureSheet()
	{
		if (SpriteSheetManager.UseNative && !isSheetInitialized) {
			sheet = genSheet ();
			isSheetInitialized= true;
		}
	}
    public void register()
    {
        if (index < 0)
        {
            mesh = new Mesh();
            if (SpriteSheetManager.Singelton == null)
            {
                SpriteSheetManager.Singelton = gameObject.AddComponent<SpriteSheetManager>();
            }
            index = takeIndex();
        }
    }
    public int Layer
    {
        get
        {
            return layer;
        }
        set
        {
            layer = value;
            if (Application.isPlaying)
            {
                if (material != null)
                {
                    material.renderQueue = 3000 + layer;
                }
            }
        }
    }
    public virtual int Index
    {
        get
        {
            return index;
        }
    }

	#region IComparable[Sprite] implementation
	public int CompareTo (SpriteSheet other)
	{
		return Material.renderQueue - other.Material.renderQueue;
	}
	#endregion

    protected virtual int takeIndex()
    {
        return SpriteSheetManager.Singelton.addSheet(this);
    }
    /*
    public bool removeSprite(Sprite sprite)
    {
        for (int i = 0; i < sprites.Count; i++)
        {
            if (sprite == sprites[i])
            {
                sprites[i] = sprites.Last();
                sprites.RemoveAt(sprites.Count - 1);
                return true;
            }
        }
        return false;
    }*/

    /*internal void removeSprite(int index)
    {
        Sprite s = sprites[sprites.Count-1];
        sprites[index] = s;
        s.index = index;
        sprites.RemoveAt(sprites.Count - 1);
    }
    public int addSprite(Sprite sprite)
    {
        sprites.Add(sprite);

        return sprites.Count - 1;
    }*/
    public Material Material
    {
        get
        {
            return material;
        }
        set
        {
            if (material != value)
            {
                SpriteSheetManager.Singelton.sheetsChanged = true;
            }
            material = value;
            if (material != null)
            {
                texSize = new Vector2(material.mainTexture.width, material.mainTexture.height);
                material.mainTextureScale = new Vector2((float)(1.0 / texSize.x), (float)(-1.0 / texSize.y));
                
                //Debug.Log(Index);
                //mr.sharedMaterial = value;
            }
        }
    }
    public virtual string Path
    {
        get
        {
            return path;
        }
        set
        {
            if (path != value && value != null)
            {

                setPath(value);
                SpriteSheetManager.Singelton.sheetsChanged = true;
            }
        }
    }
    void initTempData()
    {
		//GameVariables gv = GameVariables.SharedInstance;
		var sheetTolerance = 0.4f;
        verts = new DynamicArray<Vector3>(4,sheetTolerance);
        uv = new DynamicArray<Vector2>(4,sheetTolerance);
        triangles = new DynamicArray<int>(6,0);
        normals = new DynamicArray<Vector3>(4,sheetTolerance);
        colors = new DynamicArray<Color>(4,sheetTolerance);
        list = new List<Sprite>();
		truthCheck = new List<int>();
        oldList = new Sprite[0];
    }
    public void setPath(string value)
    {
        path = value;
        List<SpriteAttribute> attributes = ZwoptexParser.getSprites(path);
        List<Frame> newFrames = new List<Frame>(attributes.Count);
        List<string> newkeys = new List<string>(attributes.Count);
        for (int i = 0; i < attributes.Count; i++)
        {
            SpriteAttribute at = attributes[i];
            Frame f = new Frame(at);
            //Debug.Log(f.topLeft);
            newFrames.Add(f);
            newkeys.Add(at.name);

        }
        initData(newFrames, newkeys);
        if (frames.Count > 0)
        {
            updateFrames(frames, newFrames, keys, newkeys);
        }
        frames = newFrames;
        keys = newkeys;
    }

    private static string findFrameName(Frame f, List<Frame> frameList, List<string> keyList)
    {
        int i = 0;
        foreach (Frame fr in frameList)
        {
            if (f == fr)
            {
                return keyList[i];
            }
            i++;
        }
        return "";
    }

    private void updateFrames(List<Frame> frames, List<Frame> newFrames, List<string> keys, List<string> newkeys)
    {
        //Debug.Log("" + frames.Count + " " + newFrames.Count + " " + keys.Count + " " + newkeys.Count + " " + oldRoot);
        //Sprite next;
        foreach (Sprite next in oldList)
        {

            string frameName = findFrameName(next.Frame, frames, keys);
            if (frameName.Count() > 0)
            {
                next.setFrameName(frameName);
            }
            //next = next.next;
        }
        if (Index < SpriteSheetManager.MAX_SHEET_COUNT)
        {
            List<SpriteSheet> sheets = SpriteSheetManager.Singelton.replicaSheets;
            foreach (SpriteSheet sheet in sheets)
            {
                if (sheet != null)
                {
                    ReplicaSpriteSheet rss = (ReplicaSpriteSheet)sheet;
                    if (rss.originalIndex == Index)
                    {
                        rss.updateFrames(frames, newFrames, keys, newkeys);
                    }
                }
            }

        }
        /*
        UnityEngine.Object[] all = Resources.LoadAll("", typeof(GameObject));
        for (int i = 0; i < all.Count(); i++)
        {
            updateFrames(((GameObject)all[i]).transform, frames, newFrames, keys);

        }
        */
    }
    public bool updateFrames(Transform tr, List<Frame> frames, List<Frame> newFrames, List<string> keys)
    {
        bool b = false;
        Sprite[] sprites = tr.GetComponentsInChildren<Sprite>(true);
        //Debug.Log(tr.name.IndexOf("sprite") >= 0 ? "" + sprites.Count() : "");
        foreach (Sprite next in sprites)
        {
            //Debug.Log(next);
            if (next.SheetIndex == index || (next.SheetIndex / SpriteSheetManager.MAX_SHEET_COUNT - 1) == index)
            {
                string frameName = findFrameName(next.Frame, frames, keys);
                if (frameName.Count() > 0)
                {
                    b = true;
                    next.setFrameName(frameName);
                }
            }
        }
        /*foreach (Transform t in tr)
        {
            updateFrames(t, frames, newFrames);
        }*/
        return b;
    }
	/*void OnRenderObject(){
		var v = this;
		//Debug.Log (v.Material.SetPass(0));
			GL.Begin(GL.QUADS);
			for(int i = 0; i < v.Count; i++){
				int i4 = i*4;
				int i6 = i*6;
				GL.Color(v.colors[i4]);
				GL.TexCoord(v.uv[i4+0]);
				GL.Vertex(v.verts[i4+0]);
				GL.Color(v.colors[i4]);
				GL.TexCoord(v.uv[i4+1]);
				GL.Vertex(v.verts[i4+1]);
				GL.Color(v.colors[i4]);
				GL.TexCoord(v.uv[i4+2]);
				GL.Vertex(v.verts[i4+2]);
				GL.Color(v.colors[i4]);
				GL.TexCoord(v.uv[i4+3]);
				GL.Vertex(v.verts[i4+3]);
				
			}
			GL.End();
	}*/
    void initData(List<Frame> frames, List<string> keys)
    {
        data = new Dictionary<string, Frame>(frames.Count);
        for (int i = 0; i < frames.Count; i++)
        {
            //Debug.Log("dfgdfgdfg: "+keys[i]);
            data[keys[i]] = frames[i];
        }
    }
    void initData()
    {
        if (frames == null)
        {
            frames = new List<Frame>();
            keys = new List<string>();
            sprites = new List<Sprite>();
            //sortedSprites = new SortedList<float, Sprite>();
            count = 0;
        }
        initData(frames, keys);

    }
    public virtual Dictionary<string, Frame> FrameData
    {
        get
        {
#if(UNITY_EDITOR)
            if (data == null)
            {
                initData();
            }
            else
            {

            }
            return data;
#else 
if (data == null)
            {
                initData();
            }
            return data;
#endif
        }
    }
    public Frame getFrame (string frameName)
	{
		//Debug.Log(frameName + " " + (FrameData == null));
		Frame o;
		if (!FrameData.TryGetValue (frameName, out o)) {
			Debug.Log("missing frame: " + frameName);

		}
        return o;
    }
    public void initializeComponents()
    {
        if (triangles == null)
        {
            initTempData();
        }
        /*if (mr == null)
        {
            GameObject go = new GameObject("sheet " + this.index);
            go.transform.parent = transform;

            go.transform.localPosition = Vector3.zero;

            mr = go.AddComponent<MeshRenderer>();
            mf = go.AddComponent<MeshFilter>();

            mr.sharedMaterial = material;
            mf.sharedMesh = mesh;
        }*/
    }
    void LateUpdate ()
	{

		//Debug.Log("late update: " + Index);
#if UNITY_EDITOR
		if (!Application.isPlaying) {
		
			initializeComponents ();
		}
#endif

#if TEST_SPEED
		drawTime.start();
#endif
		if (mesh != null) {

			//mesh.Clear();
			//int count = sortedSprites.Count;



			//int oldTriSize = triangles.Size;

			verts.Size = count * 4;
			uv.Size = count * 4;
			triangles.Size = count * 6;
			normals.Size = count * 4;
			colors.Size = count * 4;
			
			/*
			if(verts.Size != 0){
				
				Debug.Log(material.mainTexture.name + "\t" + ((float)verts.allocateCount/(verts.allocateCount+verts.unallocateCount)) + "\t"+(verts.Size));
			}
*/			
			int size = triangles.Size;
			while (size <triangles.data.Length) {
				triangles [size] = 0;
				size++;
			}
			//Sprite next = root;
			int index = 0;
			bool sortEnabled = true;// GameVariables.SharedInstance.spriteSheetSort;
			if (sortEnabled) {
				list.Sort ();
			}
			int countt = 0;
			if (SpriteSheetManager.UseNative) {
				ensureSheet ();
				sheetSetProperties (sheet, verts.data, normals.data, uv.data, colors.data, triangles.data);

			}
			foreach (Sprite next in list) {
				int oldIndex = index;
				/*if(next == null){
					Debug.Log(""+index+" " +next);
				}*/
				index = next.draw (verts, uv, colors, index);
				
				if (!sortEnabled) {
					if ((index - oldIndex) != truthCheck [countt]) {
						Debug.Log ("lier sprite detected: " + next.name);
					}
					countt++;
				}
				//next = next.next;
			}
			if (!gldraw) {
				if (SpriteSheetManager.UseNative) {
					fillConstantPart (sheet, count);
				} else {
					for (int j = 0; j < count; j++) {
						int i6 = j * 6;
						int i4 = j * 4;

						triangles [i6 + 0] = i4 + 0;
						triangles [i6 + 1] = i4 + 1;
						triangles [i6 + 2] = i4 + 2;
						triangles [i6 + 3] = i4 + 0;
						triangles [i6 + 4] = i4 + 2;
						triangles [i6 + 5] = i4 + 3;

						normals [i4 + 0] = new Vector3 (0, 0, -0.1f);
						normals [i4 + 1] = new Vector3 (0, 0, -0.1f);
						normals [i4 + 2] = new Vector3 (0, 0, -0.1f);
						normals [i4 + 3] = new Vector3 (0, 0, -0.1f);
					}
				}
				if (mesh.vertexCount != verts.data.Length) {
					mesh.Clear ();
				}
				mesh.vertices = verts.data;
				mesh.uv = uv.data;
				mesh.normals = normals.data;
				mesh.colors = colors.data;
				
				mesh.triangles = triangles.data;
				mesh.RecalculateBounds ();

				if (maxvert < mesh.vertexCount) {
					maxvert = mesh.vertexCount;
				}
	            
				//Debug.Log(""+this.index + " " +maxvert + " " );

				//Graphics.DrawMesh(mesh, Vector3.zero, Quaternion.identity, material, 0);
			}
			Count = count;
			count = 0;
            
			oldList = list.ToArray ();
			/*if(index == 2){
				Debug.Log ("clearing");
			}*/
			list.Clear ();
			if (!sortEnabled) {
				truthCheck.Clear ();
			}
            Graphics.DrawMesh(mesh, new Vector3(0, 0, 0), Quaternion.identity, material, 0);
		} else {
			mesh = new Mesh ();
            mesh.MarkDynamic();
            Debug.Log("marking mesh dynamic");
		}
        /*if(mf)
		if (mf.sharedMesh != mesh) {
			mf.sharedMesh = mesh;
		}*/
#if TEST_SPEED


		drawTime.endAndAdd();
		if (drawResponsible == this) {
			drawTime.increaseCount();
		}
#endif
    }
	
#if TEST_SPEED
	void OnGUI ()
	{
		if (drawResponsible == this) {
			GUI.matrix = Matrix4x4.Scale (new Vector3 (2, 2, 2));
			GUI.color = Color.black;
			GUILayout.Label ("avarage draw time: " + (drawTime) );
			GUILayout.Label ("avarage gldraw time: " + (gldrawTime) );
			GUILayout.Label ("avarage rlfixed time: " + (rlFixedTime ) );
		}
	}
#endif
	public int Count{get;set;}
    protected virtual void removeIndex()
    {
		SpriteSheetManager.Singelton.removeSheet(index);
    }
    void OnDestroy()
    {
        if (index >= 0)
        {
            if (SpriteSheetManager.Singelton != null)
            {
                removeIndex();
            }
        }
		if (SpriteSheetManager.UseNative && isSheetInitialized) {
			freeSheet(sheet);
			isSheetInitialized = false;
		}
    }
    /*
    public void AddSprite(Sprite sprite)
    {
        count++;
        Sprite next = root;
        if (root == null)
        {
            root = sprite;
            root.next = null;
            return;
        }
            sprite.next = root;
            root = sprite;
            return;
        
    }
    
    */

    internal void AddSprite(Sprite sprite, int pictureCount)
    {
        count += pictureCount;
		/*if(sprite is WavingSprite){
			Debug.Log(sprite.name);
		}*/
        list.Add(sprite);
		if(!gldraw){
		truthCheck.Add(pictureCount);
		}
        /*
        Sprite next = root;
        if (root == null)
        {
            root = sprite;
            root.next = null;
            return;
        }
        if (root.z <= sprite.z)
        {
            sprite.next = root;
            root = sprite;
            return;
        }
        Sprite prev = next;
        while ((next = next.next) != null)
        {
            if (next.z <= sprite.z)
            {
                sprite.next = next;
                prev.next = sprite;
                return;
            }
            prev = next;
        }
        prev.next = sprite;
        sprite.next = null;
         * */
    }
	public class DynamicArray<T> : PublicList<T>{
		public DynamicArray(int multipleOf, float maxUnusedElements = 0.2f, int capacity = 10)
			: base(multipleOf,maxUnusedElements,capacity)
		{
		}
	}
}
public struct TimeTest
{
	public float time;
	private float onFly;
	private float timer;
	public void addTime(float time)
	{
		onFly += time;
	}
	public void increaseCount ()
	{
		time += (onFly-time)*0.05f;
		onFly = 0;
	}
	public void start()
	{
		timer = Time.realtimeSinceStartup;
	}
	public void endAndAdd()
	{
		timer = Time.realtimeSinceStartup-timer;
		addTime(timer);
	}
	public override string ToString ()
	{
		return time.ToString("0.000000000");
	}
}
public struct Matrix3x4
{
	 public float m00, m01, m02, m03;
	 public float m10, m11, m12, m13;
	 public float m20, m21, m22, m23;
	public Matrix3x4 (Matrix4x4 m)
	{
		m00 = m.m00; m01 = m.m01; m02 = m.m02; m03 = m.m03;
		m10 = m.m10; m11 = m.m11; m12 = m.m12; m13 = m.m13;
		m20 = m.m20; m21 = m.m21; m22 = m.m22; m23 = m.m23;
	}

}