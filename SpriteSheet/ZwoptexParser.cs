﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

public class SpriteSheetAttribute
{
    public float width;
    public float height;
}

public class SpriteAttribute
{
    public string name;
    public float xPos;
    public float yPos;
    public float width;
    public float height;
    public float offsetX;
    public float offsetY;
    public float originalWidth;
    public float originalHeight;

}

public class ZwoptexParser
{
    static XmlTextReader reader;
    static XmlTextReader reader2;

    public static List<SpriteAttribute> getSprites(string plistName)
    {
        List<SpriteAttribute> allSprites = new List<SpriteAttribute>(0);
        reader = new XmlTextReader(plistName);
        reader2 = new XmlTextReader(plistName);
        
        //detect num of elements in plist with another reader
        int numOfElements = 0;
        while (reader2.Read())
        {
            if (reader2.NodeType == XmlNodeType.Element && reader2.Name == "dict")
            {
                numOfElements++;
            }
        }
        numOfElements -= 3;

        SpriteSheetAttribute spriteSheet = new SpriteSheetAttribute();
        
        //width and height
        nextValue();
        nextValue();
        nextValue();
        spriteSheet.width = float.Parse(reader.Value);
        nextValue();
        nextValue();
        spriteSheet.height = float.Parse(reader.Value);
        

        //clear until frames
        while (reader.Value != "frames")
        {
            reader.Read();
        }
        //starting from the first image name to all attrubutes.
        for (int i = 0; i < numOfElements; i++)
        {
            SpriteAttribute sa = new SpriteAttribute();
            nextValue();
            
            sa.name = reader.Value;
            nextValue();
            nextValue();
            
            sa.xPos = float.Parse(reader.Value);
            nextValue();
            nextValue();
            
            sa.yPos = float.Parse(reader.Value);
            nextValue();
            nextValue();
           
            sa.width = float.Parse(reader.Value);
            nextValue();
            nextValue();
           
            sa.height = float.Parse(reader.Value);
            nextValue();
            nextValue();
            
            sa.offsetX = float.Parse(reader.Value);
            nextValue();
            nextValue();
           
            sa.offsetY = float.Parse(reader.Value);
            nextValue();
            nextValue();
            
            sa.originalWidth = float.Parse(reader.Value);
            nextValue();
            nextValue();
           
            sa.originalHeight = float.Parse(reader.Value);
            allSprites.Add(sa);
        }
        reader.Close();
        reader2.Close();
        return allSprites;
    }
    public static void nextValue()
    {
        reader.Read();
        while (reader.NodeType != XmlNodeType.Text)
        {
            reader.Read();
        }
    }
}
