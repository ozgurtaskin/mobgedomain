﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[Serializable]
public class Frame 
{
    public Vector2 topLeft;
    public Vector2 bottomRight;
    public Vector2 anchor;

    public Frame()
    {
    }
    public Frame(Frame f)
    {
        if (f != null)
        {
            topLeft = f.topLeft;
            bottomRight = f.bottomRight;
            anchor = f.anchor;
        }
    }
    public override int GetHashCode()
    {
        return base.GetHashCode();
    }
    public override bool Equals(object obj)
    {
        return base.Equals(obj);
    }
    public static bool operator != (Frame l, Frame r)
    {
        if (Frame.Equals(l, null))
        {
            return !Frame.Equals(r, null);
        }
        if (Frame.Equals(r, null))
        {
            return !Frame.Equals(l, null);
        }
        return l.topLeft != r.topLeft || l.bottomRight != r.bottomRight;
    }
    public static bool operator == (Frame l, Frame r)
    {
        return !(l != r);
    }
    public Frame(SpriteAttribute at)
    {
        topLeft = new Vector2(at.xPos, at.yPos);
        bottomRight = new Vector2(at.xPos + at.width, at.yPos+at.height);
        anchor = new Vector2(at.offsetX, at.offsetY);
    }
    public Vector2 size
    {
        get
        {
            Vector2 v = topLeft - bottomRight;
            //v.x = -v.x;
            return v / -100;
        }
    }

    internal void shrinkBy(float p)
    {
        topLeft += new Vector2(1, 1f);
        bottomRight += new Vector2(-1, -1);
    }
}
public struct SFrame{
	public Vector2 topLeft;
	public Vector2 bottomRight;
	public Vector2 anchor;
	public SFrame(Frame f)
	{
		topLeft = f.topLeft;
		bottomRight = f.bottomRight;
		anchor = f.anchor;
	}
}