using UnityEngine;
using System.Collections;

public class SheetDrawer : MonoBehaviour
{
	public SpriteSheetManager manager;
	
	
	void OnPostRender(){
		//Debug.Log("Sd");
		foreach(var v in manager.sortedSheets){
			v.Material.SetPass(0);
			GL.Begin(GL.QUADS);
			for(int i = 0; i < v.Count; i++){
				int i4 = i*4;
				//int i6 = i*6;
				GL.Color(v.colors[i4]);
				GL.TexCoord(v.uv[i4+0]);
				GL.Vertex(v.verts[i4+0]);
				GL.TexCoord(v.uv[i4+1]);
				GL.Vertex(v.verts[i4+1]);
				GL.TexCoord(v.uv[i4+2]);
				GL.Vertex(v.verts[i4+2]);
				GL.TexCoord(v.uv[i4+3]);
				GL.Vertex(v.verts[i4+3]);
				
			}
			GL.End();
		}
	}
}

