﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class SheetData : MonoBehaviour
{
    public string[] materials;
    public string[] plists;
    public int nextIndex;
    public int sheetCount;

    private SheetData shared;
    public SheetData Shared
    {
        get
        {
            if (shared == null)
            {
                shared = (SheetData)Resources.Load("SpriteSheet/sheetsInfo", typeof(SheetData));
            }
            return shared;
        }
    }
}
