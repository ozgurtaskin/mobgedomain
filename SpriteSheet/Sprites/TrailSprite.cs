﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class TrailSprite : Sprite
{
    private Queue2<Vector3[]> pointCash = new Queue2<Vector3[]>();
    float copyCount;
    int nextIndex;
    public int updateInterval = 1;
    int time = 0;
    public float CopyCount
    {
        get { return copyCount; }
        set
        {
            copyCount = value;
            if (pointCash == null)
            {
                //pointCash = ;
                nextIndex = 0;
            }
        }
    }
    public override int PictureCount
    {
        get
        {
            return pointCash.Count + 1;
        }
    }
    private Vector3[] generateQuad()
    {
        return new Vector3[4];
    }
    public override int draw(SpriteSheet.DynamicArray<Vector3> verts, SpriteSheet.DynamicArray<Vector2> uv, SpriteSheet.DynamicArray<Color> colors, int index)
    {
		int drawCount = pointCash.Count;
        float alpha = color.a;
        float alphaStep = alpha / (copyCount);
        Vector2 topLeft = frame.topLeft;
        Vector2 bottomRight = frame.bottomRight;

        foreach (Vector3[] quad in pointCash.reverse())
        {
            alpha -= alphaStep;
            int i4 = index * 4;

            fillUvs(uv, i4, topLeft, bottomRight);
            var c = new Color(color.r, color.g, color.b, alpha);
            
            colors[i4 + 0] = c;
            colors[i4 + 1] = c;
            colors[i4 + 2] = c;
            colors[i4 + 3] = c;


            verts[i4 + 0] = quad[0];
            verts[i4 + 1] = quad[1];
            verts[i4 + 2] = quad[2];
            verts[i4 + 3] = quad[3];
            
            index++;
        }


        int startIndex = index;
        index = base.draw(verts, uv, colors, index);


        if (fixedPoints == null)
        {
            fixedPoints = generateQuad();
        }
        updateFixedPoints(startIndex * 4, verts);

        return index;
    }
    void FixedUpdate()
    {
        time++;
        if (time > updateInterval)
        {
            time -= updateInterval;
            Vector3[] nquad;
            if (copyCount > 0)
            {
                while (pointCash.Count >= copyCount)
                {
                    nquad = pointCash.Dequeue();
                }
                if (fixedPoints != null)
                {
                    nquad = (Vector3[])fixedPoints.Clone();
                    pointCash.Enqueue(nquad);
                }
            }
            else
            {
                if (pointCash.Count > 0)
                {
                    pointCash.Dequeue();
                }
            }
        }
    }
}
