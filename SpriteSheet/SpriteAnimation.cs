﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class SpriteAnimation : MonoBehaviour
{
    public const float FRAME_RATE = 60.0f;

    public Sprite sprite;
    public List<Animation> animations;
    private Queue<PlayProps> animQueue;
    public float time;
    public List<Frame> frames;
    public int lastFrameIndex;


    void Awake()
    {
        animations = new List<Animation>(1);
        animQueue = new Queue<PlayProps>(0);
        frames = new List<Frame>(2);
        time = 0;
    }
    /// <summary>
    /// clears queue and add animatoin to queue
    /// </summary>
    /// <param name="speed"></param>
    /// <param name="anim"></param>
    /// <param name="mode"></param>
    public void playAnimation(float speed, Animation anim, PlayMode mode)
    {
        clearQueue();
        addToQueue(speed, anim, mode);
    }
    /// <summary>
    /// add animatoin to queue
    /// </summary>
    /// <param name="speed"></param>
    /// <param name="anim"></param>
    /// <param name="mode"></param>
    public void addToQueue(float speed, Animation anim, PlayMode mode)
    {
        PlayProps pp = new PlayProps();
        pp.speed = speed;
        pp.anim = anim;
        pp.mode = mode;
        animQueue.Enqueue(pp);
    }
    /// <summary>
    /// clearsQueue
    /// </summary>
    public void clearQueue()
    {
        time = 0;
        animQueue.Clear();
    }
    void Start()
    {
        if (sprite == null)
        {
            sprite = GetComponent<Sprite>();
        }
    }
    int getIndex(PlayProps pp, int time)
    {
        PlayProps top = pp;
        //Debug.Log(time);
        //return (int)(time * (FRAME_RATE * top.speed) + top.anim.start);
        return (int)(time + top.anim.start);
    }
    int getIndex(PlayProps pp, float time)
    {
        PlayProps top = pp;
        //Debug.Log(time);
        //return (int)(time * (FRAME_RATE * top.speed) + top.anim.start);
        return (int)(time + top.anim.start);
    }
    void Update()
    {
        lastFrameIndex = 0;
        if (animQueue.Count > 0)
        {
            var top = animQueue.Peek();
            switch (top.mode)
            {
                case PlayMode.OneShot:
                    //if (time > top.anim.count / (FRAME_RATE * top.speed))
                    if (time >= top.anim.count)
                    {
                        animQueue.Dequeue();
                        time = 0;
                        lastFrameIndex = top.anim.end;
                        
                    }
                    else
                    {
                        lastFrameIndex = getIndex(top, time);
                    }
                    break;
                case PlayMode.Backward:
                    if (time+1 >= top.anim.count)
                    {
                        animQueue.Dequeue();
                        time = 0;
                        lastFrameIndex = top.anim.start;
                    }
                    else
                    {
                        lastFrameIndex = getIndex(top, top.period-1 - time);
                    }
                    break;
                case PlayMode.Loop:
                    {
                        float t = time % top.period;
                        lastFrameIndex = getIndex(top, t);

                    }
                    break;
                case PlayMode.PingPong:
                    {
                        float period = top.period-1;
                        float t = time;
                        if (time >= period * 2) 
                        {
                            lastFrameIndex = top.anim.start;
                            time = 0;
                            animQueue.Dequeue();
                        }
                        else
                        {
                            if (t > period)
                            {
                                t = 2 * period - t;
                            }
                            lastFrameIndex = getIndex(top, t);
                        }
                    }
                    break;
                                 
                case PlayMode.PingPongLoop:
                    {
                        int period = (int)top.period-1;
                        int t = ((int)time) % (period * 2);
                        if (t > period)
                        {
                            t = 2 * period - t;
                        }
                        lastFrameIndex = getIndex(top, t);
                    }
                    break;
                default:
                    break;
            }
            time += Time.deltaTime * top.speed * FRAME_RATE;
            sprite.Frame = frames[lastFrameIndex];
        }
    }
    public struct Animation
    {
        public int start;
        public int end;
        public PlayMode mode;
        public Animation(int start, int end, PlayMode mode)
        {
            this.start = start;
            this.end = end;
            this.mode = mode;
        }
        public int count
        {
            get
            {
                return 1 + end - start;
            }
        }
        public float period(float speed)
        {
            return count / (speed * FRAME_RATE);
        }
    }
    public enum PlayMode
    {
        OneShot = 1,
        Loop = 2,
        PingPong = 3,
        PingPongLoop = 4,
        Backward = 5,
    }
    private struct PlayProps
    {
        public Animation anim;
        public PlayMode mode;
        public float speed;
        public int period
        {
            get
            {
                return anim.count;
            }
        }
    }
}
