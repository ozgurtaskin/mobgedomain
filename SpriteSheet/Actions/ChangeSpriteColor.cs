﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class ChangeSpriteColor : AbstractAction
{
    public Color dif;
    public Color targetColor;
    public Sprite sprite;

    public ChangeSpriteColor(float time, Sprite sprite, Color target)
        : base(time)
    {
        this.sprite = sprite;
        targetColor = target;
    }
    public override void fire()
    {
        dif = targetColor - sprite.color;
        base.fire();
    }
    public override void Update(float deltaTime, float progress)
    {
        if (sprite)
        {
            sprite.color = targetColor + (progress - 1) * dif;
        }
        else
        {
            stop();
        }
    }
}
