using UnityEngine;
using System.Collections;

public class TargetCamera : MonoBehaviour {

    public Transform target;
    public float distance = 5;
    public float speedRate = 0.1f;
    public float rotRate = 0.1f;
    //private GameObject fallow;
    private Transform mtransform;
    public Vector2 bottomLeft;
    public Vector2 topRight;
	private Vector3 charPos;
	private float mydt;
	public Vector3 dis;
	private float horizontalTangent;
    //public CharacterCamera ccam;

    void Awake()
    {
        mtransform = transform;
    }
	// Use this for initialization
	void Start ()
	{
		/*
		 * float distance = 1000;
		for( float f = 1; f < 300; f++){
			distance = distance*0.971f;
			Debug.Log("ordinary way  : "+distance);
			Debug.Log("original way  : "+1000*(Mathf.Pow(1-0.029f,f)));
		}*/
		//fallow = gameObject;
		if (target){
			charPos = target.transform.position;
            
	}
		horizontalTangent = Mathf.Tan (Mathf.Deg2Rad * camera.fov * 0.5f) * camera.aspect;
        //lookAtTarget(1, target.transform.position);
        //followTarget(1, target.transform.position);
		
	}
    void Reset()
    {
        bottomLeft = new Vector2(float.NegativeInfinity, float.NegativeInfinity);
        topRight = new Vector2(float.PositiveInfinity, float.PositiveInfinity);
    }
    private static Vector3 toVector3(Quaternion q)
    {
        return new Vector3(q.x, q.y, q.z);  
    } 
	public static float calculateRate(float percent)
	{
		return calculateRate(percent, Time.deltaTime*60);
	}
	public static float calculateRate(float percent, float dt)
	{
		float t = dt;
		
		t = 1- Mathf.Pow(1-percent, t);
		//t = 1 / Mathf.Log(percent,Mathf.Epsilon);
		return t;
	}
	public void goTargetImmediately ()
	{
		if (target) {
			Vector3 tpos = target.transform.position + new Vector3 (12, 0, 0);
			followTarget (1, target.transform.position + new Vector3 (12, 0, 0));
			lookAtTarget (1, tpos);
			charPos = target.transform.position;
		}
    }
    private void lookAtTarget(float percent, Vector3 target)
    { 
		if(rotRate != 0){
        percent = calculateRate(percent);

        Quaternion q = Quaternion.LookRotation((target - mtransform.position + new Vector3(0, 0, 0)));
        //q.w = Mathf.PI / 3;
        mtransform.rotation = Quaternion.Lerp(mtransform.rotation, q, percent);}
    }
    private void followTarget (float percent, Vector3 target)
	{
		// percent = calculateRate(percent);

		/*if (ccam != null)
        {
            ccam.FixedUpdate();
        }*/

		Vector3 difv = target - mtransform.position;
		//difv.y += distance*0.3f;
		//difv.z -= distance * 2;
		//difv.x += this.target.velY * 0.3f;
		//float dis = difv.magnitude;
		//if (dis > distance)
		{
			//float dif = dis - distance;
			//dif *= percent;
			//Vector3
			Vector3 newPos = mtransform.localPosition + difv * (((float)percent));
			newPos.x = float.PositiveInfinity;
			newPos.x = Mathf.Min (newPos.x, this.target.transform.position.x
                                 - newPos.z * horizontalTangent * 0.7f
                                 );
			newPos.x = Mathf.Max (newPos.x, bottomLeft.x);
			//Debug.Log(bottomLeft.x);
			newPos.x = Mathf.Min (newPos.x, topRight.x);
			newPos.y = Mathf.Max (newPos.y, bottomLeft.y);
			newPos.y = Mathf.Min (newPos.y, topRight.y);
			newPos.z = -distance;
			mtransform.localPosition = newPos;
			//Debug.Log( mydt);
		}
	}
	// Update is called once per frame
	void FixedUpdate ()
	{ 
		if (target) {
			mydt = (target.transform.position - charPos).magnitude;
			mydt = Mathf.Min (0.5f, mydt);
		
			Vector3 tpos = target.transform.position + new Vector3 (12, 0, 0);
			followTarget (speedRate, target.transform.position + new Vector3 (dis.x, dis.y * 0.3f-100, dis.z));
			lookAtTarget (rotRate, tpos);
			charPos = target.transform.position;
		}
	}
}
