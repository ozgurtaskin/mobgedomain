﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class ActionSequence
{

    public List<IAction> sequence = new List<IAction>();
    private int next;
    public event EventHandler<EventArgs> onFinish;
    private bool isRunning = false;

    public void fire()
    {
        if (!IsRunning)
        {
            isRunning = true;
            startAction();
        }
    }
    public bool IsRunning
    {
        get
        {
            return isRunning;
        }
    }
    private void startAction()
    {
        int index = next;
        if (index >= sequence.Count)
        {
            isRunning = false;
            if (onFinish != null)
            {
                onFinish(this, new EventArgs());
            }
            return;
        }
        IAction action = sequence[index];
        action.onFinish += action_onFinish;
        action.fire();
    }

    private void action_onFinish(object sender, EventArgs e)
    {
        IAction action = (IAction)sender;
        action.onFinish -= action_onFinish;
        next++;
        startAction();
    }

    public void reset()
    {
        next = 0;
        sequence.Clear();
    }
}
