﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class ImmediateCall : IAction
{

    public override event EventHandler<ActionEventArgs> onFinish;
    

    public override void fire()
    {
        if (onFinish != null)
        {
            onFinish(this, new ActionEventArgs(Pocket));
        }
    }


    public override bool stopped
    {
        get { return true; }
        set
        {
        }
    }
    public override object Pocket
    {
        get;
        set;
    }
    public override bool IsFinished
    {
        get { return true; }
    }

    public override void UpdateAction(float deltaTime)
    {

    }
}
