﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class TurnTo : AbstractAction
{
    public Quaternion start;
    public Quaternion targetq;
    private GameObject target;
    public TurnTo(float totalTime, GameObject go, Quaternion target, Quaternion start)
        : base(totalTime)
    {
        this.start = start;
        targetq = target;
        this.target = go;
        
    }
    public TurnTo(float totalTime, GameObject go, Quaternion target)
        : this(totalTime, go, target, go.transform.localRotation)
    {
    }

    public override void Update(float deltaTime, float progress)
    {
        target.transform.localRotation = Quaternion.Lerp(start, targetq, progress);
    }
}

