﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public abstract class AbstractAction : IAction
{
    public enum MoveType
    {
        Linear = 1,
        Soft = 2,
        EaseIn = 3,
        EaseOut = 4,
    }
    public MoveType moveType;

    public override event EventHandler<ActionEventArgs> onFinish;

    public float time;
    public float totalTime;
    private bool isFinished = true;
    public object pocket;

    public AbstractAction(float totalTime)
    {
        this.totalTime = totalTime;
        this.time = 0;
        moveType = MoveType.Linear;
        stopped = true;
    }
    public override bool stopped { get; set; }
    public void clearFinishEvent()
    {
        onFinish = null;
    }
    protected void pureFire()
    {
        if (!stopped)
        {
            time = 0;
            isFinished = false;
        }
        else
        {
            time = 0;
            isFinished = false;
            ActionManager.SharedInstance.addNewAction(this);

        } 
    }
    public override void fire()
    {
        pureFire();
    }
    public float Progress
    {
        get
        {
            return time / totalTime;
        }
    }
    public override bool IsFinished
    {
        get
        {
            return isFinished;
        }
    }
    public void stop()
    {
        isFinished = true;
        time = totalTime;
        
    }
    public override void UpdateAction(float deltaTime)
    {
        time += deltaTime;
        if (time >= totalTime)
        {
            isFinished = true;
            if (onFinish != null)
            {
                Update(deltaTime, 1);
                onFinish(this, new ActionEventArgs(pocket));
            }
        }
        else
        {
                    float r = time / totalTime;
            switch (moveType)
            {
                case MoveType.Linear:
                    Update(deltaTime, r);
                    break;
                case MoveType.Soft:
                    Update(deltaTime, r * r * (3.0f - 2.0f * r));
                    break;
                case MoveType.EaseIn:

                    Update(deltaTime, r*r);
                    break;
                case MoveType.EaseOut:
                    float rr = 1-r;
                    Update(deltaTime, 1-rr*rr);
                    break;

            }
                                    
            
        }
    }

    public abstract void Update(float deltaTime, float progress);


    public override object Pocket
    {
        get
        {
            return pocket;
        }
        set
        {
            pocket = value;
        }
    }
}

