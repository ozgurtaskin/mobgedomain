﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


public class RotateTo : AbstractAction
{
    public Vector3 end, diff;
    protected GameObject target;

    public Transform transform
    {
        get { return target.transform; }
    }

    public RotateTo(float totalTime, GameObject go, Vector3 target)
        : base(totalTime)
    {
        this.target = go;
        end = target;
    }
    public override void fire()
    {

        Vector3 start = this.target.transform.localEulerAngles;
        diff = end - start;
        base.fire();
    }
    public Vector3 Start
    {
        get
        {
            return end - diff;
        }
    }



    public override void Update(float deltaTime, float progress)
    {
        target.transform.localEulerAngles = diff * (progress - 1) + end;
    }
}

