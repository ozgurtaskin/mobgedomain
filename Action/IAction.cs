﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public abstract class IAction
{
    public abstract event EventHandler<ActionEventArgs> onFinish;
    public abstract bool stopped { get; set; }

    public abstract void fire();

    public abstract bool IsFinished { get; }

    public abstract void UpdateAction(float deltaTime);

    public abstract object Pocket { get; set; }
}
