﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class ScaleTo : AbstractAction
{
    public Vector3 end, diff;
    protected Transform target;
    public ScaleTo(float totalTime, Transform go, Vector3 target)
        : base(totalTime)
    {
        this.target = go;
        end = target;
    }
    public override void fire()
    {
        if (target != null)
        {
            Vector3 start = this.target.localScale;
            diff = end - start;
            base.fire();
        }
    }
    public Vector3 Start
    {
        get
        {
            return end - diff;
        }
    }

    public override void Update(float deltaTime, float progress)
    {
        if (target != null)
        {
            target.localScale = diff * (progress - 1) + end;
        }
        else
        {
            stop();
        }
    }
}

