﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class TurnAngleAxisTo : AbstractAction
{
    private float start, diff;
    private GameObject go;
    private Vector3 axis;
    public TurnAngleAxisTo(float angleStart, float angleTarget, Vector3 axis, GameObject go, float totalTime)
        : base(totalTime)
    {
        this.go = go;
        start = angleStart;
        diff = angleTarget - angleStart;
        this.axis = axis;
    }
    public void setParams(float angleStart, float angleTarget)
    {
        start = angleStart;
        diff = angleTarget - angleStart;
    }
    public override void Update(float deltaTime, float progress)
    {
        go.transform.localRotation = Quaternion.AngleAxis(start + progress * diff, axis);
    }
}
