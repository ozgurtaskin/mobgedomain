﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class MoveToWithInitialSpeed : AbstractAction
{
    public Vector3 end;
    protected Transform target;
    private Vector3 initialSpeed;
    private float prevProgress;
    public MoveToWithInitialSpeed(float totalTime, Transform go, Vector3 target, Vector3 initialSpeed)
        : base(totalTime)
    {
        this.target = go;
        end = target;
        this.initialSpeed = initialSpeed;
    }
    public MoveToWithInitialSpeed(float totalTime, GameObject go, Vector3 target, Vector3 initialSpeed)
        : this(totalTime, go.transform, target, initialSpeed)
    {
    }
    public override void fire()
    {

        if (target != null)
        {
            Vector3 start = this.target.localPosition;
            prevProgress = 0;
            base.fire();
        }
    }
    public override void Update(float deltaTime, float progress)
    {
        if (target == null)
        {
            stop();
        }
        else
        {
            if (prevProgress > 0.999f)
            {
                target.localPosition = end;
            }
            else
            {
            var pos = target.localPosition;
            pos += initialSpeed * Time.deltaTime*(1-progress);
            pos = end +  ( pos-end) * (1-progress )/ (1-prevProgress);
                target.localPosition = pos;
            }
            prevProgress = progress;
        }
    }
}

