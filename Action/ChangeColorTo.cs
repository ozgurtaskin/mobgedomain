﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class ChangeColorTo : AbstractAction
{
    private Color start, target;
    private Transform go;
    private bool traversal;
    public ChangeColorTo(Transform go,Color startColor, Color targetColor, float totalTime, bool traversal = false)
        : base(totalTime)
    {
        this.go = go;
        go.ChangeColor(startColor);
        target = targetColor;
        start = startColor;
        this.traversal = traversal;
    }

    public override void Update(float deltaTime, float progress)
    {
        if (go)
        {
            go.ChangeColor(Color.Lerp(start, target, progress), traversal);
        }
    }

}
