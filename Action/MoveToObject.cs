﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class MoveToObject : MoveTo
{
    private Transform follow;
    public MoveToObject(float totalTime, GameObject go, Transform target)
        : base(totalTime, go, target.position)
    {
        follow = target;
    }
    public override void fire()
    {

        Vector3 start = this.target.transform.position;
        end = follow.transform.position;
        diff = end - start;
        pureFire();
    }
    public override void Update(float deltaTime, float progress)
    {
        if (follow)
        {
            end = follow.position;
            target.transform.position = diff * (progress - 1) + end;
        }
    }
}
