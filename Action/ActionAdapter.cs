﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public interface IActionAdapterListener
{
    void Update(object sender, float deltaTime, float progress, object pocket);
}

public class ActionAdapter : AbstractAction
{
    public IActionAdapterListener listener;

    public ActionAdapter(float totaltime, IActionAdapterListener listener)
        : base(totaltime)
    {
        this.listener = listener;
    }

    public override void Update(float deltaTime, float progress)
    {
        listener.Update(this, deltaTime, progress,pocket);
    }
}
