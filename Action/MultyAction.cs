﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class MultyAction : AbstractAction
{
    public List<AbstractAction> actions;

    public MultyAction(float totalTime)
        : base(totalTime)
    {
    }
    
    public override void Update(float deltaTime, float progress)
    {
        foreach (AbstractAction ac in actions)
        {
            ac.UpdateAction(deltaTime);
        }
    }
}
