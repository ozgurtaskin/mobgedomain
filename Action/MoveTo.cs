﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class MoveTo : AbstractAction
{
    public Vector3 end, diff;
    protected Transform target;
    public MoveTo(float totalTime, Transform go, Vector3 target)
        : base(totalTime)
    {
        this.target = go;
        end = target;
    }
    public MoveTo(float totalTime, GameObject go, Vector3 target)
        : this(totalTime, go.transform, target)
    {
    }
    public override void fire()
    {

        if (target != null)
        {
            Vector3 start = this.target.localPosition;
            diff = end - start;
            base.fire();
        }
    }
    public Vector3 Start
    {
        get
        {
            return end - diff;
        }
    }
    public override void Update(float deltaTime, float progress)
    {
        if (target == null)
        {
            stop();
        }
        else
        {
            target.localPosition = diff * (progress - 1) + end;
        }
    }
}

