﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[ExecuteInEditMode]
public class ActionManager : MonoBehaviour
{
    [SerializeField]
    private static ActionManager sharedInstance;

    private List<IAction> actions;

    public static ActionManager SharedInstance
    {
        get
        {
            if (sharedInstance == null)
            {
                GameObject go = new GameObject("Action Manager");

                sharedInstance = go.AddComponent<ActionManager>();
                sharedInstance.actions = new List<IAction>();
            }
            return sharedInstance;
        }
    }
    public void addNewAction(IAction aa)
    {
        aa.stopped = false;
        actions.Add(aa);
    }
    void Update()
    { 
        if (!Application.isPlaying)
        {
            GameObject.DestroyImmediate(gameObject);
        }
    }


    void FixedUpdate()
    {

        float deltaTime = Time.deltaTime;
		//Debug.Log(actions.Count);
        for (int i = 0; i < actions.Count; )
        {
            IAction a = actions[i];
            if (a.IsFinished)
            {
                a.stopped = true;
                if (actions.Count > i + 1)
                {
                    
                    IAction last = actions.Last();
                    actions.RemoveAt(actions.Count - 1);
                    actions[i] = last;
                }
                else
                {
                    actions.RemoveAt(actions.Count - 1);
                }
            }
            else
            {
                a.UpdateAction(deltaTime);

                i++;
            }
        }
    }
}