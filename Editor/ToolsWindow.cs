﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEditor;

public class ToolsWindow : EditorWindow
{
    [MenuItem("Window/Tools")]
    static void init()
    {
        EditorWindow.GetWindow<ToolsWindow>("Tools");
    }

    void OnGUI()
    {
        ParticleAnimatorTools();
    }
    ParticleAnimator pa;
    bool setAlpha = false;

    int selectedType = 0;

    void ParticleAnimatorTools()
    {
        pa = (ParticleAnimator)EditorGUILayout.ObjectField("Particle Animator", pa, typeof(ParticleAnimator));
        EditorGUILayout.BeginHorizontal();
        if (pa)
        {
            setAlpha = EditorGUILayout.Toggle("set alpha", setAlpha);
            Color c = EditorGUILayout.ColorField(pa.colorAnimation[0]);
            pa.setColor(c, setAlpha);
        }
        EditorGUILayout.EndHorizontal();
        var selected = Selection.activeObject;
        Type[] types;
        if (selected && (AssetDatabase.IsMainAsset(selected)))
        {
            if (selected is GameObject)
            {
                var sgo = (GameObject)selected;
                var comps = sgo.GetComponents<Component>();
                types = new Type[comps.Length];
                for (int i = 0; i < types.Length; i++)
                {
                    types[i] = comps[i].GetType();
                }
            }
            else
            {
                types = new Type[] { selected.GetType() };
            }
            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("select", GUILayout.ExpandWidth(true)))
            {

                var path = AssetDatabase.GetAssetPath(selected);
                var firstI = path.IndexOf("/");
                var secondI = path.IndexOf("/", firstI + 1);
                var lastI = path.LastIndexOf("/");
                path = path.Substring(secondI+1,lastI-secondI-1);
                var selection = Resources.LoadAll(path, types[selectedType]);
                if (selected is GameObject)
                {
                    for (int i = 0; i < selection.Length; i++)
                    {
                        selection[i] = ((Component)selection[i]).gameObject;
                    }
                }
                //Debug.Log(path + " "+selection.Length);
                Selection.objects = selection;
            }
            EditorGUILayout.LabelField("all", GUILayout.Width(40));
            selectedType = EditorGUILayout.Popup(this.selectedType, types.toStringArray(), GUILayout.ExpandWidth(true));

            EditorGUILayout.LabelField("types in the asset folder", GUILayout.Width(150));
        }
        EditorGUILayout.BeginHorizontal();
    }

}
