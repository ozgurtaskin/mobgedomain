﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[ExecuteInEditMode]
public class DeleteGameObjetcOnUpdate : MonoBehaviour
{
    public static DeleteGameObjetcOnUpdate construct(GameObject go)
    {
        var v = go.AddComponent<DeleteGameObjetcOnUpdate>();
        return v;
    }
    void Update()
    {
        this.DestroyObj(gameObject);
    }

}
