﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEditor;
using System.IO;


public static class EditorExtension
{
    public static int listSizeField<T>(String label, List<T> l, Func<T> generateDefault, int start = 0, int count = -1)
    {
        if (count == -1)
        {
            count = l.Count;
        }
        EditorGUILayout.BeginHorizontal();
        int newSize = Mathf.Max(EditorGUILayout.IntField(label, count), 0);
        newSize = Mathf.Max(0, newSize);
        CommonCollections.setSize<T>(newSize, l, generateDefault, start, count);
        EditorGUILayout.EndHorizontal();
        return newSize;
    }
    public static void saveSharedData(UnityEngine.Object sharedData, string path, bool overrideIfNecessary = false, string extension = "asset")
    {
        var fldeer = Path.Combine("Assets/Resources", path + "." + extension);

        if (!Directory.Exists(new FileInfo(fldeer).DirectoryName))
        {
            Directory.CreateDirectory(fldeer);
        }
        var loaded = AssetDatabase.LoadMainAssetAtPath(fldeer);
        if (loaded)
        {
            
            if (!overrideIfNecessary)
            {
                goto exit;
            }
            if (loaded == sharedData)
            {
                EditorUtility.SetDirty(sharedData);
                goto exit;
            }
            //Debug.Log(AssetDatabase.DeleteAsset(fldeer));
            goto save;
        }
        else
        {
            goto save;
        }
    save:
        AssetDatabase.CreateAsset(sharedData, fldeer);
    exit:
        return;
    }
    public static Vector3 SceneMousePos
    {
        get
        {
            var r = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);
            Vector3 pos = r.origin + r.direction * ((0 - r.origin.z) / r.direction.z);
            return pos;
        }
    }
    public static void vectorListSceneGUI(List<Vector2> l, Vector3 position, bool drawPath, ref bool ctrlPressed)
    {
        bool mousedown = false;
        switch (Event.current.rawType)
        {
            case EventType.KeyDown:
                if (Event.current.keyCode == KeyCode.LeftControl)
                {
                    ctrlPressed = true;
                }
                break;
            case EventType.KeyUp:
                if (Event.current.keyCode == KeyCode.LeftControl)
                {
                    ctrlPressed = false;
                }
                break;
            case EventType.MouseUp:
                mousedown = true;
                break;
            default:
                break;
        }
        for (int i = 0; i < l.Count; i++)
        {
            l[i] = Handles.PositionHandle(position + (Vector3)l[i], Quaternion.identity) - position;
            
        }
        if (ctrlPressed)
        {
            bool buttonPressed = false;
            for (int i = 0; i < l.Count; )
            {
                if (Handles.Button(position + (Vector3)l[i], Quaternion.identity, 4, 5, Handles.RectangleCap))
                {
                    
                    l.RemoveAt(i);
                    buttonPressed = true;
                }
                else
                {
                    i++;
                }
            }
            if (!buttonPressed && mousedown)
            {
                var mpos = SceneMousePos;
                l.Add(new Vector2(mpos.x - position.x, mpos.y - position.y));
            }
        }
    }
}
