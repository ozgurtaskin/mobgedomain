﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[AddComponentMenu("Audio/Manager")]
public class AudioManager : MonoBehaviour
{
	private List<AudioSource> fxes;
	private List<AudioSource> musics;
//    private Settings settings;
	private static AudioManager sharedManager;

	void Awake ()
	{
		sharedManager = this;
		fxes = new List<AudioSource> ();
		musics = new List<AudioSource> ();
//        settings = Settings.SharedInstance;
//        settings.onChange += new EventHandler<SettingEventArgs>(settings_onChange);
	}

	public static AudioManager SharedInstance {
		get {
			return sharedManager; 
		}
	}

	public void addMusic (AudioSource music)
	{
//        music.mute = !settings.MusicOn;
		musics.Add (music);
	}

	public void addFx (AudioSource fx)
	{
//        fx.mute = !settings.FxOn;
		fxes.Add (fx);

	}
//    void settings_onChange(object sender, SettingEventArgs e)
//    {
//        //Debug.Log(fxes[0]);
//        switch (e.type)
//        {
//            case SettingType.FX:
//                updateSounds(fxes, !settings.FxOn);
//                break;
//            case SettingType.MUSIC:
//                updateSounds(musics, !settings.MusicOn);
//                break;
//            default:
//                break;
//        }
//    }
	private void updateSounds (List<AudioSource> sounds, bool mute)
	{
		for (int i = 0; i < sounds.Count;) {
			AudioSource sound = sounds [i];
            
			if (sound != null) {
				bool bb = sound.isPlaying;
				float b = sound.time;
				sound.Stop ();
				sound.mute = mute;

				if (bb) {
					sound.Play ();
					sound.time = b;
				}
				//Debug.Log(""+sound + " " + sound.mute + " " + mute);
				i++;
			} else {
				sounds [i] = sounds.Last ();
				sounds.RemoveAt (sounds.Count - 1);
			}
		}
	}
}
