﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


[AddComponentMenu("Audio/Typed Source")]
public class TypedAudioSource : MonoBehaviour
{
    
    public AudioSourceType type;
    [SerializeField]
    private AudioSource source;
    void Start()
    {
        if (source != null)
        {
            switch (type)
            {
                case AudioSourceType.FX:
                    AudioManager.SharedInstance.addFx(source);
                    break;
                case AudioSourceType.Music:
                    AudioManager.SharedInstance.addMusic(source);

                    break;
                default:
                    break;
            }
        }
    }
    void ensureSource()
    {
        if (source == null)
        {
            source = gameObject.AddComponent<AudioSource>();
            source.playOnAwake = false;

            source.rolloffMode = AudioRolloffMode.Linear;
            source.panLevel = 0.85f;
            
        }
    }
    public AudioClip Clip
    {
        get
        {
            ensureSource();
            
            return source.clip;
        }
        set
        {
            ensureSource();
            source.clip = value;
        }
    }
    public AudioSource Audio
    {
        get
        {
            return source;
        }
    }

}

public enum AudioSourceType : byte
{
    FX,
    Music,
}
