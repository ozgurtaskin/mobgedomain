﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(TypedAudioSource))]
public class AudioSourceEditor : Editor
{

    public override void OnInspectorGUI()
    {
        TypedAudioSource tas = (TypedAudioSource)target;

        tas.type = (AudioSourceType)EditorGUILayout.EnumPopup("Type", tas.type);
        tas.Clip = (AudioClip)EditorGUILayout.ObjectField("Clip", tas.Clip,typeof(AudioClip),true);
    }
}

