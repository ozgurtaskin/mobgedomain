﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Spline
{
    [Serializable]
    public class Spline3D
    {
        [SerializeField]
        List<PointData> points;
        [SerializeField]
        private InterpolationMode interpolation;
        [SerializeField]
        private LoopMode loopMode;

        public LoopMode LoopMode
        {
            get { return loopMode; }
            set
            {
                loopMode = value;
                switch (loopMode)
                {
                    case LoopMode.Open:
                        break;
                    case LoopMode.Looped:
                        break;
                    default:
                        break;
                }
            }
        }

        public InterpolationMode Interpolation
        {
            get { return interpolation; }
            set
            {
                interpolation = value;
                switch (interpolation)
                {
                    case InterpolationMode.Linear:
                        break;
                    case InterpolationMode.Bezier:
                        break;
                    case InterpolationMode.Soft:

                        break;
                    default:
                        break;
                }
            }
        }

        public Spline3D()
        {
            points = new List<PointData>();
        }
        public Vector3 this[int index]
        {
            get { return points[index].point; }
            set { points[index] = new PointData(value); }
        }
        public Vector3 getPosition(float p)
        {
            int index = (int)p;
            float percentage = p - index;
            return points[index].point;
        }
    }
    [Serializable]
    public class PointData
    {
        public Vector3 point;
        public Vector3 rightVector;
        public Vector3 leftVector;

        public PointData(Vector3 point)
        {
            this.point = point;
        }
    }
    [Serializable]
    public enum InterpolationMode
    {
        Linear = 0,
        Bezier = 1,
        Soft = 2,
    }
    [Serializable]
    public enum LoopMode
    {
        Open = 0,
        Looped = 1,

    }
}
