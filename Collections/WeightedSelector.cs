using System;
using System.Collections.Generic;


public class WeightedSelector
{
    public delegate float GetWeight<T>(T t);
    public static T selectWeighted<T>(IEnumerable<T> e, GetWeight<T> met, float totalWeight, out int index)
    {
        var l = e.GetEnumerator();
        float r = UnityEngine.Random.Range(0, totalWeight);
        index = -1;
        while (r > 0 && l.MoveNext())
        {

            index++;
            r -= met(l.Current);
        }
        return l.Current;
    }
    public static T selectWeighted<T>(IEnumerable<T> e, GetWeight<T> met, out int index)
    {

        float total = 0;
        foreach (var v in e)
        {
            total += met(v);
        }

        return selectWeighted<T>(e, met, total, out index);
    }

}

