﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Collections
{
    [Serializable]
    public class ConstantIndexedList<T> : IEnumerable<KeyValuePair<int, T>>
    {
        [SerializeField]
        private List<T> data;
        [SerializeField]
        private List<bool> exists;
        [SerializeField]
        private int nextIndex; 

        public ConstantIndexedList()
        {
            data = new List<T>();
            exists = new List<bool>();
            nextIndex = 0;
        }
        public int addElement(T element)
        {
            int ind = nextIndex;
            if (nextIndex == data.Count)
            {
                data.Add((element));
                exists.Add(true);
            }
            else
            {
                data[nextIndex] = (element);
                exists[nextIndex] = true;
            }
            do
            {
                nextIndex++;
            }
            while (nextIndex < data.Count && exists[nextIndex]);
            return ind;
        }
        /// <summary>
        /// retrieves the element with given index
        /// </summary>
        /// <param name="index">set works only if there is already an element at given index</param>
        /// <returns></returns>
        public T this[int index]
        {
            get
            {
                return (T)data[index];
            }
            set
            {
                if (exists[index])
                {
                    data[index] = value;
                }
                else
                {
                    throw new InvalidOperationException("set only wotks if there is already an element at given index");
                }
            }
        }
        public bool tryGetElement(int id, out T ts, T defaultValue = default(T))
        {
            if (data.Count <= id || !exists[id])
            {
                ts = default(T);
                return false;
            }
            ts = (T)data[id];
            return true;
        }
        public void removeAt(int index)
        {
            //Debug.Log("d");
            data[index] = default(T);
            exists[index] = false;
            if (index < nextIndex)
            {
                nextIndex = index;
            }
        }



        IEnumerator<KeyValuePair<int, T>> IEnumerable<KeyValuePair<int, T>>.GetEnumerator()
        {
            int index = 0;
            foreach (var v in data)
            {
                if (exists[index])
                {
                    yield return new KeyValuePair<int, T>(index, (T)v);
                }
                index++;
            }
        }


        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            int index = 0;
            foreach (var v in data)
            {
                if (exists[index])
                {
                    yield return new KeyValuePair<int, T>(index, (T)v);
                }
                index++;
            }
        }
    }
   /* [Serializable]
    public class ExistableNode 
    {
        public UnityEngine.Object element;
        public bool exist;
        public ExistableNode(UnityEngine.Object element)
        {
            this.element = element;
            exist = true;
        }
    }*/
}