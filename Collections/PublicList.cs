using UnityEngine;
using System.Collections.Generic;
public class PublicList<T> : IList<T>
{
	public T[] data;
	private int size;
	private int multipleOf;
	private float maxUnusedElements;
	public int allocateCount;
	public int unallocateCount;
	private bool copyOldData;
	
	public PublicList(int multipleOf, float maxUnusedElements = 0.2f, int capacity = 10)
	{
		allocateCount = 0;
		unallocateCount = 0;
		this.maxUnusedElements = maxUnusedElements;
		this.multipleOf = multipleOf;
		data = new T[multipleOf * capacity];
		size = 0;
		copyOldData = false;
	}
	public PublicList () :
		this(1,1.5f,4)
	{
		copyOldData = true;
	}
	
	public T this[int index]
	{
		get
		{
			return data[index];
		}
		set
		{
			/*if(data.Length <= index){
					Debug.Break();
					Debug.Log(data.Length + " " + index);
				}*/
			data[index] = value;
		}
	}

	public int Count { get { return Size; } }

	public void Add (T p)
	{
		Size += 1;
		data[Size-1] = p;
	}

	public void RemoveAt (int i)
	{
		int ns = size-1;
		for (int j = i; j < ns; j++) {
			data[j] = data[j+1];
		}
		data[ns] = default(T);
		Size = ns;
	}
	
	public int Size
	{
		get
		{
			return size;
		}
		set
		{
			int oldSize = size;
			size = value;
            int maxDiff = ((int)(oldSize * maxUnusedElements)) * multipleOf;
			/*if(maxDiff < multipleOf*2){
				maxDiff = multipleOf*2;
			}*/
			int capacity = data.Length;
			int diff = capacity - size;
			if (diff < 0 || diff > maxDiff)
			{
				int newCapacity = size + maxDiff/2;
				//Debug.Log(size + " " + newCapacity);
				T[] newData = new T[newCapacity];
				if(copyOldData)
				{
					for(int i = 0; i < Mathf.Min(oldSize, size); i++)
					{
						newData[i] = data[i];
					}
				}
				/*int limit = Mathf.Min(oldSize, size);
                    for (int i = 0; i < limit; i++)
                    {
                        newData[i] = data[i];
                    }*/
				data = newData;

				//allocateCount++;
			}
			else{
				//unallocateCount++;
				if(size == 0){
					data = new T[0];
					//allocateCount = 0;
					//unallocateCount = 0;
				}
			}
			
			/*int oldSize = size;
                size = value;

                int capacity = data.Length;
                if (size > capacity)
                {
                    int diff = multipleOf + 2 * size;
                    capacity = diff / multipleOf;
                    capacity *= multipleOf;

                    T[] newData = new T[capacity];
                    for (int i = 0; i < oldSize; i++)
                    {
                        newData[i] = data[i];
                    }
                    data = newData;
                }
                 * */
		}
	}

    public int IndexOf(T item)
    {
        throw new System.NotImplementedException();
    }

    public void Insert(int index, T item)
    {
        throw new System.NotImplementedException();
    }


    public void Clear()
    {
        throw new System.NotImplementedException();
    }

    public bool Contains(T item)
    {
        throw new System.NotImplementedException();
    }

    public void CopyTo(T[] array, int arrayIndex)
    {
        throw new System.NotImplementedException();
    }

    public bool IsReadOnly
    {
        get { throw new System.NotImplementedException(); }
    }

    public bool Remove(T item)
    {
        throw new System.NotImplementedException();
    }

    public IEnumerator<T> GetEnumerator()
    {
        throw new System.NotImplementedException();
    }

    System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
    {
        throw new System.NotImplementedException();
    }
}