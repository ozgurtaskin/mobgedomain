﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using System.Collections;

public static class CommonCollections
{
    public delegate int Compare<T>(T obj);
    public static void setSize<T>(int newSize, List<T> l, Func<T> generateDefault, int start = 0, int count = -1)
    {
        if (count == -1)
        {
            count = l.Count;
        }
        while (count < newSize)
        {
            l.Insert(start + count, generateDefault());
            count++;
        }
        while (count > newSize)
        {
            l.RemoveAt(start + count - 1);
            count--;
        }
    }
    public static void FastRemoveAt(this IList l, int index)
    {
        l[index] = l[l.Count - 1];
        l.RemoveAt(l.Count - 1);
    }
    public static void Swap(this IList l, int index1, int index2)
    {
        var temp = l[index1];
        l[index1] = l[index2];
        l[index2] = temp;
    }
    /// <summary>
    /// the list must be sorted with increasing order.
    /// works with O(log n)
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="l"></param>
    /// <param name="compareFunc"></param>
    /// <returns></returns>
    public static int FindNearest<T>(this IList l,Compare<T> compareFunc)
    {
        return findNearest<T>(l, compareFunc, 0, l.Count - 1);
    }
    public static string[] toStringArray(this IList l)
    {
        string[] strs = new string[l.Count];
        for (int i = 0; i < strs.Length; i++)
        { strs[i] = l[i].ToString(); }
        return strs;
    }
    private static int findNearest<T>(IList l, Compare<T> compareFunc, int start, int end)
    {
        if (start >= end)
        {
            return start;
        }
        if (start + 1 >= end)
        {
            int v = compareFunc((T)l[start]) + compareFunc((T)l[end]);
            if (v >= 0)
            {
                return start;
            }
            return end;
        }
        int middle = (start + end) / 2;
        int value = compareFunc((T)l[middle]);
        if (value == 0)
        {
            return middle;
        }
        if (value < 0)
        {
            return findNearest<T>(l, compareFunc, middle,end);
        }
        {
            return findNearest<T>(l, compareFunc,start, middle);
        }
    }
}
