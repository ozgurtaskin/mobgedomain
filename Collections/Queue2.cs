using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Threading;

[Serializable]
public class Queue2<T>: IEnumerable<T>{
	[SerializeField]
	private T[] data;
	public float growRate;
	private int minimumGrow = 4;
	private int head;
	private int tail;
	private int size;
	public Queue2()
	{
		data = new T[minimumGrow];
		head = 0;
		tail = 0;
		size = 0;
		growRate = 2;
	}
	public void Enqueue(T item)
	{
		if(size >= data.Length)
		{
			int grow = (int)(data.Length*growRate);
			if(grow < minimumGrow){
				grow = minimumGrow;
			}
			setCapacity(size + grow);
		}
		data[tail] = item;
		tail = (tail + 1)%data.Length;
		size++;
	}
	public int Count
	{
		get
		{
			return size;
		}
	}
	public IEnumerable<T> reverse()
	{
		if(size > 0 )
		{
			if( tail > head)
			{
				for(int i = tail-1;i >= head; i--)
				{
					yield return data[i];
				}
			}
			else
			{
				for(int i = tail-1;i >= 0; i--)
				{
					yield return data[i];
				}
				for(int i = data.Length - 1; i >= head; i--)
				{
					yield return data[i];
				} 
			}
		}
	}
	public T Dequeue()
	{
		if(size <= 0)
		{
			throw new InvalidOperationException("empty queue");
		}
		var res = data[head]; 
		data[head] = default(T);
		head = (head+1)%data.Length;
		size--;
		return res;
	}
	public T Peek()
	{
		if(size <= 0)
		{
			throw new InvalidOperationException("empty queue");
		}
		return data[head];
	}
	private void setCapacity(int cap)
	{
		T[] newData = new T[cap];
		if(size > 0)
		{
			if( tail > head)
			{
				Array.Copy(data,head,newData,0,size);
			}
			else
			{
				Array.Copy(data,head,newData,0,data.Length-head);
				Array.Copy(data,0,newData,data.Length-head,tail);
			}
		}
		data = newData;
		head = 0;
		tail = size%data.Length;
		
	}

    public IEnumerator<T> GetEnumerator()
    {
        if (size > 0)
        {
            if (tail > head)
            {
                for (int i = head; i < tail ; i++)
                {
                    yield return data[i];
                }
            }
            else
            {
                for (int i = head; i < data.Length; i++)
                {
                    yield return data[i];
                }
                for (int i = 0; i < tail; i++)
                {
                    yield return data[i];
                }
            }
        }
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }
}
 