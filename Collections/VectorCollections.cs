﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using System.Collections;

public static class VectorCollections
{
    public static void orderLine(this IList<Vector2> list, float length, float degree)
    {
        Vector2 trig = new Vector2(Mathf.Cos(degree * Mathf.Deg2Rad), Mathf.Sin(degree * Mathf.Deg2Rad));
        Vector2 step = trig * length / (list.Count - 1);
        for (int i = 1; i < list.Count; i++)
        {
            list[i] = i * step + list[0];
        }
    }
    public static void scale(this IList<Vector2> list, Vector2 scl, Vector2 center)
    {
        for (int i = 0; i < list.Count; i++)
        {
            var dif = list[i]-center;
            list[i] =center+ new Vector2(dif.x * scl.x, dif.y * scl.y);
        }
    }
    public static Vector2 getCenter(this IList<Vector2> list)
    {
        Vector2 v = new Vector2(0, 0);
        for (int i = 0; i < list.Count; i++)
        {
            v += list[i];
        }
        return v / list.Count;
    }
    public static void translate(this IList<Vector2> list, Vector2 amount)
    {
        for (int i = 0; i < list.Count; i++)
        {
            list[i] = list[i] + amount;
        }
        
    }
}
