﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using System.IO;
using System.Security.AccessControl;

public abstract class DataList<T> : ScriptableObject
{
    //string directoryPath;
    string fullDirectoryPath;
    private HashSet<string> fileNames;
    public void setPath(string directoryPath)
    {
        //this.directoryPath = directoryPath;
        fullDirectoryPath = Path.Combine(Application.persistentDataPath, directoryPath);
        if (!Directory.Exists(fullDirectoryPath))
        {
            Directory.CreateDirectory(fullDirectoryPath);
        }
        fileNames = new HashSet<string>();
        foreach (var v in Directory.GetFiles(fullDirectoryPath))
        {
            
            fileNames.Add(new FileInfo(v).Name);
        }
    }
    void OnEnable()
    {
        //Debug.Log("enabled");
    }
    void OnDisable()
    {
        //Debug.Log("disabled");
    }
    FileStream getFileStream(string filename)
    {
        return new FileStream(Path.Combine(fullDirectoryPath, filename), FileMode.OpenOrCreate);
    }
    
    public T getObject(string objectName)
    {
        //Debug.Log(Path.Combine(fullDirectoryPath, objectName));
        FileStream fs = getFileStream(objectName);
        var v = read(fs);
        fs.Close();
        return v;
    }
    public void saveObject(string objectName, T obj)
    {
        fileNames.Add(objectName);
        FileStream fs = getFileStream(objectName);
        write(fs, obj);
        fs.Close();
    }
    public bool deleteObject(string objectName)
    {
        if (fileNames.Remove(objectName))
        {
            File.Delete(Path.Combine(fullDirectoryPath, objectName));
            return false;
        }
        return true;
    }
    public string[] getAllObjects()
    {
        return fileNames.ToArray();
    }
    protected abstract T read(Stream s);
    protected abstract void write(Stream s, T t);
}