﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public static class MonoBehaviourExtension
{
    public static bool getFirstChildsColor(this Transform go, out Color color)
    {
        Renderer r = go.renderer;
        if (r != null)
        {
            color = r.material.color;
            return true;
        }
        for (int i = 0; i < go.childCount; i++)
        {
            if (go.GetChild(i).getFirstChildsColor(out color))
            {
                return true;
            }
        }
        color = Color.white;
        return false;

    }
    public static void ChangeColor(this Transform go, Color c, bool traversal = false)
    {
        Renderer r = go.renderer;
        if (r != null)
        {
            r.material.color = c;
        }
        if (traversal)
        {
            for (int i = 0; i < go.childCount; i++)
            {
                ChangeColor(go.GetChild(i), c, true);
            }
        }
    }
    public static void setZAngle(this Transform tr, float rad)
    {
        tr.localRotation = new Quaternion(0, 0, Mathf.Sin(rad * 0.5f), Mathf.Cos(rad * 0.5f));
    }
    public static void setXAngle(this Transform tr, float rad)
    {
        tr.localEulerAngles = new Vector3(rad * Mathf.Rad2Deg, 0, 0);
    }
    public static void setYAngle(this Transform tr, float rad)
    {
        tr.localEulerAngles = new Vector3(0, rad * Mathf.Rad2Deg, 0);
    }
    
	public static float getZAngle(this Transform tr)
	{
		Quaternion q = tr.localRotation;
		return Mathf.Atan2(q.z, q.w) * 2;
	}
    /*public static void Pause(this GameObject go, bool pause = true,bool withChildren = false)
    {
        Component[] components = withChildren ? go.GetComponentsInChildren<Component>(true) : go.GetComponents<Component>();
        foreach (Component c in components)
        {
            c.active
        }
    }*/
    public static GameObject instantiatePrefab(String s)
    {
        return (GameObject)(GameObject.Instantiate(Resources.Load(s)));
    }
    public static T instantiatePrefab<T>(String s) where T : UnityEngine.Object
    {
        return (T)(GameObject.Instantiate(Resources.Load(s, typeof(T))));
    }
    public static T loadResource<T>(string path) where T : UnityEngine.Object
    {
        return (T)Resources.Load(path, typeof(T));
    }
    public static T instantiate<T>(T obj) where T : UnityEngine.Object
    {
        return (T)GameObject.Instantiate(obj);
    }
    public static void RemoveAllChildren(this Transform mb)
    {
#if(UNITY_EDITOR)
        if (!(Application.isPlaying))
        {
            while (mb.transform.childCount > 0)
            {
                GameObject.DestroyImmediate(mb.transform.GetChild(0).gameObject);
            }
        }
        else
        {
            for (int i = 0; i < mb.transform.childCount; i++)
            {
                GameObject.Destroy(mb.transform.GetChild(i).gameObject);
            }
        }
#else        
        
            
            for (int i = 0; i < mb.transform.childCount; i++)
            {
                GameObject.Destroy(mb.transform.GetChild(i).gameObject);
            }
#endif
    }
    public static void DestroyObj(this MonoBehaviour mb, UnityEngine.Object o)
    {

        DestroyObj(o);
    } 
    public static void DestroyObj(UnityEngine.Object o)
    {
#if(UNITY_EDITOR)
        if (!(Application.isPlaying))
        {
            GameObject.DestroyImmediate(o);
        }
        else
        {
            GameObject.Destroy(o);
        }
#else        
        
            GameObject.Destroy(o);
#endif
    }
    public static void setColor(this ParticleAnimator pa, Color c, bool setAlpha = false)
    {
        Color[] colors = new Color[pa.colorAnimation.Length];
        for (int i = 0; i < pa.colorAnimation.Length; i++)
        {
            if (!setAlpha)
            {
                c.a = pa.colorAnimation[i].a;
            }
            colors[i] = c;
        }
        pa.colorAnimation = colors;
    }
}
